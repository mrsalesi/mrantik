import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:vaTamam/mrAntik/auctionsOfOneCategory.dart';
import 'package:vaTamam/mrAntik/products.dart';
import 'package:vaTamam/mrAntik/tools/jjTools.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../mrAntik/home.dart';
import 'package:go_router/go_router.dart';

import 'mrAntik/product.dart';
import 'mrAntik/tools/jjTools.dart';



Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized(); //you are awaiting on main() method. So, the solution would be this code in first line of main()


  //Get online banner1 Pics
  String resultBanner1 = await JJ().jjAjax('&do=Pic.getBanner1');
  if (resultBanner1.isNotEmpty) {
    HomePage.banner1ImagesJson = jsonDecode(resultBanner1);
  }
  //Get online Slider Pics
  String resultBanner2 = await JJ().jjAjax('&do=Pic.getBanner2');
  if (resultBanner2.isNotEmpty) {
    HomePage.banner2ImagesJson = jsonDecode(resultBanner2);
  }
  //Get online Slider Pics
  String resultBanner3 = await JJ().jjAjax('&do=Pic.getBanner3');
  if (resultBanner3.isNotEmpty) {
    HomePage.banner3ImagesJson = jsonDecode(resultBanner3);
  }

  SharedPreferences? localStorage;
  localStorage = await SharedPreferences.getInstance();
  JJ.localStorage = localStorage;
  debugPrint("main()>JJ.localStorage!.getString(likedProductsList): ${JJ.localStorage!.getString("likedProductsList")}");
  if (JJ.localStorage!.getString("likedProductsList") == null) {
    JJ.localStorage!.setString("likedProductsList", '');
  }
  //set an string to local storage for autoIncreasePrice in auctions
  if (JJ.localStorage!.getString("autoIncreasePriceList") == null || JJ.localStorage!.getString("autoIncreasePriceList")!.isEmpty) {
    Map<String, String> temp = {};
    temp['0'] = '0';
    JJ.localStorage!.setString("autoIncreasePriceList", jsonEncode(temp));
  }
  //Get public configuration like auction bid price or price to become VIP
  String result = await JJ().jjAjax('&do=WebConfig.getPublicConfigs');
  if (result.isNotEmpty) {
    var publicConfigs = jsonDecode(result);
    for (int i = 0; i < publicConfigs.length; i++) {
      debugPrint('publicConfigs[$i][webConfig_name]=' + publicConfigs[i]['webConfig_name'].toString() + "\n");
      JJ.publicConfigs[publicConfigs[i]['webConfig_name']] = publicConfigs[i]['webConfig_value'];
    }
  }

  JJ.user_token = localStorage.getString('user_token') ?? "";
  debugPrint("main()>JJ.user_token==${JJ.user_token}");
  if (JJ.user_token != "") {
    String params = "do=Access_User.loginUserWhitToken";
    params += "&user_token=";
    params += JJ.user_token;
    String result = await JJ().jjAjax(params);
    debugPrint("======$result");
    List<dynamic> jsonArray = jsonDecode(result);
    Map<String, dynamic> json = jsonArray[0];
    SharedPreferences? localStorage;
    localStorage = await SharedPreferences.getInstance();
    if (json['status'] == "0") {
      debugPrint("loginUserWhitToken errrrr status=0");
      // JJ.user_token = "";
      // localStorage.setString("user_token", "");
      JJ.jjToast(json['comment']);
    } else {
      JJ.user_token = json['user_token'].toString(); //@ToDo server can send new token
      localStorage.setString("user_token", json['user_token'].toString());
      JJ.user_id = json['id'].toString();
      JJ.user_name = json['user_name'].toString();
      JJ.user_username = json['user_username'].toString();
      JJ.user_family = json['user_family'].toString();
      JJ.user_birthdate = json['user_birthdate'].toString();
      JJ.user_mobile = json['user_mobile'].toString();
      JJ.user_address = json['user_address'].toString();
      JJ.user_city = json['user_city'].toString();
      JJ.user_grade = json['user_grade'].toString();
      JJ.user_codeMeli = json['user_codeMeli'].toString();
      JJ.userBalance = "0";
      JJ.user_postalCode = json['user_postalCode'].toString();
      JJ.user_email = json['user_email'].toString();
      JJ.user_AccountInformation = json['user_AccountInformation'].toString();

      JJ.userBalance = "0";
      //Get user balance that mean all (payments - bills) as userBalance it must be after login because of Session
      await JJ().jjAjax('&do=Payment.getUserInSessionBalance').then((result) async {
        var resultJson = jsonDecode(result);
        // JJ.userBalance = resultJson[0]['userBalance'].toString().replaceAll(".0", "");//Replace floating point and convert to float int
      });
    }
  }
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
    runApp(const MrAntik());
  });

  //Get online Slider Pics
  String result2 = await JJ().jjAjax('&do=Pic.getSlider');
  if (result2.isNotEmpty) {
    HomePage.sliderImagesJson = jsonDecode(result2);
  }

}

/// The route configuration for seo and deep linking and switch between web pages and application
final GoRouter _router = GoRouter(
  initialLocation: "/",
  routes: [
    GoRoute(
      path: '/',
      builder: (BuildContext context, GoRouterState state) {
        return const HomePage();
      },
      routes: [
        GoRoute(
          path: 'products',
          builder: (BuildContext context, GoRouterState state) {
            return const Products(categoryId: '', categoryTitle: "جدیدترین ها");
          },
        ),
        GoRoute(
          path: 'product',//
          builder: (BuildContext context, GoRouterState state) {
            String id = state.uri.queryParameters['id']!;//getting parameter from query
            debugPrint('--------switchToApp------------>>>>>>>product.id=$id');
              return Products(categoryId: '',categoryTitle: '', productId: id, );
          },
        ),
      ]
    ),

  ],
);

class MrAntik extends StatelessWidget {
  const MrAntik({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: _router,
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale("fa", "IR"),
      ],
      theme: ThemeData(fontFamily: 'B_Yekan'),
      locale: const Locale("fa", "IR"),
    );
  }
}
