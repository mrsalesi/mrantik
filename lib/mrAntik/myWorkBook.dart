import 'dart:convert';

import 'package:flutter/material.dart';

import 'tools/jjTools.dart';
import 'widgets/Header.dart';

class MyWorkBook extends StatefulWidget {
  const MyWorkBook({
    Key? key,
  }) : super(key: key);

  @override
  State<MyWorkBook> createState() => _MyWorkBookState();
}

class _MyWorkBookState extends State<MyWorkBook> {
  List<dynamic>? resultJson ;
  @override
  void initState() {
    super.initState();
    getUserWorkBook();
  }

  getUserWorkBook() async {
    String params = '';
    params = 'do=Factor.getUserWorkBook'; //user ID is in session
    params += '&user_Id=${JJ.user_id}'; //user ID is in session
    String result = await JJ().jjAjax(params);
    debugPrint('Factor.getUserWorkBook()>>'+result);
    resultJson = jsonDecode(result);
    if (resultJson!.isEmpty) {
      JJ.jjToast('خرید و فروشی برای شما ثبت نشده');
      return;
    }
    ; //Add new items to available items
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
      // 1st Section =========================================================
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              // 1st Section =========================================================
              //########################################
              Column(
                children: [
                  const Text(
                    "کارنامه معاملات",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "میانگین امتیاز:",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['product_factor_satisfyRating'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "فروش در گالری آنلاین: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['count_sales'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "فروش مزایده موفق: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['count_salesInAuctions'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "کالای مزایده: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['count_auction'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "کالای گالری آنلاین: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['count_product'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "خرید از گالری آنلاین:",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['count_buyProduct'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "خرید از مزایده:",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['count_buyInAuctions'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
              //########################################
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
