import 'dart:convert';
import 'dart:io';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vaTamam/mrAntik/paymentOK.dart';
import 'package:vaTamam/mrAntik/widgets/navBarBottom.dart';
import 'package:persian_datetimepickers/persian_datetimepickers.dart';
import 'payment.dart';
import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/allertDialog.dart';

class RegisterVIP extends StatefulWidget {
  const RegisterVIP({Key? key}) : super(key: key);

  @override
  State<RegisterVIP> createState() => _RegisterVIPState();
}

class _RegisterVIPState extends State<RegisterVIP> {
  String errorsForForm = "";
  String _user_username = "";
  String _user_email = "";
  String _user_city = "";
  bool nationalCodeIsValid = JJ.user_grade == "VIP"; //if user is VIP so he pass Meli code validation
  String nationalCodeValidateError = "";
  bool bankAccountIsValid = JJ.user_grade == "VIP"; //if user is VIP so he pass this section
  String bankAccountValidateError = "";
  bool userAgreementIsReaded = JJ.user_grade == "VIP"; //if user is VIP so he pass this section
  String userAgreementReadError = "";

  DateTime _user_birthdate = DateTime.parse(JJ.user_birthdate);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController user_adminDescriptionController = TextEditingController();
  final TextEditingController user_nameController = TextEditingController(text: JJ.user_name);
  final TextEditingController user_familyController = TextEditingController(text: JJ.user_family);
  final TextEditingController user_AccountInformationController = TextEditingController(text: JJ.user_AccountInformation);

  XFile? selectedImageFileObj;
  final ImagePicker picker = ImagePicker();

  Future<String> getImageAndUpload(ImageSource media) async {
    selectedImageFileObj = await picker.pickImage(source: media, maxHeight: 400, maxWidth: 400);
    return await JJ.fileUpload(File(selectedImageFileObj!.path));
  }

  //This function send request to server
  Future<void> validateAndSave() async {
    final FormState? form = _formKey.currentState;
    debugPrint("::");
    // if (!nationalCodeIsValid) {
    //   errorsForForm += 'هویت و کد ملی شما محرز نشده';
    // }
    // if (!bankAccountIsValid) {
    //   errorsForForm += '\nشماره ی شبا تایید نشده است';
    // }
    // if (!userAgreementIsReaded) {
    //   errorsForForm += '\nتوافقنامه را مطالعه کنید';
    //   JJ.jjToast("توافقنامه را مطالعه کنید");
    // }
    // if (!nationalCodeIsValid || !bankAccountIsValid || !userAgreementIsReaded) {
    //   setState(() {});
    //   return;
    // }
    if (form!.validate()) {
      debugPrint('RegisterVIP-Form is valid');
      String params = "do=Access_User.registerVIPUser";
      params += "&id=${JJ.user_id}";
      params += "&user_token=${JJ.user_token}";
      params += "&user_username=${_user_username}";
      params += "&user_email=${_user_email}";
      params += "&user_adminDescription=${user_adminDescriptionController.text}";
      params += "&user_birthdate=${_user_birthdate}";
      params += "&user_city=${_user_city}";
      params += "&user_attachPicPersonnelCard=${JJ.user_attachPicPersonnelCard}";
      errorsForForm = 'در حال ارسال اطلاعات ...';
      setState(() {});

      late Map<String, dynamic> productDetailJSON;
      String result = await JJ().jjAjax('do=Product.getProduct&id=1');
      List<dynamic> temp = jsonDecode(result);
      if (temp.isEmpty) {
        JJ.jjToast('مقادیر فاکتور عضویت VIP دریافت نشد.');
      } else {
        productDetailJSON = temp[0];
      }
      if (productDetailJSON != null) {
        await JJ().jjAjax(params).then((result) async {
          debugPrint("======$result");
          List<dynamic> json = jsonDecode(result);
          if (json[0]['status'] == "0") {
            errorsForForm = json[0]['comment'];
            setState(() {}); // for refresh changes
          } else {
            Navigator.of(context, rootNavigator: true).pop();
            JJ.jjToast('شما به صفحه ی پرداخت هدایت میشوید');
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Payment(
                          productJSON: productDetailJSON,
                          productDetailJSON: productDetailJSON,
                        )));
            setState(() {});
          }
        });
      }
    }
  }

  //This function send request to server to validate CodeMeli and name and family and birthdate
  Future<void> validateUserNameAndFamily() async {
    nationalCodeValidateError = "کمی صبر کنید \n";
    setState(() {});
    final FormState? form = _formKey.currentState;
    form!.validate();
    debugPrint('RegisterVIP-Form is valid');
    if (user_nameController.text.isNotEmpty && user_familyController.text.isNotEmpty) {
      String params = "do=Access_User.validateUserNameAndFamily";
      params += "&id=${JJ.user_id}";
      params += "&user_token=${JJ.user_token}";
      params += "&user_name=${user_nameController.text}";
      params += "&user_family=${user_familyController.text}";
      params += "&user_birthdate=${_user_birthdate}";
      params += "&user_codeMeli=${JJ.user_codeMeli}";
      nationalCodeValidateError = 'در حال ارسال اطلاعات ...';
      setState(() {});
      String result = await JJ().jjAjax(params);
      Map<String, dynamic> json = jsonDecode(result);
      if (json['status'] == "0") {
        nationalCodeValidateError = json['comment'];
        nationalCodeIsValid = false;
      } else {
        JJ.jjToast('احزار هویت انجام شد مراحل بعدی را انجام دهید');
        nationalCodeValidateError = "احزار هویت انجام شد مراحل بعدی را انجام دهید";
        nationalCodeIsValid = true;
      }
      setState(() {}); // for refresh changes
    }
  }

  //This function send request to server to validate CodeMeli and name and family and birthdate
  Future<void> validateUserAccountInformation() async {
    debugPrint(">>>>>>>>>>>>>>>><<<<<<");
    bankAccountValidateError = '';
    if (user_AccountInformationController.text == null || user_AccountInformationController.text.isEmpty) {
      bankAccountValidateError += 'َشماره شبا بانکی درست نیست' + '\n';
      setState(() {});
      return;
    }
    if (user_AccountInformationController.text.length != 24) {
      bankAccountValidateError += 'َشماره شبا باید 24 رقم باشد' + '\n';
      setState(() {});
      return;
    }
    bankAccountValidateError = "کمی صبر کنید \n";
    final FormState? form = _formKey.currentState;
    form!.validate();
    debugPrint('RegisterVIP-Form is valid');
    if (user_nameController.text.isNotEmpty && user_familyController.text.isNotEmpty) {
      String params = "do=Access_User.validateUserAccountInformation";
      params += "&id=${JJ.user_id}";
      params += "&user_token=${JJ.user_token}";
      params += "&user_AccountInformation=${user_AccountInformationController.text}";
      params += "&user_birthdate=${_user_birthdate}";
      params += "&user_codeMeli=${JJ.user_codeMeli}";
      bankAccountValidateError = 'در حال ارسال اطلاعات ...';
      setState(() {});
      String result = await JJ().jjAjax(params);
      Map<String, dynamic> json = jsonDecode(result);
      if (json['status'] == "0") {
        bankAccountValidateError = json['comment'];
        bankAccountIsValid = false;
      } else {
        JJ.jjToast('احزار مالکیت حساب انجام شد مراحل بعدی را انجام دهید');
        bankAccountValidateError = "احزار مالکیت حساب انجام شد مراحل بعدی را انجام دهید";
        bankAccountIsValid = true;
      }
      setState(() {}); // for refresh changes
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Scaffold(
              appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
              // 1st Section =========================================================
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    // 2nd Section =========================================================
                    const SizedBox(
                      height: 10,
                    ),
                    Image.asset(
                      "images/registerVIPHeader.jpg",
                    ),
                    // 3th Section =========================================================
                    const SizedBox(
                      height: 20,
                    ),
                    Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          children: [
                            const Text(
                              "برای شرکت در مزایده ها فرآیند احراز هویت را تکمیل کنید",
                              style: TextStyle(color: Colors.blue, fontSize: 10, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              height: 35,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 3,
                                    child: Text(
                                      "",
                                      style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                    flex: 7,
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "اگر میخواهید ناشناس باشید، نام کاربری دلخواهتان را وارد کنید",
                                        style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                      ),
                                    )),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 3,
                                    child: Text(
                                      "نام فروشگاه(کاربری):",
                                      style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                  flex: 7,
                                  child: TextFormField(
                                    initialValue: JJ.user_username,
                                    textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: const InputDecoration(
                                      border: InputBorder.none,
                                      isDense: true,
                                      filled: true,
                                      fillColor: Colors.black12,
                                      errorStyle: TextStyle(height: 0),
                                    ),
                                    validator: (value) {
                                      errorsForForm = '';
                                      if (value != null && value.isNotEmpty && value.length < 2) {
                                        errorsForForm += 'نام کاربری یا تهی باشد یا از 2 کارکتر کوچکتر نباشد' + '\n';
                                        return '';
                                      }
                                      _user_username = value!;
                                    },
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 3,
                                    child: Text(
                                      "نام:",
                                      style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                  flex: 7,
                                  child: TextFormField(
                                    controller: user_nameController,
                                    readOnly: nationalCodeIsValid,
                                    // if user is vip or Meli code is validate
                                    //if user is not vip yet, he/she can change it
                                    textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: const InputDecoration(
                                      isDense: true,
                                      border: InputBorder.none,
                                      filled: true,
                                      fillColor: Colors.black12,
                                    ),
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        nationalCodeValidateError = "نام را طبق کارت ملی وارد کنید" + "\n";
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 3,
                                    child: Text(
                                      "نام خانوادگی:",
                                      style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                  flex: 7,
                                  child: TextFormField(
                                    controller: user_familyController,
                                    readOnly: nationalCodeIsValid,
                                    // if user is vip or Meli code is validate
                                    //if user is not vip yet, he/she can change it
                                    textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: const InputDecoration(
                                      isDense: true,
                                      border: InputBorder.none,
                                      filled: true,
                                      fillColor: Colors.black12,
                                    ),
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        nationalCodeValidateError += "نام خانوادگی را طبق کارت ملی وارد کنید" + "\n";
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 3,
                                    child: Text(
                                      "کد ملی:",
                                      style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                  flex: 7,
                                  child: TextFormField(
                                    initialValue: JJ.user_codeMeli,
                                    readOnly: true,
                                    textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: const InputDecoration(
                                      isDense: true,
                                      border: InputBorder.none,
                                      filled: true,
                                      fillColor: Colors.black12,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 3,
                                    child: Text(
                                      "تاریخ تولد:",
                                      style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                  flex: 7,
                                  child: InkWell(
                                    onTap: () async {
                                      final DateTime? date = await showPersianDatePicker(
                                        initialDate: _user_birthdate,
                                        context: context,
                                      );
                                      _user_birthdate = date!;
                                      setState(() {});
                                    },
                                    child: Container(
                                      height: 30,
                                      alignment: Alignment.center,
                                      decoration: const BoxDecoration(
                                        color: Colors.black12,
                                      ),
                                      child: Text(_user_birthdate.toPersianDate()),
                                      // ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 3,
                                    child: Text(
                                      "محل سکونت:",
                                      style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                  flex: 7,
                                  child: Container(
                                      height: 30,
                                      decoration: const BoxDecoration(
                                        color: Colors.black12,
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                      ),
                                      child: DropdownSearch<String>(
                                        popupProps: const PopupProps.menu(
                                          showSelectedItems: true,
                                          showSearchBox: true,
                                        ),
                                        items: JJ.listOfCites,
                                        dropdownDecoratorProps: const DropDownDecoratorProps(
                                          textAlign: TextAlign.center,
                                          dropdownSearchDecoration: InputDecoration(
                                            fillColor: Color(0xdde8ac4f),
                                            focusedBorder: InputBorder.none,
                                            floatingLabelAlignment: FloatingLabelAlignment.center,
                                            border: InputBorder.none,
                                            isDense: true,
                                            hintText: "شهرستان یا شهر",
                                          ),
                                        ),
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            errorsForForm += 'شهر محل سکونت خود را انتخاب کنید' + '\n';
                                            return '';
                                          }
                                          _user_city = value;
                                        },
                                        onChanged: (value) {
                                          debugPrint(">>>>>" + value.toString());
                                        },
                                        selectedItem: JJ.user_city.isEmpty ? "تهران" : JJ.user_city,
                                      )),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 3,
                                    child: Text(
                                      "شماره تلفن همراه:",
                                      style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                  flex: 7,
                                  child: TextFormField(
                                    initialValue: JJ.user_mobile,
                                    readOnly: true,
                                    textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: const InputDecoration(
                                      isDense: true,
                                      border: InputBorder.none,
                                      filled: true,
                                      fillColor: Colors.black12,
                                      errorStyle: TextStyle(height: 0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            const Text(
                              "سوابق فروشندگی و خدمات خود را برای بالابردن اعتبار خود ذکر کنید",
                              style: TextStyle(color: Colors.blue, fontSize: 10, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            TextFormField(
                              controller: user_adminDescriptionController,
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              minLines: 5,
                              textAlign: TextAlign.right,
                              textAlignVertical: TextAlignVertical.center,
                              decoration: InputDecoration(
                                isDense: true,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: const BorderSide(
                                    width: 0,
                                    style: BorderStyle.none,
                                  ),
                                ),
                                filled: true,
                                fillColor: Colors.black12,
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 3,
                                    child: Text(
                                      "ایمیل (اختیاری):",
                                      style: TextStyle(color: Colors.black38, fontSize: 10, fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                  flex: 7,
                                  child: TextFormField(
                                    textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: const InputDecoration(
                                      isDense: true,
                                      border: InputBorder.none,
                                      filled: true,
                                      fillColor: Colors.black12,
                                      errorStyle: TextStyle(height: 0),
                                    ),
                                    validator: (value) {
                                      errorsForForm = '';
                                      if (value != null && value.isNotEmpty && !value.contains('@') && value.contains('.')) {
                                        errorsForForm += 'ایمیل یا تهی باشد یا ایمیل صحیح وارد کنید' + '\n';
                                        return '';
                                      }
                                      _user_email = value!;
                                    },
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                InkWell(
                                  onTap: () {
                                    if (!nationalCodeIsValid) {
                                      // if user is not vip or Meli code is validate
                                      validateUserNameAndFamily();
                                    }
                                  },
                                  child: Container(
                                    width: 100,
                                    alignment: Alignment.center,
                                    padding: const EdgeInsets.all(10),
                                    margin: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(color: Color(0xffe8ac4f), borderRadius: BorderRadius.all(Radius.circular(6))),
                                    child: const Text(
                                      "احراز هویت",
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                                const Icon(
                                  Icons.fingerprint,
                                  color: Colors.black87,
                                  size: 40,
                                ),
                                if (nationalCodeIsValid)
                                  const Icon(
                                    Icons.check_box,
                                    color: Colors.green,
                                    size: 35,
                                  )
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              nationalCodeValidateError,
                              style: TextStyle(color: nationalCodeIsValid ? Colors.green : Colors.red, fontSize: 12, height: 2),
                            ),
                            const SizedBox(
                              height: 10,
                            ),

                            //Bank acount number =================================================
                            const Divider(
                              color: Colors.black45,
                            ),
                            const Align(
                              alignment: Alignment.topRight,
                              child: Text(
                                "شماره شبا بانکی (اجباری):",
                                style: TextStyle(
                                  color: Colors.black38,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                  height: 3,
                                ),
                              ),
                            ),
                            Directionality(
                              textDirection: TextDirection.ltr,
                              child: TextFormField(
                                controller: user_AccountInformationController,
                                readOnly: JJ.user_grade == "VIP" || bankAccountIsValid,
                                keyboardType: TextInputType.number,
                                textDirection: TextDirection.ltr,
                                textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: const InputDecoration(
                                  prefixText: "IR",
                                  prefixStyle: TextStyle(),
                                  hintText: "شماره شبا دقیق و فقط بصورت عدد",
                                  hintStyle: TextStyle(fontSize: 11),
                                  isDense: true,
                                  filled: true,
                                  fillColor: Colors.black12,
                                  errorStyle: TextStyle(height: 0),
                                ),
                                onChanged: (value) {
                                  if (value != Null) {
                                    user_AccountInformationController.text = value.toString().replaceAll(RegExp("\\D"), ""); //Remove all non-numeric characters
                                    user_AccountInformationController.selection = TextSelection.collapsed(offset: user_AccountInformationController.text.length);
                                  }
                                },
                                validator: (value) {
                                  bankAccountValidateError = '';
                                  if (value == null || value.isEmpty || value.contains(RegExp("\\D"))) {
                                    bankAccountValidateError += 'َشماره شبا بانکی درست نیست' + '\n';
                                    return '';
                                  }
                                  if (value.length != 24) {
                                    bankAccountValidateError += 'َشماره شبا باید 24 رقم باشد' + '\n';
                                    return '';
                                  }
                                },
                              ),
                            ),
                            const Text(
                              textAlign: TextAlign.center,
                              "کاربر عزیز شماره شبای وارد شده ی فوق باید با مشخصات فردی شما مطابقت داشته باشد.",
                              style: TextStyle(color: Colors.black45, fontSize: 12, height: 2),
                            ),
                            InkWell(
                              onTap: () {
                                if (!bankAccountIsValid) {
                                  validateUserAccountInformation();
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 100,
                                    alignment: Alignment.center,
                                    padding: const EdgeInsets.all(10),
                                    margin: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(color: Color(0xffe8ac4f), borderRadius: BorderRadius.all(Radius.circular(6))),
                                    child: const Text(
                                      "ثبت شبا",
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  const Icon(
                                    Icons.credit_card,
                                    color: Colors.black87,
                                    size: 40,
                                  ),
                                  if (bankAccountIsValid)
                                    const Icon(
                                      Icons.check_box,
                                      color: Colors.green,
                                      size: 35,
                                    )
                                ],
                              ),
                            ),
                            Text(
                              bankAccountValidateError,
                              style: TextStyle(color: (bankAccountIsValid) ? Colors.green : Colors.red, fontSize: 12, height: 2),
                            ),
                            const Divider(
                              color: Colors.black45,
                            ),
                            //=============================================================================
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                InkWell(
                                  onTap: () {
                                    //Show rules and conditions
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => const jjAlertDialog(
                                              type: '',
                                              richText: Text(''
                                                  'دوست عزیزم ما'
                                                  ' بخاطر حمایت از شما، صنف آنتیک ، توسعه و رونق این بازار و همچنین تجمع همه علاقه مندان به اجناس قدیمی '
                                                  'در یک فضای اختصاصی و به روز اقدام به راه اندازی این اپلیکیشن نموده ایم که هزینه ی زیادی برای ما'
                                                  ' در برداشته لذا از شما خواهشمندیم بنابراعتقادات صحیح اخلاقی و انسانی و برخورداری از پشتیبانی همه جانبه ما  متعهد شوید که معامله اجناسی را که در این اپلیکیشن قرار میدهید از راه درست و فقط از این طریق (وتمام) انجام دهید و همه آنتیک دوستان را در پیشبرد اهداف این'
                                                  ' بازار یاری کنید. در غیراینصورت حرام و ما هیچ  گونه رضایتی از معامله شما نداریم .'
                                                  '\n'
                                                  '\n'
                                                  'یا رزاق'
                                                  ''),
                                              title: ' مطالعه این تعهدنامه الزامیست:',
                                            ));
                                    ;
                                    // Single tapped.
                                    setState(() {
                                      userAgreementIsReaded = true; // Make icon green
                                      userAgreementReadError = ''; //Clear error if it has before
                                    });
                                  },
                                  child: Container(
                                    width: 100,
                                    alignment: Alignment.center,
                                    padding: const EdgeInsets.all(10),
                                    margin: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(color: Color(0xffe8ac4f), borderRadius: BorderRadius.all(Radius.circular(6))),
                                    child: const Text(
                                      "تعهد نامه",
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                                const Icon(
                                  Icons.event_note_outlined,
                                  color: Colors.black87,
                                  size: 40,
                                ),
                                if (userAgreementIsReaded)
                                  const Icon(
                                    Icons.check_box,
                                    color: Colors.green,
                                    size: 35,
                                  )
                              ],
                            ),
                          ],
                        )),
                    const Divider(
                      color: Colors.black45,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text("${JJ.publicConfigs['config_vipPriceComments']} "),
                    Text(""),
                    Text(
                      "${JJ.jjSeRagham(JJ.publicConfigs['config_vipPrice'].toString())} تومان ",
                      style: const TextStyle(color: Colors.green),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {
                        validateAndSave();
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          margin: const EdgeInsets.only(bottom: 20),
                          alignment: Alignment.center,
                          height: 45,
                          width: 150,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: Colors.black,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                "images/saleStep2.png",
                                scale: 8,
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              const Text(
                                "پرداخت",
                                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Color(0xffe8ac4f)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // 4th Section all Galleries  ==================================
                    Text(
                      errorsForForm,
                      style: const TextStyle(
                        fontSize: 12,
                        height: 2,
                        color: Colors.red,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
              // Footer================================================
              bottomNavigationBar: const NavBarBottom())),
    );
  }
}
