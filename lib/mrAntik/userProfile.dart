import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'registerVIP.dart';
import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/allertDialog.dart';
import 'package:dropdown_search/dropdown_search.dart';

class UserProfile extends StatefulWidget {
  UserProfile({Key? key}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  String errorsForForm = "";
  String _user_username = "";
  String _user_name = "";
  String _user_family = "";
  String _user_codeMeli = "";
  String _user_mobile = "";
  String _user_city = "";
  String _user_badge = "";
  String _user_birthdate = "";
  String _user_pic1 = "";
  String _user_pic2 = "";
  String _user_credit = "";
  String _user_address = "";
  String _user_jensiat = "";
  String _user_attachPicPersonal = "";
  String _user_AccountInformation = "";
  String _user_postalCode = "";
  String _user_attachFileUser = "";
  String _user_email = "";

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  //This function send request to server
  Future<void> validateAndSave() async {
    final FormState? form = _formKey.currentState;
    if (form!.validate()) {
      debugPrint('Form is valid');
      String params = "do=Access_User.updateUserProfile";
      params += "&user_username=${_user_username}";
      params += "&user_name=${_user_name}";
      params += "&user_family=${_user_family}";
      // params += "&user_codeMeli=${_user_codeMeli}";
      // params += "&user_mobile=${_user_mobile}";
      params += "&user_AccountInformation=${_user_AccountInformation}";
      params += "&user_email=${_user_email}";
      params += "&user_city=${_user_city}";
      params += "&user_postalCode=${_user_postalCode}";
      params += "&user_address=${_user_address}";
      errorsForForm = 'در حال ارسال اطلاعات ...';
      setState(() {});

      await JJ().jjAjax(params).then((result) async {
        debugPrint("======$result");
        Map<String, dynamic> json = jsonDecode(result);
        SharedPreferences? localStorage;
        localStorage = await SharedPreferences.getInstance();
        if (json['status'] == "0") {
          errorsForForm = json['comment'];
          setState(() {}); // for refresh changes
        } else {
          JJ.user_address = _user_address;
          JJ.user_postalCode = _user_postalCode;
          JJ.user_city = _user_city;
          JJ.user_email = _user_email;
          Navigator.pop(context);
          errorsForForm = json['comment'];
          JJ.jjToast(json['comment']);
          setState(() {});
        }
      });
    } else {
      setState(() {});
      debugPrint('Form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.black,
          centerTitle: true,
          title: const Header()),
      body: Form(
        key: _formKey,
        child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(
                  color: const Color(0xfef7edff),
                  borderRadius: BorderRadius.circular(8.0),
                  border: Border.all(color: Colors.white54),
                ),
                child: Column(
                  children: [
                    if (JJ.user_grade !=
                        "VIP") // If user is not VIP show button to become vip for auctions
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const RegisterVIP()));
                          },
                          child: Image.asset(
                            "images/AuctionHeader.png",
                          ),
                        ),
                      ),
                    if (JJ.user_grade ==
                        "VIP") // If

                    const SizedBox(
                      height: 20,
                    ),

                    Stack(
                      children: [
                        Container(
                          height: 30,
                          margin: const EdgeInsets.only(
                              left: 30, right: 5, bottom: 10),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(
                                "images/textfieldBackGroundGold.png",
                              ),
                            ),
                          ),
                          child: Row(
                            children: [
                              const Expanded(
                                flex: 2,
                                child: Text(
                                  textAlign: TextAlign.center,
                                  "نام کاربری: ",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 14),
                                ),
                              ),
                              Expanded(
                                flex: 4,
                                child: TextFormField(
                                  initialValue: JJ.user_username,
                                  textAlign: TextAlign.center,
                                  textAlignVertical: TextAlignVertical.center,
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.only(bottom: 20),
                                    errorStyle: TextStyle(
                                      height: 0,
                                    ),
                                  ),
                                  validator: (value) {
                                    errorsForForm = '';
                                    if (value != null &&
                                        value.isNotEmpty &&
                                        value.length < 2) {
                                      errorsForForm +=
                                          'نام کاربری یا تهی باشد یا از 2 کارکتر کوچکتر نباشد' +
                                              '\n';
                                      return '';
                                    }
                                    _user_username = value!;
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          left: 5,
                          top: 5,
                          child: InkWell(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  useSafeArea: true,
                                  builder: (_) => const jjAlertDialog(
                                        type: '',
                                        richText: Text(''
                                            'نام کاربری شما معرف شما در فضای معاملات اپلیکیشن میباشد که اختیاری میباشد در صورت پرنکردن ، نام و نام خانوادگی شما به کاربران نشان داده میشود.'),
                                        title: 'نام کاربری چیست:',
                                      ));
                            },
                            child: const CircleAvatar(
                              radius: 10,
                              backgroundColor: Colors.lightBlueAccent,
                              child: Icon(
                                Icons.question_mark_rounded,
                                color: Colors.white,
                                size: 13,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(
                          left: 30, right: 5, bottom: 10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textfieldBackGroundGold.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 2,
                            child: Text(
                              textAlign: TextAlign.center,
                              "نام: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: TextFormField(
                              initialValue: JJ.user_name,
                              textAlign: TextAlign.center,
                              textAlignVertical: TextAlignVertical.center,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(bottom: 20),
                                errorStyle: TextStyle(
                                  height: 0,
                                ),
                              ),
                              validator: (value) {
                                String pattern = r'[\u0600-\u06FF]+';
                                RegExp regex = RegExp(pattern);
                                if (value == null || value.isEmpty) {
                                  errorsForForm =
                                      "وارد کردن نام طبق کارت ملی الزامی است" +
                                          '\n';
                                  return '';
                                } else if (!regex.hasMatch(value)) {
                                  errorsForForm +=
                                      "نام واقعی کاربر طبق کارت ملی" + '\n';
                                  return '';
                                }
                                _user_name = value;
                              },
                            ),
                          ),
                          const Text(
                            " * ",
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(
                          left: 30, right: 5, bottom: 10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textfieldBackGroundGold.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 2,
                            child: Text(
                              textAlign: TextAlign.center,
                              "نام خانوادگی: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: TextFormField(
                              initialValue: JJ.user_family,
                              textAlign: TextAlign.center,
                              textAlignVertical: TextAlignVertical.center,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(bottom: 20),
                                errorStyle: TextStyle(
                                  height: 0,
                                ),
                              ),
                              validator: (value) {
                                String pattern = r'[\u0600-\u06FF]+';
                                RegExp regex = RegExp(pattern);
                                if (value == null || value.isEmpty) {
                                  errorsForForm +=
                                      "وارد کردن نام خانوادگی طبق کارت ملی الزامی است" +
                                          '\n';
                                  return '';
                                } else if (!regex.hasMatch(value)) {
                                  errorsForForm +=
                                      'نام خانوادگی کاربر طبق کارت ملی' + '\n';
                                  return '';
                                }
                                _user_family = value;
                              },
                            ),
                          ),
                          const Text(
                            " * ",
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    Stack(children: [
                      Container(
                        height: 30,
                        margin: const EdgeInsets.only(
                            left: 30, right: 5, bottom: 10),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                              "images/textfieldBackGroundGold.png",
                            ),
                          ),
                        ),
                        child: Row(
                          children: [
                            const Expanded(
                              flex: 2,
                              child: Text(
                                textAlign: TextAlign.center,
                                "کد ملی: ",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Text(JJ.user_codeMeli,
                                  textAlign: TextAlign.center),
                            ),
                            const Text(
                              " * ",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Positioned(
                        left: 5,
                        top: 5,
                        child: InkWell(
                          onTap: () {
                            showDialog(
                                context: context,
                                useSafeArea: true,
                                builder: (_) => const jjAlertDialog(
                                      type: '',
                                      richText: Text(''
                                          'دقت کنید کادر های ستاره دار را که متعلق به مشخصات فردی شماست صحیح وارد کنید، زیرا غیر قابل ویرایش است و فقط از طریق پشتیبانی تغییر می کند'),
                                      title: 'کد ملی در وتمام:',
                                    ));
                          },
                          child: const CircleAvatar(
                            radius: 10,
                            backgroundColor: Colors.lightBlueAccent,
                            child: Icon(
                              Icons.question_mark_rounded,
                              color: Colors.white,
                              size: 13,
                            ),
                          ),
                        ),
                      ),
                    ]),

                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(
                          left: 30, right: 5, bottom: 10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textfieldBackGroundGold.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 2,
                            child: Text(
                              textAlign: TextAlign.center,
                              "شماره همراه: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                          Expanded(
                              flex: 4,
                              child: Text(
                                JJ.user_mobile,
                                textAlign: TextAlign.center,
                              )),
                          const Text(
                            " * ",
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(
                          left: 30, right: 5, bottom: 10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textfieldBackGroundGold.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 2,
                            child: Text(
                              textAlign: TextAlign.center,
                              "شماره شبا بانکی: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 13),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                  JJ.user_AccountInformation,
                                style: TextStyle( fontSize: 13),
                              ),
                            ),
                          ),
                      const Text(
                        " * ",
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      )
                        ],
                      ),
                    ),
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(
                          left: 30, right: 5, bottom: 10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textfieldBackGroundGold.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 2,
                            child: Text(
                              textAlign: TextAlign.center,
                              "ایمیل: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: TextFormField(
                              initialValue: JJ.user_email,
                              textAlign: TextAlign.center,
                              textAlignVertical: TextAlignVertical.center,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(bottom: 20),
                                errorStyle: TextStyle(
                                  height: 0,
                                ),
                              ),
                              validator: (value) {
                                _user_email = value!;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    // city = شهر
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(
                          left: 30, right: 5, bottom: 10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textfieldBackGroundGold.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 2,
                            child: Text(
                              textAlign: TextAlign.center,
                              "شهر: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: DropdownSearch<String>(
                              popupProps: const PopupProps.menu(
                                showSelectedItems: true,
                                showSearchBox: true,
                              ),
                              items: JJ.listOfCites,
                              dropdownDecoratorProps: const DropDownDecoratorProps(
                                textAlign: TextAlign.center,
                                baseStyle: TextStyle(height: 0,),
                                dropdownSearchDecoration: InputDecoration(
                                  fillColor: Color(0xdde8ac4f),
                                  focusedBorder: InputBorder.none,
                                  floatingLabelAlignment: FloatingLabelAlignment.center,
                                  border: InputBorder.none,
                                  isDense: true,
                                  hintText: "شهرستان یا شهر",
                                ),
                              ),

                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  errorsForForm +=
                                      'شهر محل سکونت خود را انتخاب کنید' + '\n';
                                  return '';
                                }
                                _user_city = value;
                              },
                              onChanged: (value) {
                                debugPrint(">>>>>" + value.toString());
                              },
                              selectedItem: JJ.user_city.isEmpty?"تهران" : JJ.user_city,
                            )
                            ,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(
                          left: 30, right: 5, bottom: 10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textfieldBackGroundGold.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 2,
                            child: Text(
                              textAlign: TextAlign.center,
                              "کد پستی: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              initialValue: JJ.user_postalCode,
                              textAlign: TextAlign.center,
                              textAlignVertical: TextAlignVertical.center,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(bottom: 20),
                                errorStyle: TextStyle(
                                  height: 0,
                                ),
                              ),
                              validator: (value) {
                                _user_postalCode = value!;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(
                          left: 30, right: 5, bottom: 0),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textfieldBackGroundGold.png",
                          ),
                        ),
                      ),
                      child: const Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Text(
                              textAlign: TextAlign.center,
                              "نشانی",
                              style:
                              TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: Text("")
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                          left: 30, right: 5, bottom: 0),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8),bottomRight: Radius.circular(8)),
                        color: Color(0xffe8ac4f),
                      ),
                      child: TextFormField(
                        initialValue: JJ.user_address,
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        minLines: 5,
                        decoration: const InputDecoration(
                          focusedBorder: InputBorder.none,
                          contentPadding: EdgeInsets.all(10),
                          errorStyle: TextStyle(
                            height: 0,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.length < 10) {
                            errorsForForm +=
                                'در قسمت آدرس دست کم 20 حرف وارد کنید' +
                                    '\n';
                            return '';
                          }
                          _user_address = value;
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const SizedBox(height: 15),
                    Text(
                      errorsForForm,
                      style: const TextStyle(
                        fontSize: 12,
                        height: 2,
                        color: Colors.red,
                      ),
                    ),
                    const SizedBox(height: 15),
                    InkWell(
                      onTap: validateAndSave,
                      child: Container(
                        alignment: Alignment.center,
                        width: 120,
                        padding: const EdgeInsets.all(8),
                        margin: const EdgeInsets.all(8),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          color: Colors.black38,
                        ),
                        child: const Text(
                          "ثبت ویرایش",
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
      ),
    );
  }
}
