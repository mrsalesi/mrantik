import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vaTamam/mrAntik/rules.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'package:persian_datetimepickers/persian_datetimepickers.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

import 'paymentOK.dart';
import 'tools/jjTools.dart';

class PaymentAndFactor extends StatefulWidget {
  final Map<String, dynamic> paymentAndFacotrJSON;

  const PaymentAndFactor({
    Key? key,
    required this.paymentAndFacotrJSON,
  }) : super(key: key);

  @override
  State<PaymentAndFactor> createState() => _PaymentAndFactorState();
}

class _PaymentAndFactorState extends State<PaymentAndFactor> {
  final GlobalKey<FormState> _formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKey3 = GlobalKey<FormState>();
  String errorsForForm = "";
  String postalBarcode = "";
  String postArrivalTime = "";
  String postBillAttach = "";
  String beforSendingClip = "";
  String rejectPostBillAttach = "";
  String rejectPostalBarcode = "";
  String uploadingText = "";
  final TextEditingController postalBarcodeController = TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final TextEditingController postalCodeController = TextEditingController();
  final TextEditingController customerCommentsController = TextEditingController();
  final TextEditingController rejectPstalBarcodeController = TextEditingController();
  String satisfyRating = "3";
  String customerIsSatisfied = "";
  String unSatisfiedReason = "";
  late VideoPlayerController videoController = VideoPlayerController.network("${JJ.server}upload/");
  late Future<void> videoPlayerFuture;

  bool unSatisfiedReason1 = false;
  bool unSatisfiedReason2 = false;
  bool unSatisfiedReason3 = false;
  bool unSatisfiedReason4 = false;
  String unSatisfiedPic1 = "";
  String unSatisfiedPic2 = "";
  String unSatisfiedPic3 = "";

  bool userInSessionIsSeller = false; // if seller user open this page switch seller and customer header
  int saleStep = 1; //pre invoice

  @override
  void dispose() {
    videoController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    debugPrint('PaymentAndFactor.initState()....');
    super.initState();
    postBillAttach = widget.paymentAndFacotrJSON!['product_factor_postBillAttach'];
    beforSendingClip = widget.paymentAndFacotrJSON!['product_factor_beforSendingClip'];
    if (beforSendingClip.isNotEmpty) {
      // videoController = VideoPlayerController.network("https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_20mb.mp4");
      videoController = VideoPlayerController.network("${JJ.server}upload/$beforSendingClip");
      debugPrint("~~~~~~~~~~>>>>${JJ.server}upload/$beforSendingClip");
      videoPlayerFuture = videoController.initialize();
      setState(() {});
    }
    // else {
    //   videoController = VideoPlayerController.file(File());
    // }

    postalBarcodeController.text = widget.paymentAndFacotrJSON!['product_factor_postalBarcode'];
    // اگر در مرحله ی اولیه نبود این موارد نباید ست بشود
    if (widget.paymentAndFacotrJSON!['product_factor_status'] == 'پرداخت نشده') {
      saleStep = 0;
    } else if (widget.paymentAndFacotrJSON!['product_factor_status'] == 'پرداخت شده') {
      saleStep = 2;
    } else if (widget.paymentAndFacotrJSON!['product_factor_status'] == 'ارسال شده') {
      saleStep = 3;
    } else if (widget.paymentAndFacotrJSON!['product_factor_status'] == 'تحویل داده شده') {
      saleStep = 4;
    } else if (widget.paymentAndFacotrJSON!['product_factor_status'] == 'تایید خریدار') {
      saleStep = 5;
    } else if (widget.paymentAndFacotrJSON!['product_factor_status'] == 'رد خریدار') {
      saleStep = 5;
    } else if (widget.paymentAndFacotrJSON!['product_factor_status'] == 'پایان فروش') {
      saleStep = 6;
    }
    //خط زیر برای مقدار دهی موقغ دیدن خریدار یا مشاهده مجدد فروشنده است
    // postalBarcodeController.text();
    //check user in session is seller or customer
    debugPrint(widget.paymentAndFacotrJSON['product_creator'] + ' : ' + JJ.user_id);
    if (widget.paymentAndFacotrJSON['product_creator'] == JJ.user_id) {
      //That means user in session is seller for this factor
      userInSessionIsSeller = true;
    }
    addressController.text = widget.paymentAndFacotrJSON['product_factor_address'];
    postalCodeController.text = widget.paymentAndFacotrJSON['product_factor_postalCode'];
    satisfyRating = widget.paymentAndFacotrJSON['product_factor_satisfyRating'];
    customerCommentsController.text = widget.paymentAndFacotrJSON['product_factor_customerComments'];
    customerIsSatisfied = widget.paymentAndFacotrJSON['product_factor_customerIsSatisfied'];
    postArrivalTime = widget.paymentAndFacotrJSON['product_factor_postArrivalTime'];
    unSatisfiedPic1 = widget.paymentAndFacotrJSON['product_factor_unSatisfiedPic1'];
    unSatisfiedPic2 = widget.paymentAndFacotrJSON['product_factor_unSatisfiedPic2'];
    unSatisfiedPic3 = widget.paymentAndFacotrJSON['product_factor_unSatisfiedPic3'];
  }

  Future<void> insertCustomerIsSatisfied() async {
    String params = "do=Factor.insertCustomerIsSatisfied";
    params += "&id=${widget.paymentAndFacotrJSON['payment_factor_id']}";
    params += "&product_factor_satisfyRating=$satisfyRating";
    params += "&product_factor_customerIsSatisfied=1";
    params += "&product_factor_customerComments=${customerCommentsController.text}";
    params += "&product_factor_serialNumber=${JJ.user_token}";
    errorsForForm = 'در حال ارسال اطلاعات ...';
    setState(() {});
    String result = await JJ().jjAjax(params);
    Map<String, dynamic> json = jsonDecode(result);
    if (json['status'] == "0") {
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {}); // for refresh changes
      // JJ.user_token = "";
      // localStorage.setString("user_token", "");
    } else {
      //If price is successfully added and if autoIncreasePrice is set by user must save id in local storage to show in form for next visist of auction
      saleStep = 5;
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {});
    }
  }

  Future<void> insertCustomerIsNotSatisfied() async {
    if (unSatisfiedPic1.isEmpty || unSatisfiedPic2.isEmpty || unSatisfiedPic3.isEmpty) {
      errorsForForm = 'هر 3 تصویر از نقاط ضعف کالا را بارگذاری کنید';
      setState(() {});
      return;
    }
    if (unSatisfiedReason1 == false && unSatisfiedReason2 == false && unSatisfiedReason3 == false && unSatisfiedReason4 == false) {
      errorsForForm = 'دلایل مرجوع کردن کالا را تیک بزنید';
      setState(() {});
      return;
    }
    String params = "do=Factor.insertCustomerIsNotSatisfied";
    params += "&id=${widget.paymentAndFacotrJSON['payment_factor_id']}";
    params += "&product_factor_satisfyRating=$satisfyRating";
    params += "&product_factor_customerIsSatisfied=0";
    params += "&product_factor_customerComments=${customerCommentsController.text}";
    params += "&product_factor_unSatisfiedReason1=$unSatisfiedReason1";
    params += "&product_factor_unSatisfiedReason2=$unSatisfiedReason2";
    params += "&product_factor_unSatisfiedReason3=$unSatisfiedReason3";
    params += "&product_factor_unSatisfiedReason4=$unSatisfiedReason4";
    params += "&product_factor_unSatisfiedPic1=$unSatisfiedPic1";
    params += "&product_factor_unSatisfiedPic2=$unSatisfiedPic2";
    params += "&product_factor_unSatisfiedPic3=$unSatisfiedPic3";
    params += "&product_factor_serialNumber=${widget.paymentAndFacotrJSON['product_factor_serialNumber']}";
    errorsForForm = 'در حال ارسال اطلاعات ...';
    setState(() {});
    String result = await JJ().jjAjax(params);
    Map<String, dynamic> json = jsonDecode(result);
    if (json['status'] == "0") {
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {}); // for refresh changes
      // JJ.user_token = "";
      // localStorage.setString("user_token", "");
    } else {
      //If price is successfully added and if autoIncreasePrice is set by user must save id in local storage to show in form for next visist of auction
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {});
    }
  }

  Future<void> insertDeliveredByCustomer() async {
    debugPrint('Form is valid');
    String params = "do=Factor.insertDeliveredByCustomer";
    params += "&id=${widget.paymentAndFacotrJSON['payment_factor_id']}";
    errorsForForm = 'در حال ارسال اطلاعات ...';
    setState(() {});
    String result = await JJ().jjAjax(params);
    Map<String, dynamic> json = jsonDecode(result);
    if (json['status'] == "0") {
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {}); // for refresh changes
      // JJ.user_token = "";
      // localStorage.setString("user_token", "");
    } else {
      //If price is successfully added
      saleStep = 4;
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {});
    }
  }

  /// This function sent post bill from seller to invoice customer
  Future<void> insertPostBill() async {
    postalBarcode = postalBarcodeController.text ?? '';
    if (postalBarcode.isEmpty) {
      setState(() {
        errorsForForm = ' کد رهگیری یا سریال رسید یا بارکد فیش پستی را وارد کنید';
      });
      return;
    }
    if (postArrivalTime.isEmpty) {
      setState(() {
        errorsForForm = 'تاریخ رسیدن مرسوله به دست خریدار را بصورت تقریبی وارد کنید';
      });
      return;
    }
    if (postBillAttach.isEmpty) {
      setState(() {
        errorsForForm = 'بارگذاری تصویر رسید پستی یا اسکرین شات پیامک پست الزامی است';
      });
      return;
    }
    if (beforSendingClip.isEmpty) {
      setState(() {
        errorsForForm = 'بارگذاری یک ویدئو کامل و کوتاه از تمام جزئیات کالا الزامی است';
      });
      return;
    }
    debugPrint('Form is valid');
    String params = "do=Factor.insertPostalBill";
    params += "&id=${widget.paymentAndFacotrJSON['payment_factor_id']}";
    params += "&product_factor_postalBarcode=$postalBarcode";
    params += "&product_factor_postBillAttach=${postBillAttach}";
    params += "&product_factor_beforSendingClip=${beforSendingClip}";
    params += "&product_factor_postArrivalTime=${postArrivalTime}";
    params += "&product_factor_serialNumber=${widget.paymentAndFacotrJSON['product_factor_serialNumber']}";
    errorsForForm = 'در حال ارسال اطلاعات ...';
    setState(() {});
    String result = await JJ().jjAjax(params);
    Map<String, dynamic> json = jsonDecode(result);
    if (json['status'] == "0") {
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {}); // for refresh changes
      // JJ.user_token = "";
      // localStorage.setString("user_token", "");
    } else {
      //If price is successfully added and if autoIncreasePrice is set by user must save id in local storage to show in form for next visist of auction
      saleStep = 3;
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {});
    }
  }

  /// This function sent post bill from seller to invoice customer
  Future<void> insertRejectPostBill() async {
    rejectPostalBarcode = rejectPstalBarcodeController.text ?? '';
    if (rejectPostalBarcode.isEmpty) {
      setState(() {
        errorsForForm = ' کد رهگیری یا سریال رسید یا بارکد فیش پستی را وارد کنید';
      });
      return;
    }
    if (rejectPostBillAttach.isEmpty) {
      setState(() {
        errorsForForm = 'بارگذاری تصویر رسید پستی یا اسکرین شات پیامک پست الزامی است';
      });
      return;
    }

    debugPrint('Form is valid');
    String params = "do=Factor.insertRejectPostInfo";
    params += "&id=${widget.paymentAndFacotrJSON['payment_factor_id']}";
    params += "&product_factor_rejectPostBillAttach=$rejectPostBillAttach";
    params += "&product_factor_rejectPostalBarcode=${rejectPostalBarcode}";
    params += "&product_factor_serialNumber=${widget.paymentAndFacotrJSON['product_factor_serialNumber']}";
    errorsForForm = 'در حال ارسال اطلاعات ...';
    setState(() {});
    String result = await JJ().jjAjax(params);
    Map<String, dynamic> json = jsonDecode(result);
    if (json['status'] == "0") {
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {}); // for refresh changes
      // JJ.user_token = "";
      // localStorage.setString("user_token", "");
    } else {
      //If price is successfully added and if autoIncreasePrice is set by user must save id in local storage to show in form for next visist of auction
      saleStep = 6;
      errorsForForm = json['comment'];
      JJ.jjToast(json['comment']);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Scaffold(
            appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
            // 1st Section Status of sale=========================================================
            body: SingleChildScrollView(
                child: Column(
              children: [
                Container(
                  height: 50,
                  padding: const EdgeInsets.all(10),
                  color: Colors.black87,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        (saleStep >= 1) ? "images/saleStep1.png" : "images/saleStepPre1.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        (saleStep >= 2) ? "images/saleStep2.png" : "images/saleStepPre2.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        (saleStep >= 3) ? "images/saleStep3.png" : "images/saleStepPre3.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        (saleStep >= 4) ? "images/saleStep4.png" : "images/saleStepPre4.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        (saleStep >= 5) ? "images/saleStep5.png" : "images/saleStepPre5.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        (saleStep >= 6) ? "images/saleStep6.png" : "images/saleStepPre6.png",
                        scale: 7,
                      ),
                    ],
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Column(children: [
                      const SizedBox(
                        height: 10,
                      ),
                      const Center(
                        child: Text(
                          'فاکتور فروش',
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.only(left: 2, top: 4, bottom: 4),
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(Radius.circular(8)),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                            ),
                            child: Column(
                              children: [
                                Align(
                                    alignment: Alignment.topRight,
                                    child: Image.asset(
                                      'images/LabelCostomer.png',
                                      scale: 7,
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.person_rounded,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        ' ${widget.paymentAndFacotrJSON['customer_name']} ${widget.paymentAndFacotrJSON['customer_family']} ',
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.credit_card,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        " ${widget.paymentAndFacotrJSON['customer_codeMeli']} ",
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8, bottom: 10),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.location_on,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        " ${widget.paymentAndFacotrJSON['customer_city']} ",
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )),
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.only(right: 2, top: 4, bottom: 4),
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(Radius.circular(8)),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                            ),
                            child: Column(
                              children: [
                                Align(
                                    alignment: Alignment.topRight,
                                    child: Image.asset(
                                      "images/LabelSeller.png",
                                      scale: 7,
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.person_rounded,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        ' ${widget.paymentAndFacotrJSON['seller_name']} ${widget.paymentAndFacotrJSON['seller_family']} ',
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.credit_card,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        " ${widget.paymentAndFacotrJSON['seller_codeMeli']} ",
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8, bottom: 10),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.location_on,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        " ${widget.paymentAndFacotrJSON['seller_city']} ",
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )),
                        ],
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          border: Border.all(
                            color: Colors.black26,
                          ),
                        ),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 2,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                textAlign: TextAlign.right,
                                                ' ${widget.paymentAndFacotrJSON['product_name']}',
                                                style: const TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Align(
                                                child: Text(
                                                  textAlign: TextAlign.right,
                                                  " کد:${widget.paymentAndFacotrJSON['product_factor_productId']} \n ${widget.paymentAndFacotrJSON['product_factor_title']}",
                                                  style: const TextStyle(
                                                    fontSize: 12,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Text(
                                        textDirection: TextDirection.rtl,
                                        softWrap: true,
                                        textAlign: TextAlign.right,
                                        "مشخصات کالا:",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        widget.paymentAndFacotrJSON['products_content'],
                                        softWrap: true,
                                        textAlign: TextAlign.justify,
                                        style: const TextStyle(
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                            Expanded(
                                flex: 1,
                                child: Container(
                                    padding: const EdgeInsets.all(4),
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                    ),
                                    child: Image.network(
                                      '${JJ.server}upload/${widget.paymentAndFacotrJSON['product_pic1']}',
                                      fit: BoxFit.fitWidth,
                                    ))),
                          ],
                        ),
                      ),
                      //Price container========================================
                      Container(
                        margin: const EdgeInsets.only(top: 5, bottom: 5),
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          border: Border.all(
                            color: Colors.black26,
                          ),
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 15, left: 5, right: 5),
                                    child: Image.asset("images/saleStep1.png", alignment: Alignment.centerLeft),
                                  ),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    height: 30,
                                    padding: const EdgeInsets.all(0),
                                    margin: const EdgeInsets.only(top: 20, left: 10, bottom: 2),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black54),
                                      borderRadius: const BorderRadius.all(Radius.circular(4)),
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 4,
                                          child: Container(
                                            height: 30,
                                            alignment: Alignment.center,
                                            child: const Text(
                                              " قیمت کالا:",
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                            flex: 4,
                                            child: Container(
                                              padding: const EdgeInsets.only(right: 10),
                                              alignment: Alignment.centerRight,
                                              height: 30,
                                              color: const Color(0xfff8e9d6),
                                              child: Text(
                                                textDirection: TextDirection.ltr,
                                                widget.paymentAndFacotrJSON['product_price1'].toString().seRagham(),
                                                style: const TextStyle(fontSize: 15),
                                              ),
                                            )),
                                        Expanded(
                                            flex: 1,
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(4), topLeft: Radius.circular(4)),
                                                color: Color(0xfff8e9d6),
                                              ),
                                              alignment: Alignment.center,
                                              height: 30,
                                              child: const Text(
                                                "(تومان)",
                                                style: TextStyle(fontSize: 10),
                                              ),
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                const Expanded(
                                  flex: 1,
                                  child: Text(""),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    height: 30,
                                    padding: const EdgeInsets.all(0),
                                    margin: const EdgeInsets.only(top: 20, left: 10, bottom: 20),
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(4)),
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 4,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: widget.paymentAndFacotrJSON['payment_status'] == 'پرداخت شده' ? Colors.greenAccent : const Color(0xfff8e9d6),
                                              borderRadius: const BorderRadius.only(topRight: Radius.circular(4), bottomRight: Radius.circular(4)),
                                            ),
                                            height: 30,
                                            alignment: Alignment.center,
                                            child: Text(
                                              widget.paymentAndFacotrJSON['payment_status'],
                                              style: const TextStyle(fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                            flex: 4,
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: AssetImage(
                                                    "images/textFielsBg.png",
                                                  ),
                                                ),
                                              ),
                                              padding: const EdgeInsets.only(right: 10),
                                              alignment: Alignment.centerRight,
                                              height: 30,
                                              child: Text(
                                                textDirection: TextDirection.ltr,
                                                (widget.paymentAndFacotrJSON['payment_amount'].toString().seRagham()).toString(),
                                                style: const TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                            )),
                                        Expanded(
                                            flex: 1,
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: AssetImage(
                                                    "images/textFielsBg.png",
                                                  ),
                                                ),
                                                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(4), topLeft: Radius.circular(4)),
                                                color: Color(0xfff8e9d6),
                                              ),
                                              alignment: Alignment.center,
                                              height: 30,
                                              child: const Text(
                                                "(تومان)",
                                                style: TextStyle(fontSize: 10),
                                              ),
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      //Customer Address=======================================
                      Container(
                        margin: const EdgeInsets.only(top: 5, bottom: 5),
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          border: Border.all(
                            color: Colors.black26,
                          ),
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 15, left: 5, right: 5),
                                    child: Image.asset("images/saleStep4.png", alignment: Alignment.centerLeft),
                                  ),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    height: 30,
                                    padding: const EdgeInsets.all(0),
                                    margin: const EdgeInsets.only(top: 20, left: 10, bottom: 2),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 9,
                                          child: Container(
                                            height: 30,
                                            alignment: Alignment.centerRight,
                                            child: Image.asset(
                                              "images/LabelCustomerAddress.png",
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                const Expanded(
                                  flex: 1,
                                  child: Text(""),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: TextFormField(
                                      controller: addressController,
                                      maxLength: 500,
                                      enabled: false,
                                      keyboardType: TextInputType.multiline,
                                      maxLines: null,
                                      minLines: 5,
                                      decoration: const InputDecoration(
                                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                        border: OutlineInputBorder(),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                        ),
                                        hintText: " نشانی دقیق محل دریافت کالا ",
                                        hintStyle: TextStyle(color: Colors.black38, fontSize: 12),
                                        fillColor: Colors.black12,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                const Expanded(
                                  flex: 1,
                                  child: Text(""),
                                ),
                                const Expanded(
                                  flex: 2,
                                  child: Text(" کد پستی: "),
                                ),
                                Expanded(
                                  flex: 6,
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: TextFormField(
                                      enabled: false,
                                      controller: postalCodeController,
                                      textAlign: TextAlign.center,
                                      keyboardType: TextInputType.number,
                                      decoration: const InputDecoration(
                                        isDense: true,
                                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                        border: OutlineInputBorder(),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                        ),
                                        hintText: "کد پستی 10 رقمی",
                                        hintStyle: TextStyle(color: Colors.black38, fontSize: 12),
                                        fillColor: Colors.black12,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const Padding(
                              padding: EdgeInsets.only(left: 20, right: 20),
                              child: Divider(
                                color: Colors.black38,
                              ),
                            ),
                            Container(
                              alignment: Alignment.topRight,
                              padding: const EdgeInsets.only(top: 10, right: 20),
                              child: const Text.rich(
                                TextSpan(children: [
                                  TextSpan(
                                    text: "جزئیات ارسال:",
                                    style: TextStyle(color: Color(0xffe8ac4f)),
                                  ),
                                  TextSpan(text: " توسط فروشنده تا سه روز کاری"),
                                ]),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topRight,
                              padding: const EdgeInsets.only(top: 10, right: 20),
                              child: Text.rich(
                                softWrap: true,
                                textAlign: TextAlign.right,
                                TextSpan(children: [
                                  const TextSpan(
                                    text: "هزینه ارسال:",
                                    style: TextStyle(color: Color(0xffe8ac4f)),
                                  ),
                                  TextSpan(text: widget.paymentAndFacotrJSON['product_postCostBySeller'] == '1' ? 'هزینه ی ارسال با فروشنده است' : "پس کرایه ( هزینه ارسال با خریدار)"),
                                ]),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topRight,
                              padding: const EdgeInsets.all(20),
                              child: Text.rich(
                                textAlign: TextAlign.justify,
                                TextSpan(children: [
                                  const TextSpan(text: "از طریق فرآیند"),
                                  TextSpan(
                                    text: " ضمانت برگشت پول توسط وتمام ",
                                    style: const TextStyle(color: Color(0xffe8ac4f)),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        //show rules and conditions
                                        showDialog(
                                            context: context,
                                            useSafeArea: true,
                                            builder: (_) => const AlertDialog(insetPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 40), shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))), contentPadding: EdgeInsets.all(0.0), content: Rules()));
                                        // Single tapped.
                                      },
                                  ),
                                  const TextSpan(text: "کالای خریداری شده طبق مشخصات درج شده تحویل شما میگردد، در غیر اینصورت و عدم موافقت شما تمامی مبالغ واریز شما به حسابتان واریز میگردد"),
                                ]),
                              ),
                            ),
                            if (JJ.user_id == widget.paymentAndFacotrJSON['product_factor_userId'].toString()) // seller user can not pay her/himself product
                              if (widget.paymentAndFacotrJSON['payment_status'] != 'پرداخت شده')
                                InkWell(
                                  onTap: () async {
                                    debugPrint("saleStep:$saleStep");
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => PaymentOK(
                                                  paymentJson: widget.paymentAndFacotrJSON,
                                                )));
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.all(20),
                                    width: 220,
                                    padding: const EdgeInsets.all(8),
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage(
                                          "images/textFielsBg.png",
                                        ),
                                      ),
                                    ),
                                    child: const Text(
                                      "پرداخت",
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black87,
                                      ),
                                    ),
                                  ),
                                ),
                          ],
                        ),
                      ),
                      //#############saleStep==2:Post info section from seller to customer ##########################
                      if (saleStep >= 2)
                        Container(
                            margin: const EdgeInsets.only(top: 5, bottom: 5),
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(Radius.circular(8)),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                            ),
                            child: Column(children: [
                              Row(children: [
                                Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 15, left: 5, right: 5),
                                    child: Image.asset("images/saleStep3.png", alignment: Alignment.centerLeft),
                                  ),
                                ),
                                const Expanded(
                                  flex: 8,
                                  child: Text(""),
                                )
                              ]),
                              //Upload short clip befor aneding product ===================================
                              Row(
                                children: [
                                  const Expanded(
                                    flex: 1,
                                    child: Text(""),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Column(
                                      children: [
                                        const Text(
                                          "بارگذاری فیلم:",
                                        ),
                                        Text(
                                          "\n $uploadingText \n",
                                        ),
                                        InkWell(
                                          onTap: () {
                                            setState(() {
                                              videoController.value.isPlaying ? videoController.pause() : videoController.play();
                                            });
                                          },
                                          child: Icon(
                                            videoController.value.isPlaying ? Icons.pause : Icons.play_arrow,
                                            size: 40,
                                            color: Colors.blueAccent,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: InkWell(
                                      onTap: () async {
                                        if (userInSessionIsSeller && saleStep == 2) {
                                          uploadingText = "لطفا صبر کنید تا ویدئو بارگذاری شود";
                                          setState(() {});
                                          String imageName = await JJ.getVideoAndUpload(ImageSource.camera);
                                          debugPrint('product_factor_beforSendingClip>>>>${imageName}');
                                          uploadingText = 'ویدئو بارگذاری شد';
                                          beforSendingClip = imageName;
                                          videoController = VideoPlayerController.network("${JJ.server}upload/$beforSendingClip");
                                          videoPlayerFuture = videoController.initialize();
                                          setState(() {});
                                        }
                                      },
                                      child: beforSendingClip.isEmpty //If image is not set
                                          ? //If file not uploaded yet
                                          Container(
                                              margin: const EdgeInsets.all(4),
                                              padding: const EdgeInsets.all(15),
                                              decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(4)), shape: BoxShape.rectangle, color: Colors.black12),
                                              child: Column(
                                                children: [
                                                  const Icon(Icons.video_camera_back_outlined, color: Colors.white, size: 80),
                                                  Text("$uploadingText \n", style: const TextStyle(color: Colors.red)),
                                                  const Text(
                                                    'فروشنده ی محترم:'
                                                    '\n'
                                                    'الزامی است قبل از بسته بندی'
                                                    ' یک ویدئو کامل و کوتاه '
                                                    'از تمام جزئیات کالا گرفته و در این قسمت آپلود کنید',
                                                    style: TextStyle(color: Colors.black54, fontSize: 12, height: 1.8),
                                                  ),
                                                ],
                                              ))
                                          : Container(
                                              margin: const EdgeInsets.all(10),
                                              padding: const EdgeInsets.all(0),
                                              decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                              child: FutureBuilder(
                                                future: videoPlayerFuture,
                                                builder: (context, snapshot) {
                                                  if (snapshot.connectionState == ConnectionState.done) {
                                                    return AspectRatio(
                                                      aspectRatio: videoController.value.aspectRatio,
                                                      child: VideoPlayer(videoController),
                                                    );
                                                  } else {
                                                    return const Center(child: CircularProgressIndicator());
                                                  }
                                                },
                                              ),
                                            ),
                                    ),
                                  )
                                ],
                              ),
                              //===========================================================================
                              Row(
                                children: [
                                  const Expanded(flex: 1, child: Text("")),
                                  const Expanded(
                                    flex: 2,
                                    child: Text("کد رهگیری:"),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: TextFormField(
                                        controller: postalBarcodeController,
                                        enabled: userInSessionIsSeller,
                                        textAlign: TextAlign.center,
                                        keyboardType: TextInputType.number,
                                        decoration: const InputDecoration(
                                          isDense: true,
                                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                          border: OutlineInputBorder(),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                          ),
                                          hintText: "فروشنده باید کد رهگیری مرسوله را وارد کند",
                                          hintStyle: TextStyle(color: Colors.black38, fontSize: 12),
                                          fillColor: Colors.black12,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  const Expanded(
                                    flex: 1,
                                    child: Text(""),
                                  ),
                                  const Expanded(
                                    flex: 2,
                                    child: Text("تخمین تحویل:"),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: Container(
                                      height: 45,
                                      margin: const EdgeInsets.all(4),
                                      padding: const EdgeInsets.all(8),
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(border: Border.all(color: Colors.grey), borderRadius: const BorderRadius.all(Radius.circular(4))),
                                      child: userInSessionIsSeller
                                          ? // customer can not select a date, he/she can visit only
                                          InkWell(
                                              child: Text(
                                                postArrivalTime.isEmpty ? 'حدودا چه زمانی کالا بدست خریدار میرسد؟' : postArrivalTime.toPersianDate(),
                                                textAlign: TextAlign.center,
                                                style: postArrivalTime.isEmpty
                                                    ? // befor select a date by seller
                                                    const TextStyle(color: Colors.black38, fontSize: 12)
                                                    : const TextStyle(
                                                        // after select a date by seller
                                                        color: Colors.black87,
                                                        fontSize: 18),
                                              ),
                                              onTap: () async {
                                                final DateTime? date = await showPersianDatePicker(
                                                  context: context,
                                                );
                                                postArrivalTime = date.toString();
                                                debugPrint(postArrivalTime);
                                                setState(() {});
                                              },
                                            )
                                          : // else >> it is not seller so it is customer
                                          Text(
                                              postArrivalTime.toPersianDate(),
                                              textAlign: TextAlign.center,
                                              style: const TextStyle(
                                                  // after select a date by seller
                                                  color: Colors.black87,
                                                  fontSize: 18),
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  const Expanded(
                                    flex: 1,
                                    child: Text(""),
                                  ),
                                  const Expanded(
                                    flex: 2,
                                    child: Text(
                                      "رسید پست:",
                                    ),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: InkWell(
                                      onTap: () async {
                                        if (userInSessionIsSeller && saleStep == 2) {
                                          String imageName = await JJ.getImageAndUpload(ImageSource.gallery);
                                          debugPrint('user_attachPicPersonnelCard>>>>${imageName}');
                                          postBillAttach = imageName;
                                          // localStorage ?? await SharedPreferences.getInstance();
                                          // localStorage?.setString("product_pic2", imageName);
                                          setState(() {});
                                        }
                                      },
                                      child: postBillAttach.isEmpty //If image is not set
                                          ? //If file not uploaded yet
                                          Container(
                                              margin: const EdgeInsets.all(4),
                                              padding: const EdgeInsets.all(15),
                                              decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(4)), shape: BoxShape.rectangle, color: Colors.black12),
                                              child: Column(
                                                children: [
                                                  Image.asset(
                                                    "images/addimage.png",
                                                    scale: 5,
                                                  ),
                                                  const Text(
                                                    'فروشنده ی محترم:'
                                                    '\n'
                                                    'تصویر بارنامه'
                                                    '\n'
                                                    'یا رسید پستی'
                                                    '\n'
                                                    'یا اسکرین شات پیامک پست'
                                                    '\n'
                                                    'را در این قسمت بارگذاری کنید',
                                                    style: TextStyle(color: Colors.black54, fontSize: 12, height: 1.8),
                                                  ),
                                                ],
                                              ))
                                          : Container(
                                              margin: const EdgeInsets.all(10),
                                              padding: const EdgeInsets.all(0),
                                              decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                              child: Image.network("${JJ.server}upload/$postBillAttach", height: 210)),
                                    ),
                                  )
                                ],
                              ),
                              if (userInSessionIsSeller && saleStep == 2)
                                InkWell(
                                  onTap: () {
                                    insertPostBill();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: 320,
                                    padding: const EdgeInsets.all(8),
                                    margin: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(
                                      color: Colors.black87,
                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        const Text(
                                          " ثبت اطلاعات ارسال مرسوله ",
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: Color(0xffe8ac4f),
                                          ),
                                        ),
                                        Image.asset(
                                          "images/saleStep3.png",
                                          scale: 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                            ])),
                      //#################################################################################

                      //#############saleStep==3 :Customer must approve or report ##########################
                      if (saleStep >= 3)
                        Container(
                            margin: const EdgeInsets.only(top: 5, bottom: 5),
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(Radius.circular(8)),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                            ),
                            child: Column(children: [
                              if (!userInSessionIsSeller)
                                InkWell(
                                  onTap: () {
                                    if (saleStep == 3) {
                                      insertDeliveredByCustomer();
                                    }
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: 320,
                                    padding: const EdgeInsets.all(8),
                                    margin: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(
                                      color: Colors.black87,
                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        const Text(
                                          " کالا به خریدار تحویل داده شد ",
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: Color(0xffe8ac4f),
                                          ),
                                        ),
                                        Image.asset(
                                          "images/saleStep4.png",
                                          scale: 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              //#################saleStep==4 product is waiting for final approve ###########
                              if (saleStep >= 4)
                                Column(
                                  children: [
                                    const Padding(
                                      padding: EdgeInsets.only(left: 20, right: 20),
                                      child: Divider(
                                        color: Colors.black38,
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        const SizedBox(
                                          width: 30,
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: TextButton.icon(
                                              onPressed: () {
                                                if (!userInSessionIsSeller) {
                                                  satisfyRating = '5';
                                                  setState(() {});
                                                }
                                              },
                                              icon: Icon(Icons.sentiment_very_satisfied_outlined, color: Colors.green, size: satisfyRating == '5' ? 40 : 24),
                                              label: const Text('')),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: TextButton.icon(
                                              onPressed: () {
                                                if (!userInSessionIsSeller) {
                                                  satisfyRating = '4';
                                                  setState(() {});
                                                }
                                              },
                                              icon: Icon(Icons.sentiment_satisfied_alt_sharp, color: Colors.green, size: satisfyRating == '4' ? 40 : 24),
                                              label: const Text('')),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: TextButton.icon(
                                              onPressed: () {
                                                if (!userInSessionIsSeller) {
                                                  satisfyRating = '3';
                                                  setState(() {});
                                                }
                                              },
                                              icon: Icon(Icons.sentiment_neutral, color: Colors.orange, size: satisfyRating == '3' ? 40 : 24),
                                              label: const Text('')),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: TextButton.icon(
                                              onPressed: () {
                                                if (!userInSessionIsSeller) {
                                                  satisfyRating = '2';
                                                  setState(() {});
                                                }
                                              },
                                              icon: Icon(Icons.sentiment_dissatisfied, color: Colors.redAccent, size: satisfyRating == '2' ? 40 : 24),
                                              label: const Text('')),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: TextButton.icon(
                                              onPressed: () {
                                                if (!userInSessionIsSeller) {
                                                  satisfyRating = '1';
                                                  setState(() {});
                                                }
                                              },
                                              icon: Icon(Icons.sentiment_very_dissatisfied, color: Colors.red, size: satisfyRating == '1' ? 40 : 24),
                                              label: const Text('')),
                                        ),
                                        const SizedBox(
                                          width: 30,
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextFormField(
                                        controller: customerCommentsController,
                                        maxLength: 245,
                                        enabled: !userInSessionIsSeller,
                                        keyboardType: TextInputType.multiline,
                                        maxLines: null,
                                        minLines: 5,
                                        decoration: const InputDecoration(
                                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                          border: OutlineInputBorder(),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                          ),
                                          hintText: " توضیحات خریدار در صورت صلاحدید ",
                                          hintStyle: TextStyle(color: Colors.black38, fontSize: 12),
                                          fillColor: Colors.black12,
                                        ),
                                      ),
                                    ),
                                    if (!userInSessionIsSeller)
                                      Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: InkWell(
                                              onTap: () {
                                                insertCustomerIsSatisfied();
                                                customerIsSatisfied = '1';
                                              },
                                              child: Container(
                                                alignment: Alignment.center,
                                                width: 320,
                                                padding: const EdgeInsets.all(8),
                                                margin: const EdgeInsets.all(20),
                                                decoration: const BoxDecoration(
                                                  color: Colors.green,
                                                  borderRadius: BorderRadius.all(Radius.circular(8)),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: const [
                                                    Text(
                                                      " تایید میکنم ",
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                    Icon(
                                                      Icons.check_circle_outline_outlined,
                                                      color: Colors.white,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: InkWell(
                                              onTap: () {
                                                customerIsSatisfied = '0';
                                                setState(() {});
                                              },
                                              child: Container(
                                                alignment: Alignment.center,
                                                width: 320,
                                                padding: const EdgeInsets.all(8),
                                                margin: const EdgeInsets.all(20),
                                                decoration: const BoxDecoration(
                                                  color: Colors.red,
                                                  borderRadius: BorderRadius.all(Radius.circular(8)),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: const [
                                                    Text(
                                                      " مرجوع میکنم ",
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                    Icon(
                                                      Icons.remove_circle_outline_rounded,
                                                      color: Colors.white,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    if (customerIsSatisfied == '0')
                                      Column(
                                        children: [
                                          const SizedBox(
                                            height: 30,
                                          ),
                                          const Text('چرا ناراضی هستید؟', style: TextStyle(color: Colors.red, fontSize: 18.0)),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Container(
                                            height: 30,
                                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  child: Checkbox(
                                                    activeColor: Colors.red,
                                                    value: unSatisfiedReason1,
                                                    onChanged: (newValue) {
                                                      setState(() {
                                                        unSatisfiedReason1 = newValue!;
                                                        if (newValue!) {
                                                          debugPrint("checkedValue....<>>>$newValue");
                                                        } else {
                                                          debugPrint("checkedValue....<!>>>$newValue");
                                                        }
                                                      });
                                                    },
                                                  ),
                                                ),
                                                const Expanded(
                                                  flex: 4,
                                                  child: Text(style: TextStyle(fontSize: 14), " کالا با عکس مطابقت ندارد "),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 30,
                                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  child: Checkbox(
                                                    activeColor: Colors.red,
                                                    value: unSatisfiedReason2,
                                                    onChanged: (newValue) {
                                                      setState(() {
                                                        unSatisfiedReason2 = newValue!;
                                                        if (newValue!) {
                                                          debugPrint("checkedValue....<>>>$newValue");
                                                        } else {
                                                          debugPrint("checkedValue....<!>>>$newValue");
                                                        }
                                                      });
                                                    },
                                                  ),
                                                ),
                                                const Expanded(
                                                  flex: 4,
                                                  child: Text(style: TextStyle(fontSize: 14), " کالا شکسته یا ناقص است "),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 30,
                                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  child: Checkbox(
                                                    activeColor: Colors.red,
                                                    value: unSatisfiedReason3,
                                                    onChanged: (newValue) {
                                                      setState(() {
                                                        unSatisfiedReason3 = newValue!;
                                                        if (newValue!) {
                                                          debugPrint("checkedValue....<>>>$newValue");
                                                        } else {
                                                          debugPrint("checkedValue....<!>>>$newValue");
                                                        }
                                                      });
                                                    },
                                                  ),
                                                ),
                                                const Expanded(
                                                  flex: 4,
                                                  child: Text(style: TextStyle(fontSize: 14), " عدم تطابق توضیحات ارائه شده با کالای ارسالی "),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 30,
                                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  child: Checkbox(
                                                    activeColor: Colors.red,
                                                    value: unSatisfiedReason4,
                                                    onChanged: (newValue) {
                                                      setState(() {
                                                        unSatisfiedReason4 = newValue!;
                                                        if (newValue!) {
                                                          debugPrint("checkedValue....<>>>$newValue");
                                                        } else {
                                                          debugPrint("checkedValue....<!>>>$newValue");
                                                        }
                                                      });
                                                    },
                                                  ),
                                                ),
                                                const Expanded(
                                                  flex: 4,
                                                  child: Text(style: TextStyle(fontSize: 14), " کالا معیوب یا حراب است "),
                                                )
                                              ],
                                            ),
                                          ),
                                          //++++ unsatisfiy pic ++++++++++++++++++++++++++++++++
                                          Row(
                                            children: [
                                              Expanded(
                                                flex: 1,
                                                child: InkWell(
                                                  onTap: () async {
                                                    if (!userInSessionIsSeller) {
                                                      String imageName = await JJ.getImageAndUpload(ImageSource.gallery);
                                                      debugPrint('unSatisfiedPic1>>>>${imageName}');
                                                      unSatisfiedPic1 = imageName;
                                                      // localStorage ?? await SharedPreferences.getInstance();
                                                      // localStorage?.setString("product_pic2", imageName);
                                                      setState(() {});
                                                    }
                                                  },
                                                  child: unSatisfiedPic1.isEmpty //If image is not set
                                                      ? //If file not uploaded yet
                                                      Container(
                                                          margin: const EdgeInsets.all(4),
                                                          padding: const EdgeInsets.all(15),
                                                          decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(4)), shape: BoxShape.rectangle, color: Colors.black12),
                                                          child: Column(
                                                            children: [
                                                              Image.asset(
                                                                "images/addimage.png",
                                                                scale: 5,
                                                              ),
                                                            ],
                                                          ))
                                                      : Container(
                                                          margin: const EdgeInsets.all(10),
                                                          padding: const EdgeInsets.all(4),
                                                          decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                                          child: Image.network(
                                                            "${JJ.server}upload/$unSatisfiedPic1",
                                                          )),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: InkWell(
                                                  onTap: () async {
                                                    if (!userInSessionIsSeller) {
                                                      String imageName = await JJ.getImageAndUpload(ImageSource.gallery);
                                                      debugPrint('unSatisfiedPic2>>>>${imageName}');
                                                      unSatisfiedPic2 = imageName;
                                                      // localStorage ?? await SharedPreferences.getInstance();
                                                      // localStorage?.setString("product_pic2", imageName);
                                                      setState(() {});
                                                    }
                                                  },
                                                  child: unSatisfiedPic2.isEmpty //If image is not set
                                                      ? //If file not uploaded yet
                                                      Container(
                                                          margin: const EdgeInsets.all(4),
                                                          padding: const EdgeInsets.all(15),
                                                          decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(4)), shape: BoxShape.rectangle, color: Colors.black12),
                                                          child: Column(
                                                            children: [
                                                              Image.asset(
                                                                "images/addimage.png",
                                                                scale: 5,
                                                              ),
                                                            ],
                                                          ))
                                                      : Container(
                                                          margin: const EdgeInsets.all(10),
                                                          padding: const EdgeInsets.all(4),
                                                          decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                                          child: Image.network(
                                                            "${JJ.server}upload/$unSatisfiedPic2",
                                                          )),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: InkWell(
                                                  onTap: () async {
                                                    if (!userInSessionIsSeller) {
                                                      String imageName = await JJ.getImageAndUpload(ImageSource.gallery);
                                                      debugPrint('unSatisfiedPic3>>>>${imageName}');
                                                      unSatisfiedPic3 = imageName;
                                                      // localStorage ?? await SharedPreferences.getInstance();
                                                      // localStorage?.setString("product_pic2", imageName);
                                                      setState(() {});
                                                    }
                                                  },
                                                  child: unSatisfiedPic3.isEmpty //If image is not set
                                                      ? //If file not uploaded yet
                                                      Container(
                                                          margin: const EdgeInsets.all(4),
                                                          padding: const EdgeInsets.all(15),
                                                          decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(4)), shape: BoxShape.rectangle, color: Colors.black12),
                                                          child: Column(
                                                            children: [
                                                              Image.asset(
                                                                "images/addimage.png",
                                                                scale: 5,
                                                              ),
                                                            ],
                                                          ))
                                                      : Container(
                                                          margin: const EdgeInsets.all(10),
                                                          padding: const EdgeInsets.all(4),
                                                          decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                                          child: Image.network(
                                                            "${JJ.server}upload/$unSatisfiedPic3",
                                                          )),
                                                ),
                                              ),
                                            ],
                                          ),
                                          //++++++++++++++++++++++++++++++++++++++++++++++++++++
                                          const SizedBox(
                                            height: 25,
                                          ),
                                          if (!userInSessionIsSeller)
                                            InkWell(
                                              onTap: () {
                                                if (saleStep == 4) {
                                                  insertCustomerIsNotSatisfied();
                                                } else if (saleStep == 5) {
                                                  errorsForForm = 'شما قبلا نظر خود را ثبت کرده اید';
                                                  setState(() {});
                                                }
                                              },
                                              child: Container(
                                                alignment: Alignment.center,
                                                width: 320,
                                                padding: const EdgeInsets.all(8),
                                                margin: const EdgeInsets.all(20),
                                                decoration: const BoxDecoration(
                                                  color: Colors.red,
                                                  borderRadius: BorderRadius.all(Radius.circular(8)),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: const [
                                                    Text(
                                                      " ثبت دلایل و مستندات ",
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                    Icon(
                                                      Icons.remove_circle_outline_rounded,
                                                      color: Colors.white,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          const SizedBox(
                                            height: 25,
                                          ),
                                        ],
                                      ),

                                    //===========================================================================
                                    Row(
                                      children: [
                                        const Expanded(flex: 1, child: Text("")),
                                        const Expanded(
                                          flex: 2,
                                          child: Text("کد رهگیری:"),
                                        ),
                                        Expanded(
                                          flex: 6,
                                          child: Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: TextFormField(
                                              controller: rejectPstalBarcodeController,
                                              enabled: !userInSessionIsSeller,
                                              textAlign: TextAlign.center,
                                              keyboardType: TextInputType.number,
                                              decoration: const InputDecoration(
                                                isDense: true,
                                                focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                                border: OutlineInputBorder(),
                                                enabledBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                                ),
                                                hintText: "خریدار باید کد رهگیری مرسوله مرجوعی را وارد کند",
                                                hintStyle: TextStyle(color: Colors.black38, fontSize: 12),
                                                fillColor: Colors.black12,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        const Expanded(
                                          flex: 1,
                                          child: Text(""),
                                        ),
                                        const Expanded(
                                          flex: 2,
                                          child: Text(
                                            "رسید پست:",
                                          ),
                                        ),
                                        Expanded(
                                          flex: 6,
                                          child: InkWell(
                                            onTap: () async {
                                              if (!userInSessionIsSeller && saleStep == 5) {
                                                String imageName = await JJ.getImageAndUpload(ImageSource.gallery);
                                                debugPrint('product_factor_rejectPostBillAttach>>>>${imageName}');
                                                rejectPostBillAttach = imageName;
                                                // localStorage ?? await SharedPreferences.getInstance();
                                                // localStorage?.setString("product_pic2", imageName);
                                                setState(() {});
                                              }
                                            },
                                            child: rejectPostBillAttach.isEmpty //If image is not set
                                                ? //If file not uploaded yet
                                                Container(
                                                    margin: const EdgeInsets.all(4),
                                                    padding: const EdgeInsets.all(15),
                                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(4)), shape: BoxShape.rectangle, color: Colors.black12),
                                                    child: Column(
                                                      children: [
                                                        Image.asset(
                                                          "images/addimage.png",
                                                          scale: 5,
                                                        ),
                                                        const Text(
                                                          'خریدار محترم:'
                                                          '\n'
                                                          'تصویر بارنامه'
                                                          '\n'
                                                          'یا رسید پستی'
                                                          '\n'
                                                          'یا اسکرین شات پیامک پست'
                                                          '\n'
                                                          'را در این قسمت بارگذاری کنید',
                                                          style: TextStyle(color: Colors.black54, fontSize: 12, height: 1.8),
                                                        ),
                                                      ],
                                                    ))
                                                : Container(
                                                    margin: const EdgeInsets.all(10),
                                                    padding: const EdgeInsets.all(0),
                                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                                    child: Image.network("${JJ.server}upload/$rejectPostBillAttach", height: 210)),
                                          ),
                                        )
                                      ],
                                    ),
                                    if (!userInSessionIsSeller && saleStep == 5)
                                      InkWell(
                                        onTap: () {
                                          insertRejectPostBill();
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          width: 320,
                                          padding: const EdgeInsets.all(8),
                                          margin: const EdgeInsets.all(20),
                                          decoration: const BoxDecoration(
                                            color: Colors.black87,
                                            borderRadius: BorderRadius.all(Radius.circular(8)),
                                          ),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              const Text(
                                                " ثبت اطلاعات ارسال مرسوله مرجوعی ",
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: Color(0xffe8ac4f),
                                                ),
                                              ),
                                              Image.asset(
                                                "images/saleStep3.png",
                                                scale: 6,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                            ])),
                      //#################################################################################
                      // Buy Button =========================================================
                    ])),
                Text(
                  errorsForForm,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 12,
                    height: 2,
                    color: Colors.red,
                  ),
                ),
                const SizedBox(height: 75),
              ],
            ))));
  }
}
