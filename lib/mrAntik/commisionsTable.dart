import 'dart:io';
import 'dart:math';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'package:vaTamam/mrAntik/widgets/navBarTop.dart';
import 'package:zoom_widget/zoom_widget.dart';

class CommissionsTable extends StatelessWidget {
  const CommissionsTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      SizedBox(
          width: MediaQuery.of(context).size.width - 5,
          height: MediaQuery.of(context).size.height-160 ,
        child: Zoom(
          enableScroll: false,
          initScale: 1,
          centerOnScale: true,
          doubleTapZoom: true,
          maxScale: 2,
          opacityScrollBars: 0.2,
          child: Container(
            width: MediaQuery.of(context).size.width - 5,
            // height: MediaQuery.of(context).size.height-160 ,
            decoration: BoxDecoration(
              color: const Color(0xfef7edff),
              borderRadius: BorderRadius.circular(8.0),
              border: Border.all(color: Colors.white54),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black38,
                  spreadRadius: 5,
                  blurRadius: 10,
                  offset: Offset(7, 7), // changes position of shadow
                )
              ],
            ),
            child: const Padding(
              padding: EdgeInsets.all(15.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "جدول تعرفه های وتمام:",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      // Image.asset("images/login.png", scale: 6),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: double.infinity,
                      child: Text.rich(
                    style: TextStyle(fontSize: 12, height: 2),
                    TextSpan(children: [
                      TextSpan(text: "کارمزدی که از دریافتی فروشنده کسر میشود:" "\n",style: TextStyle(fontWeight: FontWeight.bold)),
                    ]),
                    softWrap: true,
                    textAlign: TextAlign.justify,
                  )
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("0 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 500,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%15"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("500,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 1,000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%10"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("1,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 2,000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%9"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("2,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 3000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%8"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("3,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 4000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%7"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("4,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 5000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%6"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("5,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 6000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%5.5"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("6,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 7000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%5"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("7,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 9000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%4.5"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("9,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 12000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%4"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("12,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 15000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%3.5"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("15,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 20000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%3"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("20,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 50000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%2.5"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("50,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 100000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%2"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("100,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 500,000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%1.5"
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          textAlign: TextAlign.right,
                          style:TextStyle(fontSize: 12),
                          "قیمت کالا از"
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("500,000,000 تا",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(" 5,000,000,000 تومان ",
                          style:TextStyle(fontSize: 12),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                        style:TextStyle(fontSize: 12),
                          "%1"
                        ),

                      ),
                    ],
                  ),
                  SizedBox(height: 15,),


                ],
              ),
            ),
          ),
        ));
  }
}
