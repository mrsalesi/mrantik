import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http_parser/http_parser.dart';
import "package:universal_html/html.dart" as html;

Map<String, String> headers = {
  'Content-type': 'application/octet-stream',
  'Content-Type': 'application/json',
  'Accept': '*/*',
  // "Authorization": "Bearer $token",
};

class JJ {
  //Java Server Address must add here and work for all request
  // static const String server = "https://app.mrantic.ir/";
  static const String server = "https://app.vatamam.com/";

  // static const String server = "http://localhost:8084/MrAntik/";
  // static const String server = "http://www.mrantic.ir:8080/";
  // static const String server = "https://mrantic.ir/";

  // static const String server = "/";
  // static const String server = "http://172.17.72.122:8084/MrAntik/";

  // static const String server = "http//172.16.73.129:8080/MrAntik/";
  // static const String server = "http://192.168.60.88:8084/MrAntik/";

  // static const String server = "http://192.168.106.88:8084/MrAntik/";
  // static const String server = "http://localhost:49932/MrAntik/";

  static const String serverController = "${server}Server?";

  static String user_token = "";
  static String user_username = "";
  static String user_codeMeli = "";
  static String user_birthdate = "";
  static String user_attachPicPersonnelCard = "";
  static String user_id = "";
  static String user_name = "";
  static String user_family = "";
  static String user_mobile = "";
  static String user_postalCode = "";
  static String user_AccountInformation = "";
  static String user_address = "";
  static String user_city = "تهران";
  static String user_grade = "";
  static String user_email = "";
  static var publicConfigs = {};
  static String userBalance = ""; //Fill from Payment.java in main.dart

  static SharedPreferences? localStorage;

  // flutter build apk --target-platform=android-arm
  // flutter build apk --target-platform=android-arm64

  /// This function send simple get request to server and return response as String
  Future<String> jjAjax(String params) async {
    debugPrint("jjAjax>>>>>>>${serverController}responseType=JSON&${params}");
    try {
      String url = '${serverController}responseType=JSON&$params';
      // String hostname = Requests.getHostname(url);
      // Set cookies using [CookieJar.parseCookiesString]
      // await Requests.setStoredCookies(hostname, cookies);

// Add single cookie using [CookieJar.parseCookiesString]
//       var cookieJar = await Requests.getStoredCookies(hostname);
      // cookieJar["name"] = Cookie("name", "value");
      // await Requests.setStoredCookies(hostname, cookieJar);
      // final resp = await Requests.get(url);
      BaseOptions options = BaseOptions(
        baseUrl: url,
        connectTimeout: const Duration(seconds: 180),
        receiveTimeout: const Duration(seconds: 180),
      );
      Dio dio = Dio(options);
      var cookieJar = CookieJar();
      dio.interceptors.add(CookieManager(cookieJar));
      final response = await dio.get(url).catchError((e) {
        debugPrint("Expppp~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        debugPrint(e.toString());
      }).timeout(const Duration(seconds: 180), onTimeout: () {
        debugPrint("Ahhhhh~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        throw "TimeOut";
      });
      ;
      ;

// Add a single cookie using [Requests.addCookie]
// Same as the above one but without exposing `cookies.dart`
//       Requests.addCookie(hostname, "name", "value");

      debugPrint('jjAjax-response.statusCode:${response.statusCode}');
      debugPrint(response.data);
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return (response.data);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        jjToast('خطا در اتصال به سرور');
        return '';
      }
    } catch (e) {
      jjToast('خطا در اتصال به شبکه');
      return '';
    }
    //debugPrint(await http.read(Uri.parse('http://95.217.123.171:8080/Server')));
  }

  Future<String> readResponse(HttpClientResponse response) {
    final completer = Completer<String>();
    final contents = StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }

  static Future<String> fileUpload(File imageFile) async {
    try {
      var stream = http.ByteStream(imageFile.openRead());
      stream.cast();
      var length = await imageFile.length();
      var uri = Uri.parse("${JJ.server}UploadServlet");
      // Uri uri =
      // Uri(scheme: 'https', port: 443, host: 'app.mrantic.ir', path: 'UploadServlet'); // to reach this endpoint: 'https://mywebsite.com:3000/folder'

      var request = http.MultipartRequest("POST", uri);
      debugPrint("jjTools.fileUpload()~~~~~~~~~~~~~~~~~~~~~~~~~~~${JJ.server}");
      var multipartFile = http.MultipartFile(
        'file',
        stream,
        length,
        filename: basename(imageFile.path),
        contentType: MediaType('application', 'zip'),
      );
      request.files.add(multipartFile);
      final response = await request.send().timeout(const Duration(minutes: 10), onTimeout: () {
        debugPrint("ahhhhh~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        throw "TimeOut";
      });
      debugPrint(response.reasonPhrase);
      if (response.statusCode == 200) {
        final contents = StringBuffer();
        await for (var data in response.stream.transform(utf8.decoder)) {
          contents.write(data);
          return contents.toString();
        }
      }
      jjToast('خطای بارگزاری در برنامه کد 78');
      return '';
    } catch (e) {
      jjToast('خطا در اتصال به شبکه کد 81');
      return '';
    }
  }

  static ConvertFileToCast(data) {
    List<int> list = data.cast<int>();
    return list;
  }

  ///This function send web files that are as byte to UploadServer (UploadServlet.java)
  static Future<String> fileUploadWeb(Uint8List imageFile) async {
    try {
      var uri = Uri.parse("${JJ.server}UploadServlet");
      // Uri uri =
      // Uri.http('www.mrantic.ir','UploadServlet');
      var request = http.MultipartRequest("POST", uri);
      request.headers.addAll(headers);
      debugPrint("jjTools.fileUploadWeb()~~~~~~~~~~~~~~~~~~~~~~~~~~~~5" + uri.host);
      request.files.add(http.MultipartFile.fromBytes('image', await ConvertFileToCast(imageFile), filename: "aaaaa.png", contentType: MediaType('*', '*')));
      // request;
      var response = await request.send().timeout(const Duration(minutes: 10), onTimeout: () {
        debugPrint("Ahhhhh~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        throw "TimeOut";
      });
      if (response.statusCode == 200) {
        debugPrint("~~~~~~~~~~~~~~~~~~~~~~~~~~~~6 ${response.statusCode} --- ${response.toString()}");
        final contents = StringBuffer();
        await for (var data in response.stream.transform(utf8.decoder)) {
          contents.write(data);
          debugPrint("~~~~~~~~~~~~~~~~~~~~~~~~~~~~7 ");
          return contents.toString();
        }
      }
      jjToast('خطای بارگزاری در برنامه کد 78');
      return '';
    } catch (e) {
      debugPrint(e.toString());
      jjToast('خطا در اتصال به شبکه کد 81');
      return '';
    }
  }

  ///This Function returns a list that which using for create dropdown menu
  ///use .toList() after return
  static List<String> getListFromJson(List<dynamic> json, String atrName) {
    List<String> l = [];
    for (int i = 0; i < json.length; i++) {
      l.add(json[i][atrName]);
    }
    return l;
  }

  static void jjToast(String message) {
    Fluttertoast.showToast(msg: message, toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.TOP, timeInSecForIosWeb: 5, textColor: Colors.white, fontSize: 16.0, backgroundColor: Colors.black87);
  }

  ///تبدیل تاریخ از متن به تاریخ شمسی
  static DateTime toDateTime(String timeStampStr) {
    var inputStr = timeStampStr.replaceAll("/", "-");
    var splitedStr = inputStr.split("-");
    if (splitedStr[2].length == 1) {
      splitedStr[2] = "0${splitedStr[2]}";
    }
    if (splitedStr[1].length == 1) {
      splitedStr[1] = "0${splitedStr[1]}";
    }
    return DateTime.parse(splitedStr.join("-"));
  }

  static Future<String> getImageAndUpload(ImageSource media) async {
    debugPrint("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~1");
    XFile? selectedImageFileObj;
    final ImagePicker picker = ImagePicker();
    selectedImageFileObj = await picker.pickImage(source: media, maxHeight: 1200, maxWidth: 1200);
    return await JJ.fileUpload(File(selectedImageFileObj!.path));
  }

  static Future<String> getVideoAndUpload(ImageSource media) async {
    XFile? selectedImageFileObj;
    final ImagePicker picker = ImagePicker();
    selectedImageFileObj = await picker.pickVideo(source: media, maxDuration: const Duration(minutes: 5));
    return await JJ.fileUpload(File(selectedImageFileObj!.path));
  }

  static String jjSeRagham(String digitStr) {
    return digitStr.seRagham();
  }

  static int jjToInt(String digitStr) {
    digitStr = digitStr.replaceAll(RegExp('\\D'), '');
    digitStr = digitStr.isEmpty ? "0" : digitStr;
    debugPrint('jjToInt>>>>' + digitStr);
    return int.parse(digitStr);
  }

  static void jjShare(String text) async {
    if (kIsWeb) {
      Map map = {"text": "$text"};
      await html.window.navigator.share(map);
    } else {
      Share.share(text);
    }
  }

  static List<String> listOfCites = [
    'تهران',
    'اصفهان',
    'مشهد',
    'تبریز',
    'آبادان',
    'آباد',
    'آبدانان',
    'آبیک',
    'آذرشهر',
    'آران و بیدگل',
    'آزادشهر',
    'آزادشهر',
    'آستارا',
    'آستانه اشرفیه',
    'آشتيان',
    'آق‌قلا',
    'آمل',
    'ابرکوه',
    'ابهر',
    'ابوموسي',
    'اراک',
    'اردبیل',
    'اردستان',
    'اردل',
    'اردکان',
    'ارسنجان',
    'ارومیه',
    'ازنا',
    'استهبان',
    'اسدآباد',
    'اسفراین',
    'اسلام‌آباد غرب',
    'اسلام‌شهر',
    'اسکو',
    'اشنویه',
    'اقلید',
    'البرز',
    'اليگودرز',
    'امیدیه',
    'اندیمشک',
    'اهر',
    'اهواز',
    'اَملَش',
    'ایجرود',
    'ایذه',
    'ایرانشهر',
    'ایلام',
    'ایوان',
    'بابل',
    'بابلسر',
    'باغ‌ملک',
    'بافت',
    'بافق',
    'بانه',
    'بجنورد',
    'برخوار و میمه',
    'بردسکن',
    'بردسیر',
    'بروجرد',
    'بروجن',
    'بستان‌آباد',
    'بستک',
    'بم',
    'بناب',
    'بندر انزلی',
    'بندر عباس',
    'بندر لنگه',
    'بندر ماهشهر',
    'بندر گز',
    'بهار',
    'بهبهان',
    'بهشهر',
    'بهمئی',
    'بوئین‌زهرا',
    'بوانات',
    'بوشهر',
    'بوکان',
    'بویراحمد',
    'بیجار',
    'بیرجند',
    'بیله‌سوار',
    'پارس‌آباد',
    'پاسارگاد',
    'پاوه',
    'پاکدشت',
    'پل‌دختر',
    'پیرانشهر',
    'تاکستان',
    'تایباد',
    'تبریز',
    'تربت جام',
    'تربت حیدریه',
    'ترکمن',
    'تفت',
    'تفرش',
    'تنکابن',
    'تنگستان',
    'تويسرکان',
    'تکاب',
    'تیران و کرون',
    'ثلاث باباجانی',
    'جاجرم',
    'جاسک',
    'جلفا',
    'جم',
    'جهرم',
    'جوانرود',
    'جويبار',
    'جیرفت',
    'چابهار',
    'چادگان',
    'چاراویماق',
    'چالدران',
    'چالوس',
    'چناران',
    'حاجي‌آباد',
    'خاتم',
    'خاش',
    'خدابنده',
    'خرمدره',
    'خرمشهر',
    'خرم‌آباد',
    'خرم‌بید',
    'خلخال',
    'خلیل‌آباد',
    'خمير',
    'خمين',
    'خمینی‌شهر',
    'خنج',
    'خواف',
    'خوانسار',
    'خوی',
    'داراب',
    'دالاهو',
    'دامغان',
    'درمیان',
    'دره‌شهر',
    'درگز',
    'دزفول',
    'دشت آزادگان',
    'دشتستان',
    'دشتی',
    'دلفان',
    'دليجان',
    'دلگان',
    'دماوند',
    'دنا',
    'دهلران',
    'دورود',
    'دیر',
    'دیلم',
    'دیواندره',
    'رامسر',
    'رامشیر',
    'رامهرمز',
    'رامیان',
    'راور',
    'رباط‌کریم',
    'رزن',
    'رشت',
    'رشتخوار',
    'رضوانشهر',
    'رفسنجان',
    'روانسر',
    'رودان',
    'رودبار جنوب',
    'رودبار',
    'رودسر',
    'ری',
    'زابل',
    'زاهدان',
    'زرند',
    'زرنديه',
    'زرین‌دشت',
    'زنجان',
    'زهک',
    'ساري',
    'ساوجبلاغ',
    'ساوه',
    'سبزوار',
    'سراب',
    'سراوان',
    'سرایان',
    'سرباز',
    'سربیشه',
    'سرخس',
    'سردشت',
    'سروآباد',
    'سرپل ذهاب',
    'سقز',
    'سلسله',
    'سلماس',
    'سمنان',
    'سمیرم سفلی',
    'سمیرم',
    'سنقر',
    'سنندج',
    'سوادکوه',
    'سپیدان',
    'سیاهکل',
    'سیرجان',
    'شادگان',
    'شازند',
    'شاهرود',
    'شاهین‌دژ',
    'شبستر',
    'شمیرانات',
    'شهر بابک',
    'شهرضا',
    'شهرکرد',
    'شهریار',
    'شوش',
    'شوشتر',
    'شَفت',
    'شیراز',
    'شیروان و چرداول',
    'شیروان',
    'صحنه',
    'صدوق',
    'صومعه‌سرا',
    'طارم',
    'طبس',
    'طوالش',
    'عجب‌شیر',
    'علی‌آباد',
    'عنبرآباد',
    'فارسان',
    'فاروج',
    'فراشبند',
    'فردوس',
    'فریدن',
    'فریدون‌شهر',
    'فریمان',
    'فسا',
    'فلاورجان',
    'فومَن',
    'فیروزآباد',
    'فیروزکوه',
    'قائم‌شهر',
    'قائنات',
    'قروه',
    'قزوین',
    'قشم',
    'قصر شیرین',
    'قلعه گنج',
    'قم',
    'قوچان',
    'قیر و کارزین',
    'کازرون',
    'کاشان',
    'کاشمر',
    'کامیاران',
    'کبودرآهنگ',
    'کرج',
    'کردکوی',
    'کرمان',
    'کرمانشاه',
    'کلات',
    'کلاله',
    'کلیبر',
    'کميجان',
    'کنارک',
    'کنگان',
    'کنگاور',
    'کهنوج',
    'کهگیلویه',
    'کوثر',
    'کوهبنان',
    'کوهدشت',
    'کوهرنگ',
    'گاوبندي',
    'گتوند',
    'گرمسار',
    'گرگان',
    'گلوگاه',
    'گلپایگان',
    'گناباد',
    'گناوه',
    'گنبد کاووس',
    'گِرمی',
    'گچساران',
    'گیلان غرب',
    'لارستان',
    'لالی',
    'لامِرد',
    'لاهیجان',
    'لردگان',
    'لنجان',
    'لنگرود',
    'ماسال',
    'مانه و سملقان',
    'ماه‌نشان',
    'ماکو',
    'مبارکه',
    'محلات',
    'محمودآباد',
    'مراغه',
    'مراوه‌تپه',
    'مرند',
    'مرودشت',
    'مریوان',
    'مسجد سلیمان',
    'ملاير',
    'ملکان',
    'ممسنی',
    'منوجان',
    'مه ولات',
    'مهاباد',
    'مهدی‌شهر',
    'مهر',
    'مهران',
    'مهريز',
    'ميناب',
    'مِشگین‌شهر',
    'مِيبُد',
    'میاندوآب',
    'میانه',
    'مینودشت',
    'نائین',
    'نجف‌آباد',
    'نطنز',
    'نظرآباد',
    'نقده',
    'نهاوند',
    'نهبندان',
    'نور',
    'نوشهر',
    'نَمین',
    'نکا',
    'نیر',
    'نیشابور',
    'نیک‌شهر',
    'نی‌ریز',
    'هرسین',
    'هریس',
    'هشترود',
    'همدان',
    'هندیجان',
    'ورامین',
    'ورزقان',
    'يزد',
  ];
}
