class MrAnticCalculator {
  ///This function calculate commission by mrAntic Admin rule and return as int
  static int commission(int price) {
    if (price < 100000) return (price * 0.2).toInt();
    if (price < 500000) return (price * 0.15).toInt();
    if (price < 1000000) return (price * 0.10).toInt();
    if (price < 2000000) return (price * 0.09).toInt();
    if (price < 3000000) return (price * 0.08).toInt();
    if (price < 4000000) return (price * 0.07).toInt();
    if (price < 5000000) return (price * 0.06).toInt();
    if (price < 6000000) return (price * 0.055).toInt();
    if (price < 7000000) return (price * 0.05).toInt();
    if (price < 9000000) return (price * 0.045).toInt();
    if (price < 12000000) return (price * 0.04).toInt();
    if (price < 15000000) return (price * 0.035).toInt();
    if (price < 20000000) return (price * 0.03).toInt();
    if (price < 50000000) return (price * 0.025).toInt();
    if (price < 100000000) return (price * 0.02).toInt();
    if (price < 500000000) return (price * 0.015).toInt();
      int number = (price * 0.01).toInt();
    return (number/10000.0).ceil()*10000  ;//round to 10,000
  }
  /// This function calculate next price that user can bit it in auction
  static int nextBid(String lastBid, String initPrice){
    int last = lastBid.isEmpty ? 0 : int.parse(lastBid);
    int initPriceInt = initPrice.isEmpty ? 0 : int.parse(initPrice);
    int nextStep = MrAnticCalculator.commission(last);
    nextStep = nextStep == 0 ? 100000 : nextStep ;
    int nextbid = last + nextStep ;
    nextbid=(nextbid/10000.0).ceil()*10000;//round to 10,000
    return nextbid < initPriceInt ? initPriceInt : nextbid;
    }
}
