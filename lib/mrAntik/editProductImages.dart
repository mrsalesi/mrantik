import 'dart:io';
import "dart:async";
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vaTamam/mrAntik/addNewProduct.dart';
import 'package:vaTamam/mrAntik/addOneImageForProduct.dart';
import 'package:vaTamam/mrAntik/rules.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'tools/jjTools.dart';
import 'widgets/Header.dart';

class EditProductImages extends StatefulWidget {
  const EditProductImages({Key? key}) : super(key: key);

  @override
  State<EditProductImages> createState() => _EditProductImagesState();
}
enum SingingCharacter { black, white } // This enum use for bg_remover option and need an other _character variable that is defined in below
class _EditProductImagesState extends State<EditProductImages> {
  XFile? selectedImageFileObj;
  final ImagePicker picker = ImagePicker();
  SharedPreferences? localStorage;

// This function use to refresh page after AlertDialog cloed
  callback() {
    debugPrint(">>>>>>>>>>>>>>>>>>>>>>>callback();");
    setState(() {});
  }

  static Future<String> changeImageBgColor(String imageName, String bgColor) async {
    String params = "do=Pic.changeImageBgColor";
    params += '&imageName=$imageName';
    params += '&background=$bgColor';
    String result = await JJ().jjAjax(params);
    debugPrint('result ::::$result');
    return result.isEmpty ? imageName : result;
  }

  Future<String> getImageAndUpload(ImageSource media) async {
    if (!kIsWeb) {
      // If platform is not Android or iOS so it needs different way to upload
      selectedImageFileObj = await picker.pickImage(source: media);
      return await JJ.fileUpload(File(selectedImageFileObj!.path));
    } else {
      //In web file upload is like below
      XFile? selectedImageFileObj;
      final ImagePicker picker = ImagePicker();
      selectedImageFileObj = await picker.pickImage(source: media);
      if (selectedImageFileObj != null) {
        var f = await selectedImageFileObj.readAsBytes();
        Uint8List webImage = f;
        return await JJ.fileUploadWeb(webImage);
      } else {
        debugPrint("No file selected");
        return "";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            const Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text(
                    "عکس کالا",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                )),
            Row(
              children: [
                Expanded(
                    child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        String imageName = "";
                        if (AddNewProductByUser.picArray[0].isEmpty) {
                          //get new image if image is empty
                          imageName = await getImageAndUpload(ImageSource.gallery);
                          AddNewProductByUser.picArray[0] = imageName;
                        }
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: addOneImageForProduct(
                                  picNo: 1,
                                ))).then((value) => callback());
                        // showDialogForAddImageInBGremoverQueue(imageName);
                        setState(() {
                          debugPrint('AddNewProductByUser.picArray[0]>>>>${AddNewProductByUser.picArray[0]}');
                        });
                      },
                      child: AddNewProductByUser.picArray[0].isEmpty //If image is not set
                          ? //If file not uploaded yet
                          Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Text(
                                      "1",
                                      style: TextStyle(color: Colors.white, fontSize: 25),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70)),
                              ],
                            )
                          : Stack(children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.white),
                                  child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[0]}")),
                              Positioned(
                                left: 10,
                                child: InkWell(
                                    onTap: () {
                                      AddNewProductByUser.picArray[0] = '';
                                      setState(() {});
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.deepOrange,
                                    )),
                              ),
                            ]),
                    ),
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        String imageName = "";
                        if (AddNewProductByUser.picArray[1].isEmpty) {
                          //get new image if image is empty
                          imageName = await getImageAndUpload(ImageSource.gallery);
                          AddNewProductByUser.picArray[1] = imageName;
                        }
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: addOneImageForProduct(
                                  picNo: 2,
                                ))).then((value) => callback());
                        // showDialogForAddImageInBGremoverQueue(imageName);
                        setState(() {
                          debugPrint('AddNewProductByUser.picArray[1]>>>>${AddNewProductByUser.picArray[1]}');
                        });
                      },
                      child: AddNewProductByUser.picArray[1].isEmpty //If image is not set
                          ? //If file not uploaded yet
                          Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Text(
                                      "2",
                                      style: TextStyle(color: Colors.white, fontSize: 25),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70)),
                              ],
                            )
                          : Stack(children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.white),
                                  child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[1]}")),
                              Positioned(
                                left: 10,
                                child: InkWell(
                                    onTap: () {
                                      AddNewProductByUser.picArray[1] = '';
                                      setState(() {});
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.deepOrange,
                                    )),
                              ),
                            ]),
                    ),
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        String imageName = "";
                        if (AddNewProductByUser.picArray[2].isEmpty) {
                          //get new image if image is empty
                          imageName = await getImageAndUpload(ImageSource.gallery);
                          AddNewProductByUser.picArray[2] = imageName;
                        }
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: addOneImageForProduct(
                                  picNo: 3,
                                ))).then((value) => callback());
                        // showDialogForAddImageInBGremoverQueue(imageName);
                        setState(() {
                          debugPrint('AddNewProductByUser.picArray[2]>>>>${AddNewProductByUser.picArray[2]}');
                        });
                      },
                      child: AddNewProductByUser.picArray[2].isEmpty //If image is not set
                          ? //If file not uploaded yet
                          Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Text(
                                      "3",
                                      style: TextStyle(color: Colors.white, fontSize: 25),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70)),
                              ],
                            )
                          : Stack(children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.white),
                                  child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[2]}")),
                              Positioned(
                                left: 10,
                                child: InkWell(
                                    onTap: () {
                                      AddNewProductByUser.picArray[2] = '';
                                      setState(() {});
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.deepOrange,
                                    )),
                              ),
                            ]),
                    ),
                  ],
                )),
              ],
            ),
            Row(
              children: [
                Expanded(
                    child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        String imageName = "";
                        if (AddNewProductByUser.picArray[3].isEmpty) {
                          //get new image if image is empty
                          imageName = await getImageAndUpload(ImageSource.gallery);
                          AddNewProductByUser.picArray[3] = imageName;
                        }
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: addOneImageForProduct(
                                  picNo: 4,
                                ))).then((value) => callback());
                        // showDialogForAddImageInBGremoverQueue(imageName);
                        setState(() {
                          debugPrint('AddNewProductByUser.picArray[3]>>>>${AddNewProductByUser.picArray[3]}');
                        });
                      },
                      child: AddNewProductByUser.picArray[3].isEmpty //If image is not set
                          ? //If file not uploaded yet
                          Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Text(
                                      "4",
                                      style: TextStyle(color: Colors.white, fontSize: 25),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70)),
                              ],
                            )
                          : Stack(children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.white),
                                  child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[3]}")),
                              Positioned(
                                left: 10,
                                child: InkWell(
                                    onTap: () {
                                      AddNewProductByUser.picArray[3] = '';
                                      setState(() {});
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.deepOrange,
                                    )),
                              ),
                            ]),
                    ),
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        String imageName = "";
                        if (AddNewProductByUser.picArray[4].isEmpty) {
                          //get new image if image is empty
                          imageName = await getImageAndUpload(ImageSource.gallery);
                          AddNewProductByUser.picArray[4] = imageName;
                        }
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: addOneImageForProduct(
                                  picNo: 5,
                                ))).then((value) => callback());
                        // showDialogForAddImageInBGremoverQueue(imageName);
                        setState(() {
                          debugPrint('AddNewProductByUser.picArray[4]>>>>${AddNewProductByUser.picArray[4]}');
                        });
                      },
                      child: AddNewProductByUser.picArray[4].isEmpty //If image is not set
                          ? //If file not uploaded yet
                          Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Text(
                                      "5",
                                      style: TextStyle(color: Colors.white, fontSize: 25),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70)),
                              ],
                            )
                          : Stack(children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.white),
                                  child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[4]}")),
                              Positioned(
                                left: 10,
                                child: InkWell(
                                    onTap: () {
                                      AddNewProductByUser.picArray[4] = '';
                                      setState(() {});
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.deepOrange,
                                    )),
                              ),
                            ]),
                    ),
                  ],
                )),

                Expanded(
                    child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        String imageName = "";
                        if (AddNewProductByUser.picArray[5].isEmpty) {
                          //get new image if image is empty
                          imageName = await getImageAndUpload(ImageSource.gallery);
                          AddNewProductByUser.picArray[5] = imageName;
                        }
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: addOneImageForProduct(
                                  picNo: 6,
                                ))).then((value) => callback());
                        // showDialogForAddImageInBGremoverQueue(imageName);
                        setState(() {
                          debugPrint('AddNewProductByUser.picArray[5]>>>>${AddNewProductByUser.picArray[5]}');
                        });
                      },
                      child: AddNewProductByUser.picArray[5].isEmpty //If image is not set
                          ? //If file not uploaded yet
                          Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Text(
                                      "6",
                                      style: TextStyle(color: Colors.white, fontSize: 25),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70)),
                              ],
                            )
                          : Stack(children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.white),
                                  child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[5]}")),
                              Positioned(
                                left: 10,
                                child: InkWell(
                                    onTap: () {
                                      AddNewProductByUser.picArray[5] = '';
                                      setState(() {});
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.deepOrange,
                                    )),
                              ),
                            ]),
                    ),
                  ],
                )),
              ],
            ),
            Row(
              children: [
                Expanded(
                    child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        String imageName = "";
                        if (AddNewProductByUser.picArray[6].isEmpty) {
                          //get new image if image is empty
                          imageName = await getImageAndUpload(ImageSource.gallery);
                          AddNewProductByUser.picArray[6] = imageName;
                        }
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: addOneImageForProduct(
                                  picNo: 7,
                                ))).then((value) => callback());
                        // showDialogForAddImageInBGremoverQueue(imageName);
                        setState(() {
                          debugPrint('AddNewProductByUser.picArray[6]>>>>${AddNewProductByUser.picArray[6]}');
                        });
                      },
                      child: AddNewProductByUser.picArray[6].isEmpty //If image is not set
                          ? //If file not uploaded yet
                          Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Text(
                                      "7",
                                      style: TextStyle(color: Colors.white, fontSize: 25),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70)),
                              ],
                            )
                          : Stack(children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.white),
                                  child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[6]}")),
                              Positioned(
                                left: 10,
                                child: InkWell(
                                    onTap: () {
                                      AddNewProductByUser.picArray[6] = '';
                                      setState(() {});
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.deepOrange,
                                    )),
                              ),
                            ]),
                    ),
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        String imageName = "";
                        if (AddNewProductByUser.picArray[7].isEmpty) {
                          //get new image if image is empty
                          imageName = await getImageAndUpload(ImageSource.gallery);
                          AddNewProductByUser.picArray[7] = imageName;
                        }
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: addOneImageForProduct(
                                  picNo: 8,
                                ))).then((value) => callback());
                        // showDialogForAddImageInBGremoverQueue(imageName);
                        setState(() {
                          debugPrint('AddNewProductByUser.picArray[7]>>>>${AddNewProductByUser.picArray[7]}');
                        });
                      },
                      child: AddNewProductByUser.picArray[7].isEmpty //If image is not set
                          ? //If file not uploaded yet
                          Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Text(
                                      "8",
                                      style: TextStyle(color: Colors.white, fontSize: 25),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70)),
                              ],
                            )
                          : Stack(children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.white),
                                  child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[7]}")),
                              Positioned(
                                left: 10,
                                child: InkWell(
                                    onTap: () {
                                      AddNewProductByUser.picArray[7] = '';
                                      setState(() {});
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.deepOrange,
                                    )),
                              ),
                            ]),
                    ),
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        String imageName = "";
                        if (AddNewProductByUser.picArray[8].isEmpty) {
                          //get new image if image is empty
                          imageName = await getImageAndUpload(ImageSource.gallery);
                          AddNewProductByUser.picArray[8] = imageName;
                        }
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: addOneImageForProduct(
                                  picNo: 9,
                                ))).then((value) => callback());
                        // showDialogForAddImageInBGremoverQueue(imageName);
                        setState(() {
                          debugPrint('AddNewProductByUser.picArray[8]>>>>${AddNewProductByUser.picArray[8]}');
                        });
                      },
                      child: AddNewProductByUser.picArray[8].isEmpty //If image is not set
                          ? //If file not uploaded yet
                          Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Text(
                                      "9",
                                      style: TextStyle(color: Colors.white, fontSize: 25),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70)),
                              ],
                            )
                          : Stack(children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.white),
                                  child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[8]}")),
                              Positioned(
                                left: 10,
                                child: InkWell(
                                    onTap: () {
                                      AddNewProductByUser.picArray[8] = '';
                                      setState(() {});
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.deepOrange,
                                    )),
                              ),
                            ]),
                    ),
                  ],
                )),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text.rich(
                TextSpan(style: const TextStyle(fontSize: 12, height: 2), children: [
                  const TextSpan(text: "کاربر گرامی اگر عکس های ارسالی شما طبق"),
                  TextSpan(
                    text: " استاندارد و قوانین سایت ",
                    style: const TextStyle(color: Color(0xffe8ac4f)),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        //show rules and conditions
                        showDialog(
                            context: context,
                            useSafeArea: true,
                            builder: (_) => AlertDialog(
                                  insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                  shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                  contentPadding: const EdgeInsets.all(0.0),
                                  content: Container(
                                    child: const Rules(),
                                  ),
                                ));
                        // Single tapped.
                      },
                  ),
                  const TextSpan(text: "نباشد از طرف کارشناسان ما رد می شود"),
                ]),
                softWrap: true,
                textAlign: TextAlign.right,
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      // localStorage ?? await SharedPreferences.getInstance();
                      AddNewProductByUser.picArray[0] = '';
                      AddNewProductByUser.picArray[1] = '';
                      AddNewProductByUser.picArray[2] = '';
                      AddNewProductByUser.picArray[3] = '';
                      AddNewProductByUser.picArray[4] = '';
                      AddNewProductByUser.picArray[5] = '';
                      AddNewProductByUser.picArray[6] = '';
                      AddNewProductByUser.picArray[7] = '';
                      AddNewProductByUser.picArray[8] = '';

                      //show rules and conditions
                      setState(() {});
                      // Single tapped.
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: 120,
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(8),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.red,
                      ),
                      child: Text(
                        " حذف تمام تصاویر ",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: InkWell(
                      onTap: () {
                        Navigator.pop(context, () {});
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: 120,
                        margin: const EdgeInsets.all(20),
                        padding: const EdgeInsets.all(8),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          color: Colors.green,
                        ),
                        child: Text(" ثبت و ادامه"),
                      )),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
