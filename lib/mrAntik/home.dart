import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:go_router/go_router.dart';
import 'package:share_plus/share_plus.dart';
import 'package:video_player/video_player.dart';
import 'login.dart';
import 'register.dart';
import 'product.dart';
import 'auction.dart';
import 'tools/jjTools.dart';
import 'widgets/allertDialog.dart';
import 'widgets/navBarBottom.dart';
import 'widgets/navBarTop.dart';
import 'package:page_transition/page_transition.dart';

import 'products.dart';
import 'AuctionsBySeller.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);
  static String category = "";
  static List sliderImagesJson = [];
  static List banner1ImagesJson = [];
  static List banner2ImagesJson = [];
  static List banner3ImagesJson = [];
  static late VideoPlayerController _controller1; //if first banner is .mp4 video
  static late VideoPlayerController _controller2; //if second banner is .mp4 video
  static late VideoPlayerController _controller3; //if third banner is .mp4 video
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<dynamic> topProductList = [1];
  List<dynamic> topAuctionList = [1];

  @override
  initState() {
    super.initState();
    getTopProducts();
    getTopAuctions();
    if (HomePage.banner1ImagesJson.isNotEmpty) if ("${JJ.server}upload/${HomePage.banner1ImagesJson[0]['pic_pic_name']}".contains(".mp4")) {
      HomePage._controller1 = VideoPlayerController.networkUrl(Uri.parse("${JJ.server}upload/${HomePage.banner1ImagesJson[0]['pic_pic_name']}"))
        ..initialize().then((_) {
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          HomePage._controller1.play();
          HomePage._controller1.setLooping(true);
          setState(() {});
        });
    }
    if (HomePage.banner2ImagesJson.isNotEmpty) if ("${JJ.server}upload/${HomePage.banner2ImagesJson[0]['pic_pic_name']}".contains(".mp4")) {
      HomePage._controller2 = VideoPlayerController.networkUrl(Uri.parse("${JJ.server}upload/${HomePage.banner2ImagesJson[0]['pic_pic_name']}"))
        ..initialize().then((_) {
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          HomePage._controller2.play();
          HomePage._controller2.setLooping(true);
          setState(() {});
        });
    }
    if (HomePage.banner3ImagesJson.isNotEmpty) if ("${JJ.server}upload/${HomePage.banner3ImagesJson[0]['pic_pic_name']}".contains(".mp4")) {
      HomePage._controller3 = VideoPlayerController.networkUrl(Uri.parse("${JJ.server}upload/${HomePage.banner3ImagesJson[0]['pic_pic_name']}"))
        ..initialize().then((_) {
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          HomePage._controller3.play();
          HomePage._controller3.setLooping(true);
          setState(() {});
        });
    }

    if (JJ.user_token != "") {
      //refresh user date in serverSide session
      String params = "do=Access_User.loginUserWhitToken";
      params += "&user_token=${JJ.user_token}";
      JJ().jjAjax(params);
    }
  }

  getTopProducts() async {
    debugPrint('getTopProducts() ...');
    String result = await JJ().jjAjax('do=Product.getTopProducts&orderBy=product_visit');
    // topProductList=[1];
    if (result.isNotEmpty) {
      topProductList.addAll(jsonDecode(result));
      setState(() {});
    }
  }

  getTopAuctions() async {
    debugPrint('getTopAuctions() ...');
    String result = await JJ().jjAjax('do=Product.getTopAuctions&orderBy=product_visit'
        '&product_isAuction=1' //The different between get products and get ausctions
        );
    // topProductList=[1];
    if (result.isNotEmpty) {
      topAuctionList.addAll(jsonDecode(result));
      setState(() {});
    }
  }

  processOnClickScript(String script) async {
    if (HomePage.banner1ImagesJson[0]['pic_scriptOnClickImg'].toString().startsWith("Auction=")) {
      String auctionId = script.substring(8); //Finding auction id
      debugPrint('proceesOnClickScript get auction($auctionId) ...');
      String result = await JJ().jjAjax('do=Product.getTopAuctions'
          '&filterBy= AND product.id=$auctionId' // set filter to get only specific product by id
          );
      List<dynamic> temp = jsonDecode(result);
      if (temp.isEmpty) {
        JJ.jjToast('آیتم دیگری وجود ندارد');
        return;
      }
      Navigator.push(context, PageTransition(type: PageTransitionType.leftToRight, child: Auction(productJSON: temp[0])));
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    //=====================================================================

    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Scaffold(
            appBar: AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Colors.black,
                centerTitle: true,
                title: const Row(
                  children: [
                    Expanded(
                      flex: 10,
                      child: Image(
                        image: AssetImage('images/logo.png'),
                        height: 75,

                      ),
                    ),
                  ],
                )),
            // 1st Section =========================================================
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    const NavBarTop(),
                    // 2nd Section Login&Register or Invite friends=========================================================
                    const SizedBox(
                      height: 20,
                    ),
                    if (JJ.user_token != "") // If user is  login show InviteFriends btn
                      InkWell(
                        onTap: () {
                          Share.share('سلام؛ من کاربر اپلیکیشن وتمام شدم، پیشنهاد میکنم  https://www.vatamam.com');
                        },
                        child: Container(
                          margin: const EdgeInsets.only(bottom: 15),
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                            border: Border.all(),
                            color: const Color(0xffe8ac4f),
                            // image: DecorationImage(
                            //   fit: BoxFit.cover,
                            //   image: AssetImage(
                            //     "images/textFielsBg.png",
                            //   ),
                            // )
                          ),
                          child: const Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'دعوت از دوستان   ',
                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                              ),
                              // Image.asset("images/myMenu_InviteFriends.png", scale: 5,),
                              Icon(Icons.share),
                            ],
                          ),
                        ),
                      ),
                    if (JJ.user_token == "") // If user is not login show Login&Register btn
                      Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: InkWell(
                                      onTap: () {
                                        showDialog(
                                            context: context,
                                            useSafeArea: true,
                                            builder: (_) => AlertDialog(
                                                  insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                                  shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                                  contentPadding: const EdgeInsets.all(0.0),
                                                  content: Register(),
                                                ));
                                      },
                                      child: Image.asset("images/register.png"))),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                  child: InkWell(
                                      onTap: () {
                                        showDialog(
                                            context: context,
                                            useSafeArea: true,
                                            builder: (_) => AlertDialog(
                                                  insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                                  shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                                  contentPadding: const EdgeInsets.all(0.0),
                                                  content: Login(),
                                                ));
                                      },
                                      child: Image.asset("images/login.png"))),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          // InkWell(
                          //     onTap: () {
                          //
                          //     },
                          //     child: Image.asset("images/register.png" , scale: 6
                          //       ,)),
                          const SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                child: const AuctionsBySeller(
                                  categoryId: '',
                                  categoryTitle: 'جدید ترین ها',
                                )));
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.asset(
                          "images/temp/auction.png",
                        ),
                      ),
                    ),
                    // 3th Section top Auctions slider =========================================================
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      //note: Auction Sliders using carousel slider
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: CarouselSlider(
                          options: CarouselOptions(
                            height: 100.0,
                            viewportFraction: 0.3,
                            initialPage: 0,
                            enableInfiniteScroll: false,
                            padEnds: false,
                          ),
                          items: topAuctionList.map((item) {
                            return (item == 1 //The first item use an icon
                                ? Builder(
                                    builder: (BuildContext context) {
                                      return Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: MediaQuery.of(context).size.height,
                                          margin: const EdgeInsets.symmetric(horizontal: 5.0),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      PageTransition(
                                                          type: PageTransitionType.leftToRight,
                                                          child: const AuctionsBySeller(
                                                            categoryId: '',
                                                            categoryTitle: 'پر بازدید ترین مزایده ها',
                                                          )));
                                                },
                                                child: Image.asset(
                                                  "images/more_auction.png",
                                                ),
                                              ),
                                            ],
                                          ));
                                    },
                                  )
                                : Builder( // For second item and more
                                    builder: (BuildContext context) {
                                      return InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType.leftToRight,
                                                  child: Auction(
                                                    productJSON: item,
                                                  )));
                                        },
                                        child: Container(
                                            width: MediaQuery.of(context).size.width,
                                            margin: const EdgeInsets.symmetric(horizontal: 7.0),
                                            decoration: BoxDecoration(
                                              color: Colors.black,
                                              border: Border.all(width: 1, color: Colors.grey),
                                            ),
                                            child: Image.network(
                                              '${JJ.server}upload/${item['product_pic1']}',
                                              fit: BoxFit.fill,
                                            )),
                                      );
                                    },
                                  ));
                          }).toList(),
                        ),
                      ),
                    ),
                    // 4th Section all Galleries  ==================================
                    const SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      onTap: () {
                        // context.push("/Products?categoryId=''&categoryTitle='جدید ترین ها'");
                        context.push("/Products?categoryId=''&categoryTitle='جدید ترین ها'");
                        // Navigator.push(
                        //     context,
                        //     PageTransition(
                        //         type: PageTransitionType.leftToRight,
                        //         child: const Products(
                        //           categoryId: '',
                        //           categoryTitle: 'جدید ترین ها',
                        //         )));
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.asset(
                          "images/temp/galleries.png",
                        ),
                      ),
                    ),
                    // 5th Section selected product slider =========================
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      //note: Galleries Sliders using carousel slider
                      decoration: BoxDecoration(
                        color: Colors.black12,
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(width: 1, color: Colors.black),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: CarouselSlider(
                          options: CarouselOptions(
                            height: 100.0,
                            viewportFraction: 0.3,
                            initialPage: 0,
                            enableInfiniteScroll: false,
                            padEnds: false,
                          ),
                          items: topProductList.map((item) {
                            return (item == 1
                                ? Builder(
                                    builder: (BuildContext context) {
                                      return Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: MediaQuery.of(context).size.height,
                                          margin: const EdgeInsets.symmetric(horizontal: 7.0),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      PageTransition(
                                                          type: PageTransitionType.leftToRight,
                                                          child: const Products(
                                                            categoryId: '',
                                                            categoryTitle: 'پر بازدید ترین',
                                                          )));
                                                },
                                                child: Image.asset(
                                                  "images/more_product.png",
                                                  fit: BoxFit.fitHeight,
                                                ),
                                              ),
                                            ],
                                          ));
                                    },
                                  )
                                : Builder(
                                    builder: (BuildContext context) {
                                      return InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType.leftToRight,
                                                  child: Product(
                                                    productJSON: item,
                                                  )));
                                        },
                                        child: Container(
                                            width: MediaQuery.of(context).size.width,
                                            margin: const EdgeInsets.symmetric(horizontal: 7.0),
                                            decoration: BoxDecoration(
                                              color: Colors.black12,
                                              border: Border.all(width: 1, color: Colors.black),
                                            ),
                                            child: Image.network(
                                              '${JJ.server}upload/${item['product_pic1']}',
                                              fit: BoxFit.fill,
                                            )),
                                      );
                                    },
                                  ));
                          }).toList(),
                        ),
                      ),
                    ),
                    // 6th Section Important sub categories=========================
                    Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                                child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: const Products(
                                          categoryId: '55',
                                          categoryTitle: 'آباژور',
                                        )));
                              },
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/temp/cat01.png",
                                    scale: 1.7,
                                  ),
                                  const Text(
                                    "آباژور",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            )),
                            Expanded(
                                child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: const Products(
                                          categoryId: '98',
                                          categoryTitle: 'سیلورپلیت',
                                        )));
                              },
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/temp/cat02.png",
                                    scale: 1.7,
                                  ),
                                  const Text(
                                    "سیلورپلیت",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            )),
                            Expanded(
                                child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: const Products(
                                          categoryId: '441',
                                          categoryTitle: 'ساعت دیواری',
                                        )));
                              },
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/temp/cat03.png",
                                    scale: 1.7,
                                  ),
                                  const Text(
                                    "ساعت دیواری",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            )),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: const Products(
                                          categoryId: '80',
                                          categoryTitle: 'سکه',
                                        )));
                              },
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/temp/cat04.png",
                                    scale: 1.7,
                                  ),
                                  const Text(
                                    "سکه",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            )),
                            Expanded(
                                child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: const Products(
                                          categoryId: '604',
                                          categoryTitle: 'انگشتر',
                                        )));
                              },
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/temp/cat06.png",
                                    scale: 1.7,
                                  ),
                                  const Text(
                                    "انگشتر",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            )),
                            Expanded(
                                child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: const Products(
                                          categoryId: '580',
                                          categoryTitle: 'دیوار کوب',
                                        )));
                              },
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/temp/cat05.png",
                                    scale: 1.7,
                                  ),
                                  const Text(
                                    "دیوارکوب",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            )),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: const Products(
                                          categoryId: '71',
                                          categoryTitle: 'اپالین',
                                        )));
                              },
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/temp/cat07.png",
                                    scale: 1.7,
                                  ),
                                  const Text(
                                    "اپالین",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            )),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          child: const Products(
                                            categoryId: '139',
                                            categoryTitle: 'تمبر',
                                          )));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      "images/temp/cat09.png",
                                      scale: 1.7,
                                    ),
                                    const Text(
                                      "تمبر",
                                      style: TextStyle(fontSize: 12),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                                child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: const Products(
                                          categoryId: '168',
                                          categoryTitle: 'مجسمه',
                                        )));
                              },
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/temp/cat08.png",
                                    scale: 1.7,
                                  ),
                                  const Text(
                                    "مجسمه",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            )),
                          ],
                        ),
                      ],
                    ),
                    // 7th Section banner1================================================
                    if (HomePage.banner1ImagesJson.isNotEmpty)
                      InkWell(
                        onTap: () {
                          var pic_scriptOnClickImg = HomePage.banner1ImagesJson[0]['pic_scriptOnClickImg'].toString();
                          if (pic_scriptOnClickImg.isEmpty) {
                            return;
                          } else {
                            processOnClickScript(pic_scriptOnClickImg);
                          }
                        },
                        child: //If it is .mp4 video
                            "${JJ.server}upload/${HomePage.banner1ImagesJson[0]['pic_pic_name']}".contains(".mp4")
                                ? Stack(
                                    children: [
                                      Container(
                                          width: MediaQuery.of(context).size.width,
                                          alignment: Alignment.center,
                                          margin: const EdgeInsets.only(top: 20),
                                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(8.0), color: Colors.black12, border: Border.all(color: Colors.black)),
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(8),
                                            child: HomePage._controller1.value.isInitialized
                                                ? AspectRatio(
                                                    aspectRatio: 2.1, //try different ration based on your preference
                                                    child: VideoPlayer(HomePage._controller1),
                                                  )
                                                : Image.network("${JJ.server}upload/${HomePage.banner1ImagesJson[0]['pic_pic_name']}", fit: BoxFit.cover),
                                          )),
                                      Positioned(
                                          top: 40,
                                          child: Container(
                                              width: MediaQuery.of(context).size.width,
                                              color: Colors.black12,
                                              alignment: Alignment.center,
                                              child: Text(
                                                '${HomePage.banner1ImagesJson[0]['pic_title']}',
                                                style: const TextStyle(
                                                  color: Colors.white,
                                                ),
                                              ))
                                      )
                                    ],
                                  )
                                : ClipRRect(
                                    borderRadius: BorderRadius.circular(8.0),
                                    child: Container(
                                      margin: const EdgeInsets.only(top: 20),
                                      height: 130,
                                      child: Image.network("${JJ.server}upload/${HomePage.banner1ImagesJson[0]['pic_pic_name']}", fit: BoxFit.cover),
                                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(8.0), color: Colors.black12, border: Border.all(color: Colors.black)),
                                    ),
                                  ),
                      ),
                    // Slider for images ===========================================================
                    const SizedBox(
                      height: 20,
                    ),
                    HomePage.sliderImagesJson.length == 1
                        ? Image.network('${JJ.server}upload/${HomePage.sliderImagesJson[0]['pic_pic_name']}')
                        :
                        //If there were more than one image in network
                        ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: CarouselSlider(
                              options: CarouselOptions(
                                height: 120,
                                autoPlay: true,
                                viewportFraction: 1,
                                padEnds: false,
                                enableInfiniteScroll: true,
                                //scrollDirection: Axis.vertical,
                              ),
                              items: HomePage.sliderImagesJson
                                  .map(
                                    (item) => GestureDetector(
                                      onTap: () {},
                                      child: Image.network(
                                        '${JJ.server}upload/${item['pic_pic_name']}',
                                        width: MediaQuery.of(context).size.width,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  )
                                  .toList(),
                            ),
                          ),

                    // 8th Section banner2================================================
                    if (HomePage.banner2ImagesJson.isNotEmpty)
                      InkWell(
                        onTap: () {
                          var pic_scriptOnClickImg = HomePage.banner2ImagesJson[0]['pic_scriptOnClickImg'].toString();
                          if (pic_scriptOnClickImg.isEmpty) {
                            return;
                          } else {
                            processOnClickScript(pic_scriptOnClickImg);
                          }
                        },
                        child: "${JJ.server}upload/${HomePage.banner2ImagesJson[0]['pic_pic_name']}".contains(".mp4")
                            ? Stack(
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(top: 20),
                                    height: 130,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(8.0), color: Colors.black12, border: Border.all(color: Colors.black)),
                                    child: HomePage._controller2.value.isInitialized
                                        ? ClipRRect(
                                            borderRadius: BorderRadius.circular(8),
                                            child: AspectRatio(
                                                aspectRatio: 2.1, //try different ration based on your preference
                                                child: VideoPlayer(HomePage._controller2)))
                                        : Image.network("${JJ.server}upload/${HomePage.banner2ImagesJson[0]['pic_pic_name']}", fit: BoxFit.cover),
                                  ),
                                  Positioned(
                                      top: 40,
                                      child: Container(
                                          width: MediaQuery.of(context).size.width,
                                          color: Colors.black12,
                                          alignment: Alignment.center,
                                          child: Text(
                                            '${HomePage.banner2ImagesJson[0]['pic_title']}',
                                            style: const TextStyle(
                                              color: Colors.white,
                                            ),
                                          )))
                                ],
                              )
                            : ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Container(
                                  margin: const EdgeInsets.only(top: 20),
                                  width: double.infinity,
                                  child: Image.network("${JJ.server}upload/${HomePage.banner2ImagesJson[0]['pic_pic_name']}", fit: BoxFit.cover),
                                ),
                              ),
                      ),
                    // 9th download repositores ================================================
                    const SizedBox(
                      height: 20,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Container(
                        color: const Color(0xffe2e2e2),
                        height: 230,
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Column(
                            children: [
                              Image.asset("images/repo00.png",scale: 1.9,),
                              const SizedBox(
                                height: 15,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                      child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(8),
                                            border: Border.all(color: Colors.white,width: 2),
                                            color: Colors.white,
                                          ),
                                          height: 45,
                                          child: Image.asset(
                                            "images/repo01.jpg",
                                            scale: 1.9,
                                            fit:BoxFit.cover
                                          ))),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                  Expanded(
                                      child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(8),
                                            border: Border.all(color: Colors.white,width: 2),
                                            color: Colors.white,
                                          ),
                                          height: 45,
                                          child: Image.asset(
                                            "images/repo04.jpg",
                                            scale: 1.9,
                                            fit:BoxFit.cover
                                          ))),
                                ],
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                      child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(8),
                                            border: Border.all(color: Colors.white,width: 2),
                                            color: Colors.white,
                                          ),
                                          height: 45,
                                          child: Image.asset(
                                            "images/repo02.jpg",
                                            scale: 1.9,
                                            fit: BoxFit.cover,
                                          ))),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                  Expanded(
                                      child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(8),
                                            border: Border.all(color: Colors.white,width: 2),
                                            color: Colors.white,
                                          ),
                                          height: 45,
                                          child: Image.asset(
                                            "images/repo03.jpg",
                                            scale: 1.9,
                                            fit: BoxFit.cover,
                                          ))),
                                ],
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  height: 40,
                                  child: InkWell(
                                    onTap: () {
                                      showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => const jjAlertDialog(
                                          richText: Text("1- در نوار پایین دکمه ی     " + "share " + "را انتخاب کنید" + "\n" + "2- در منوی باز شده گزینه ی " + "Add to home screen " + "را انتخاب کنید" + "\n" + "3- در مرحله ی بعد در قسمت بالا روی Add کلیک کنید" + "\n"),
                                          title: "Add to home screen",
                                          type: "info",
                                        ),
                                      );
                                    },
                                    child: Image.asset(
                                        "images/repo05.png",
                                        fit:BoxFit.cover
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // 10th Section banner3================================================
                    if (HomePage.banner3ImagesJson.isNotEmpty)
                      InkWell(
                        onTap: () {
                          var pic_scriptOnClickImg = HomePage.banner3ImagesJson[0]['pic_scriptOnClickImg'].toString();
                          if (pic_scriptOnClickImg.isEmpty) {
                            return;
                          } else {
                            processOnClickScript(pic_scriptOnClickImg);
                          }
                        },
                        child: "${JJ.server}upload/${HomePage.banner3ImagesJson[0]['pic_pic_name']}".contains(".mp4")
                            ? Stack(
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(top: 20),
                                    height: 130,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(8.0), color: Colors.black12, border: Border.all(color: Colors.black)),
                                    child: HomePage._controller3.value.isInitialized
                                        ? ClipRRect(
                                            borderRadius: BorderRadius.circular(8),
                                            child: AspectRatio(
                                                aspectRatio: 2.1, //try different ration based on your preference
                                                child: VideoPlayer(HomePage._controller3)))
                                        : Image.network("${JJ.server}upload/${HomePage.banner3ImagesJson[0]['pic_pic_name']}", fit: BoxFit.cover),
                                  ),
                                  Positioned(
                                      top: 40,
                                      child: Container(
                                          width: MediaQuery.of(context).size.width,
                                          color: Colors.black12,
                                          alignment: Alignment.center,
                                          child: Text(
                                            '${HomePage.banner3ImagesJson[0]['pic_title']}',
                                            style: const TextStyle(
                                              color: Colors.white,
                                            ),
                                          )))
                                ],
                              )
                            : ClipRRect(
                                //
                                borderRadius: BorderRadius.circular(8.0),
                                child: Container(
                                  margin: const EdgeInsets.only(top: 20),
                                  width: double.infinity,
                                  child: Image.network("${JJ.server}upload/${HomePage.banner3ImagesJson[0]['pic_pic_name']}", fit: BoxFit.cover),
                                ),
                              ),
                      ),
                  ],
                ),
              ),
            ),
            // 11th Section Footer================================================
            bottomNavigationBar: const NavBarBottom()));
  }
}
