import 'dart:convert';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vaTamam/mrAntik/register.dart';
import 'package:vaTamam/mrAntik/tools/jjTools.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home.dart';
import 'passwordRecovery.dart';

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
  String errorsForForm = "";
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    final _signUpKey = GlobalKey<FormState>();

    final Map<String, TextEditingController> loginController = {
      'user_mobile': TextEditingController(),
      'user_pass': TextEditingController(),
    };
    debugPrint("JJ.user_token3==${JJ.user_token}");
    return SingleChildScrollView(
        child: Form(
      key: _signUpKey,
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            //-----------------------Login Form
            Container(
              decoration: BoxDecoration(
                color: const Color(0xfef7edff),
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(color: Colors.white54),
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset("images/login.png", scale: 7.5),
                      const SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 30,
                    margin:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "شماره همراه: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: TextFormField(
                            controller: loginController['user_mobile'],
                            autofocus: true,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                RegExp("[0-9]+"),
                              ),
                            ],
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: const InputDecoration(
                              hintText: 'کد ملی/شماره همراه',
                              hintStyle: TextStyle(
                                fontSize: 10,
                              ),
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(bottom: 20),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "رمز عبور: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: TextFormField(
                            controller: loginController['user_pass'],
                            textAlign: TextAlign.center,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: const InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(bottom: 20),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                      width: 250,
                      child: Text.rich(
                        TextSpan(
                            style: const TextStyle(fontSize: 12),
                            children: [
                              const TextSpan(
                                  text: "رمز عبور را فراموش کرده ام! "),
                              TextSpan(
                                text: "بازیابی رمز عبور",
                                style:
                                    const TextStyle(color: Color(0xffe8ac4f)),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    //show rules and conditions
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => AlertDialog(
                                              insetPadding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 20,
                                                      vertical: 40),
                                              shape: const RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              5.0))),
                                              contentPadding:
                                                  const EdgeInsets.all(0.0),
                                              content: PasswordRecovery(),
                                            ));
                                    // Single tapped.
                                  },
                              ),
                            ]),
                        maxLines: (2),
                        softWrap: true,
                        textAlign: TextAlign.center,
                      )),
                  const SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                      width: 250,
                      child: Text.rich(
                        TextSpan(
                            style: const TextStyle(fontSize: 12),
                            children: [
                              const TextSpan(text: "قبلاُ ثبت نام نکرده ام "),
                              TextSpan(
                                text: "ثبت نام کنید",
                                style:
                                    const TextStyle(color: Color(0xffe8ac4f)),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.pop(
                                        context); //Remove last dialog boxes
                                    //show rules and conditions
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => AlertDialog(
                                              insetPadding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 20,
                                                      vertical: 40),
                                              shape: const RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              5.0))),
                                              contentPadding:
                                                  const EdgeInsets.all(0.0),
                                              content: Register(),
                                            ));
                                    // Single tapped.
                                  },
                              ),
                            ]),
                        maxLines: (2),
                        softWrap: true,
                        textAlign: TextAlign.center,
                      )),
                  const SizedBox(height: 15),
                  Text(widget.errorsForForm,
                      style: const TextStyle(color: Colors.red, fontSize: 11)),
                  const SizedBox(height: 15),
                  TextButton(
                    onPressed: () async {
                      if (!_signUpKey.currentState!.validate()) {
                        return;
                      }
                      // To get data I wrote an extension method bellow
                      String params = "do=Access_User.loginUser";
                      for (var key in loginController.keys) {
                        params += "&" + key + "=";
                        var val = loginController[key]?.text;
                        params += val!;
                        debugPrint(key);
                        debugPrint(loginController[key]?.text);
                      }
                      setState(() {});
                      widget.errorsForForm = 'در حال ارسال اطلاعات ...';
                      await JJ().jjAjax(params).then((result) async {
                        debugPrint("======$result");
                        Map<String, dynamic> json = jsonDecode(result);
                        SharedPreferences? localStorage;
                        localStorage = await SharedPreferences.getInstance();
                        if (json['status'] == "0") {
                          widget.errorsForForm = json['comment'];
                          setState(() {}); // for refresh changes
                          JJ.user_token = "";
                          localStorage.setString("user_token", "");
                        } else {
                          Navigator.of(context, rootNavigator: true).pop();
                          JJ.user_token = json['user_token'].toString();
                          localStorage.setString(
                              "user_token", json['user_token'].toString());
                          JJ.user_id = json['id'].toString();
                          JJ.user_name = json['user_name'].toString();
                          JJ.user_username = json['user_username'].toString();
                          JJ.user_family = json['user_family'].toString();
                          JJ.user_mobile = json['user_mobile'].toString();
                          JJ.user_address = json['user_address'].toString();
                          JJ.user_city = json['user_city'].toString();
                          JJ.user_grade = json['user_grade'].toString();
                          JJ.user_birthdate = json['user_birthdate'].toString();
                          JJ.user_AccountInformation = json['user_AccountInformation'].toString();
                          JJ.user_codeMeli = json['user_codeMeli'].toString();
                          JJ.userBalance = "0";
                          JJ.user_postalCode = json['user_postalCode'].toString();
                          JJ.user_email = json['user_email'].toString();
                          widget.errorsForForm =
                              "${json["user_name"].toString()} ${json['user_family'].toString()}";
                          widget.errorsForForm += ' عزیز خوش آمدید';
                          JJ.jjToast(widget.errorsForForm);
                        }
                      });

                      // JJ.jjAjax(params).then((value){
                      //   debugPrint(value);
                      // });
                      // if(result==""){
                      //
                      // }
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: 120,
                      padding: const EdgeInsets.all(8),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.black38,
                      ),
                      child: const Text(
                        "ورود",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    ));
  }
}
