import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/AuctionRow.dart';

import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/navBarBottom.dart';
import 'widgets/navBarTop.dart';

class MyAuctions extends StatefulWidget {
  const MyAuctions({Key? key}) : super(key: key);

  @override
  State<MyAuctions> createState() => _MyAuctionsState();
}

class _MyAuctionsState extends State<MyAuctions> {
  late List<dynamic> items = [];

  @override
  void initState() {
    super.initState();
    getMoreItem(0);
  }

  getMoreItem(int lastIndex) async {
    debugPrint('MyAuctions.getMoreItem()');
    String result = await JJ().jjAjax(
        'do=Product.getMyAuctions&lastIndex=$lastIndex');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('پایان لیست مزایداتی که اعلام قیمت کرده اید}');
      return;
    }
    items.addAll(temp); //Add new items to available items
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.black,
            centerTitle: true,
            title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const NavBarTop(),
                // 2th Section =========================================================
                const SizedBox(
                  height: 10,
                ),
                // 3th Section =========================================================
                //########################################
                for (int i = 0; i < items.length; i++)
                  AuctionRow(productJSON: items[i]),
                //########################################
                const SizedBox(
                  height: 10,
                ),

                InkWell(
                  onTap: () {
                    getMoreItem(items.length);
                  },
                  child: const Center(
                    child: Icon(
                      Icons.add_circle_outlined,
                      color: Colors.black12,
                      size: 55,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        //Footer================================================
        bottomNavigationBar: const NavBarBottom());
  }
}
