import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/myLikedProducts.dart';
import 'package:vaTamam/mrAntik/widgets/AuctionRow.dart';
import 'package:vaTamam/mrAntik/widgets/navBarTop.dart';
import 'package:vaTamam/mrAntik/widgets/productRow.dart';
import 'package:page_transition/page_transition.dart';

import 'login.dart';
import 'register.dart';
import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/navBarBottom.dart';
import 'widgets/navBarTop.dart';
import 'widgets/sellerAndLastAuctionRow.dart';

class MyFollowingGalleries extends StatefulWidget {
  final String categoryTitle = 'لیست کالاهای محبوب';

  const MyFollowingGalleries({Key? key}) : super(key: key);

  @override
  State<MyFollowingGalleries> createState() => _MyFollowingGalleriesState();
}

class _MyFollowingGalleriesState extends State<MyFollowingGalleries> {
  late List<dynamic> items = [];

  @override
  void initState() {
    super.initState();
    getMoreItem(0);
  }

  getMoreItem(int lastIndex) async {
    String result = await JJ().jjAjax('do=Product.MyFollowingGalleries&lastIndex=$lastIndex');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('موردی در لیست گالری های محبوب شما وجود ندارد');
      return;
    }
    items.addAll(temp); //Add new items to available items
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const NavBarTop(),
                // 2nd Section =========================================================
                const SizedBox(
                  height: 20,
                ),
                if (JJ.user_token == "")
                  Row(
                    children: [
                      Expanded(
                          child: InkWell(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    useSafeArea: true,
                                    builder: (_) => AlertDialog(
                                          insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                          contentPadding: const EdgeInsets.all(0.0),
                                          content: Register(),
                                        ));
                              },
                              child: Image.asset("images/register.png"))),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: InkWell(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    useSafeArea: true,
                                    builder: (_) => AlertDialog(
                                          insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                          contentPadding: const EdgeInsets.all(0.0),
                                          content: Login(),
                                        ));
                              },
                              child: Image.asset("images/login.png"))),
                    ],
                  )
                else
                  Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: (){
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.leftToRight,
                                    child: const MyLikedProducts()));
                          },
                          child: Container(
                            height: 40,
                            margin: const EdgeInsets.all(0),
                            decoration:  BoxDecoration(
                                border: Border.all(color: Colors.black12),
                                borderRadius: BorderRadius.only(
                                    topLeft:  Radius.circular(30),topRight:  Radius.circular(30)),
                                color: Colors.white
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  widget.categoryTitle,
                                ),
                                Icon(Icons.favorite_border,)
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: 40,
                          margin: const EdgeInsets.all(0),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black12),
                              borderRadius: const BorderRadius.only(
                                  topLeft:  Radius.circular(30),topRight:  Radius.circular(30)),
                              color: Colors.black
                          ),
                          child: const Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "لیست گالری های محبوب",
                                textAlign: TextAlign.center,
                                style: TextStyle(color:  Color(0xffe8ac4f)),
                              ),
                              Icon(Icons.favorite , color:  Color(0xffe8ac4f),)
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                // 3th Section =========================================================
                const SizedBox(
                  height: 10,
                ),

                // 6th Section. All top products in for loop=========================================================
                //########################################
                for (int i = 0; i < items.length; i++) SellerAndLastAuctionRow(productJSON: items[i]),
                //########################################
                const SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () {
                    getMoreItem(items.length);
                  },
                  child: const Center(
                    child: Icon(
                      Icons.add_circle_outlined,
                      color: Colors.black12,
                      size: 55,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        //Footer================================================
        bottomNavigationBar: const NavBarBottom());
  }
}
