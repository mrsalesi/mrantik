import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'package:vaTamam/mrAntik/widgets/allertDialog.dart';
import 'package:vaTamam/mrAntik/widgets/navBarBottom.dart';
import 'package:page_transition/page_transition.dart';

import 'addNewProductSubCategories.dart';
import 'addNewProduct.dart';
import 'tools/jjTools.dart';

class Categories extends StatefulWidget {
  final List<dynamic>? categoriesJSON;

  const Categories({Key? key, required this.categoriesJSON}) : super(key: key);

  @override
  State<Categories> createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  late List<dynamic> items = [];

  search(String query) async {
    debugPrint('Categories.search()');
    JJ.jjToast('در حال جستجو ...');
    String result = await JJ().jjAjax('do=ProductCategory.search'
        '&query=$query');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('جستجو نتیجه ای نداشت: $query');
      return;
    }
    items = temp; //Add new items to available items
    setState(() {});
  }

  var searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    debugPrint('----------------------Categories----------------------------');
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Scaffold(
          appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
          // 1st Section =========================================================
          body: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: SizedBox(
                      height: 40,
                      child: TextField(
                        controller: searchController,
                        textInputAction: TextInputAction.go,
                        decoration: InputDecoration(
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black54, width: 1),
                            ),
                            border: const OutlineInputBorder(),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey, width: 1.0),
                            ),
                            labelText: 'جستجو برای انتخاب دسته بندی ...',
                            labelStyle: const TextStyle(
                              color: Colors.black54,
                            ),
                            suffixIcon:searchController.text.length >2 ? InkWell(//Show search icon after type 3 character
                                onTap: () {
                                  if (searchController.text.isNotEmpty) {
                                    search(searchController.text);
                                  }
                                },
                                child: const Image(image: AssetImage("images/search.png"))) : null,
                            hintText: "حداقل سه کاراکتر",
                            hintStyle: TextStyle(fontSize: 12),
                            filled: true,
                            fillColor: Colors.black12),
                        onChanged: (val){
                          if (searchController.text.length==2) {//Clear last search result and show default categories
                            items.clear();
                          }
                          setState(() {});
                        },
                        onSubmitted: (value) {
                          if (value.length > 2) {
                            search(value);
                          } else {
                            items.clear();
                          }
                        },
                      ),
                    ),
                  ),
                ),
                for (int i = 0; i < (items.length); i++)
                  Column(
                    children: [
                      SizedBox(
                        height: 35,
                        child: InkWell(
                          onTap: () async {
                            await JJ().jjAjax('do=ProductCategory.getMenu&productCategory_parent=${items![i]['id']}').then((result) async {
                              //If there was no sub categories go to add product page
                              List<dynamic> featuresJsonArray;
                              await JJ().jjAjax('do=ProductCategoryFeature.getSelectOptionTags&CategoryFeature_categoryId=${items![i]['id']}').then((result2) async {
                                featuresJsonArray = jsonDecode(result2);
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: AddNewProductByUser(
                                          categoryId: items![i]['id'],
                                          categoryTitle: "${items![i]['productCategory_title']}",
                                          featuresJsonArray: featuresJsonArray,
                                        )));
                              });
                            }); // AddNewProductByUser(id:widget.categoriesJSON![i]['id'])));
                          },
                          child: Row(
                            children: [
                              Expanded(flex: 7, child: Text(items![i]['full_path'])),
                              Expanded(
                                  flex: 1,
                                  child: Image.asset(
                                    "images/leftArrow.png",
                                    scale: 9,
                                  )),
                            ],
                          ),
                        ),
                      ),
                      const Divider(
                        color: Colors.black38,
                        thickness: 1,
                      ),
                    ],
                  ),
                if (items.isEmpty)
                  for (int i = 0; i < (widget.categoriesJSON?.length ?? 0); i++)
                    Column(
                      children: [
                        SizedBox(
                          height: 35,
                          child: InkWell(
                            onTap: () async {
                              await JJ().jjAjax('do=ProductCategory.getMenu&productCategory_parent=${widget.categoriesJSON![i]['id']}').then((result) async {
                                List<dynamic> temp = jsonDecode(result);
                                if (temp.isEmpty) {
                                  //If there was no sub categories go to add product page
                                  List<dynamic> featuresJsonArray;
                                  await JJ().jjAjax('do=ProductCategoryFeature.getSelectOptionTags&CategoryFeature_categoryId=${widget.categoriesJSON![i]['id']}').then((result2) async {
                                    featuresJsonArray = jsonDecode(result2);
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.leftToRight,
                                            child: AddNewProductByUser(
                                              categoryId: widget.categoriesJSON![i]['id'],
                                              categoryTitle: "${widget.categoriesJSON![i]['productCategory_title']}",
                                              featuresJsonArray: featuresJsonArray,
                                            )));
                                  });
                                } else {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          child: SubCategories(
                                            categoriesJSON: temp,
                                            parenCategoriyTitle: widget.categoriesJSON![i]['productCategory_title'],
                                            parenCategoriyPic: widget.categoriesJSON![i]['productCategory_pic'],
                                          )));
                                }
                              }); // AddNewProductByUser(id:widget.categoriesJSON![i]['id'])));
                            },
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: Image.network(
                                      JJ.server + 'upload/' + (widget.categoriesJSON![i]['productCategory_pic']),
                                      scale: 3,
                                    )),
                                Expanded(flex: 7, child: Text(widget.categoriesJSON![i]['productCategory_title'])),
                                Expanded(
                                    flex: 1,
                                    child: Image.asset(
                                      "images/leftArrow.png",
                                      scale: 9,
                                    )),
                              ],
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.black38,
                          thickness: 1,
                        ),
                      ],
                    ),
              ],
            ),
          ),
        ));
  }
}
