import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/tools/jjTools.dart';
import 'package:page_transition/page_transition.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'products.dart';
import 'sellerPhoneNubmer.dart';

class RegisteredSeller extends StatefulWidget {
  final String sellerId;

  const RegisteredSeller({Key? key, required this.sellerId}) : super(key: key);

  @override
  State<RegisteredSeller> createState() => _RegisteredSellerState();
}

class _RegisteredSellerState extends State<RegisteredSeller> {
  bool isFollowedByThisUser = true;
  String creatorId = '';
  String user_mobile = '000';
  String user_username = '';
  String user_name = '-';
  String user_family = '-';
  String user_credit = '';
  String user_createDate = '-';
  String countSales = '';
  String user_followers = '';

  @override
  void initState() {
    debugPrint('initState..  ---->>isFollowedByThisUser:::$isFollowedByThisUser');
    super.initState();
    getUserPhoneNumber(widget.sellerId);
  }

  Future<void> toggleFollow(String id) async {
    isFollowedByThisUser = isFollowedByThisUser ? false : true; //FOR toggle btn
    debugPrint('toggleFollow() isFollowedByThisUser:::$isFollowedByThisUser');
    String params = 'do=Access_User.toglleFollow&id=$id';
    String result = await JJ().jjAjax(params);
    Map<String, dynamic> json = jsonDecode(result);
    if (json['status'] == "0") {
      JJ.jjToast(json['comment']);
    } else {
      // JJ.jjToast(json['comment']);
      setState(() {});
    }
  }

  getUserPhoneNumber(String id) async {
    debugPrint('getUserPhoneNumber() ...');
    String result = await JJ().jjAjax('do=Access_User.getUserPhoneNumber&id=$id');
    Map<String, dynamic> json = jsonDecode(result);
    if (json['status'] == "0") {
      String comment = json['comment'];
      if (context.mounted) Navigator.of(context).pop();
      JJ.jjToast(json['comment']);
    } else {
      creatorId = json['id'];
      user_mobile = json['user_mobile'];
      user_username = json['user_username'];
      user_name = json['user_name'];
      user_family = json['user_family'];
      DateTime tempDate = DateTime.parse(json['user_createDate']);
      user_createDate = tempDate.toPersianDate();
      user_credit = json['user_credit'];
      countSales = json['countSales'];
      user_followers = json['user_followers'];
      debugPrint('isFollowedByThisUser:::$isFollowedByThisUser');// To find if need to set following flag true
      if (user_followers.contains(',${JJ.user_id},')) {
        isFollowedByThisUser = true;
      } else {
        isFollowedByThisUser = false;
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    String _selectedValue;
    ;
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
              color: const Color(0xfef7edff),
              borderRadius: BorderRadius.circular(8.0),
              border: Border.all(color: Colors.white54),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset("images/info.png", width: 20, alignment: Alignment.topRight),
                    const Text(" مشخصات فروشنده ",
                        style: TextStyle(
                          color: Colors.lightBlueAccent,
                        )),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 30,
                  margin: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        "images/textfieldBackGroundGold.png",
                      ),
                    ),
                  ),
                  child: Row(
                    children: [
                      const Expanded(
                        flex: 2,
                        child: Text(
                          textAlign: TextAlign.center,
                          "نام کاربر",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          textAlign: TextAlign.center,
                          user_username.isEmpty ? '$user_name $user_family' : user_username,
                          style: const TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 30,
                  margin: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        "images/textfieldBackGroundGold.png",
                      ),
                    ),
                  ),
                  child: Row(
                    children: [
                      const Expanded(
                        flex: 2,
                        child: Text(
                          textAlign: TextAlign.center,
                          "اعتبار فروشنده",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          user_credit,
                          textAlign: TextAlign.center,
                          style: const TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 30,
                  margin: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        "images/textfieldBackGroundGold.png",
                      ),
                    ),
                  ),
                  child: Row(
                    children: [
                      const Expanded(
                        flex: 2,
                        child: Text(
                          textAlign: TextAlign.center,
                          "تاریخ عضویت",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          textAlign: TextAlign.center,
                          user_createDate,
                          style: const TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 30,
                  margin: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        "images/textfieldBackGroundGold.png",
                      ),
                    ),
                  ),
                  child: Row(
                    children: [
                      const Expanded(
                        flex: 2,
                        child: Text(
                          textAlign: TextAlign.center,
                          "خریداران قبلی",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          countSales,
                          textAlign: TextAlign.center,
                          style: const TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        PageTransition(
                            type: PageTransitionType.leftToRight,
                            child: Products(
                              categoryId: '',
                              categoryTitle: user_username.isEmpty ? '$user_name $user_family' : user_username,
                              product_creator: creatorId,
                            )));
                  },
                  child: Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textFielsBg.png",
                        ),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          textAlign: TextAlign.center,
                          "مشاهده گالری فروشنده",
                          style: TextStyle(color: Colors.black87, fontSize: 14),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    showDialog(
                        context: context,
                        useSafeArea: true,
                        builder: (_) => AlertDialog(
                              insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                              contentPadding: const EdgeInsets.all(0.0),
                              content: SellerPhoneNubmer(sellerId: widget.sellerId),
                            ));
                  },
                  child: Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textFielsBg.png",
                        ),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          textAlign: TextAlign.center,
                          "سوال از فروشنده",
                          style: TextStyle(color: Colors.black87, fontSize: 14),
                        ),
                        Icon(
                          Icons.call,
                          color: Colors.black87,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 30,
                  margin: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        "images/textFielsBg.png",
                      ),
                    ),
                  ),
                  child: InkWell(
                    onTap: () {
                      // Add this user id to followers items of seller in server
                      toggleFollow(creatorId);
                      setState(() {});
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          textAlign: TextAlign.center,
                          "دنبال کردن و مطلع شدن از مزایدات فروشنده",
                          style: TextStyle(color: Colors.black87, fontSize: 14),
                        ),
                        Icon(
                          isFollowedByThisUser ? Icons.favorite_outlined : Icons.favorite_outline_outlined,
                          color: Colors.red,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ));
  }
}
