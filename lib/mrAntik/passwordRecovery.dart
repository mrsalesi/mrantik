import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

import 'tools/jjTools.dart';

class PasswordRecovery extends StatefulWidget {
  PasswordRecovery({Key? key}) : super(key: key);

  @override
  State<PasswordRecovery> createState() => _PasswordRecoveryState();

  String errorsForForm = "";
  String _user_codeMeli = "";
}

class _PasswordRecoveryState extends State<PasswordRecovery> {
  @override
  //This function send request to server
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<void> validateAndSave() async {
    final FormState? form = _formKey.currentState;
    if (form!.validate()) {
      debugPrint('Form is valid');
      String params = "do=Access_User.passwordRecovery";
      params += "&user_codeMeli=${widget._user_codeMeli}";
      widget.errorsForForm = 'در حال ارسال اطلاعات ...';
      setState(() {});
      await JJ().jjAjax(params).then((result) async {
        debugPrint("======$result");
        Map<String, dynamic> json = jsonDecode(result);
        widget.errorsForForm = json['comment'];
        JJ.jjToast(json['comment']);
        if (json['status'] == "1") {
          //on error message from server
          Navigator.of(context, rootNavigator: true).pop();
          // setState(() {});
        }
        setState(() {}); // for refresh changes
      });
    } else {
      debugPrint('Form is invalid');
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
          child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            //-----------------------Login Form
            Container(
              decoration: BoxDecoration(
                color: const Color(0xfef7edff),
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(color: Colors.white54),
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset("images/passRecovery.png", scale: 7.5),
                      const SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 30,
                    margin:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "کد ملی/مویابل: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: const InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(bottom: 20),
                              errorStyle: TextStyle(
                                height: 0,
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                widget.errorsForForm =
                                    'وارد کردن کد ملی یا شماره همراه الزامی است' +
                                        '\n';
                                return '';
                              } else if (!value.isValidIranianNationalCode() &&
                                  !value.isValidIranianMobileNumber()) {
                                widget.errorsForForm =
                                    'کد ملی یا شماره موبایل صحیح نیست' + '\n';
                                return '';
                              }
                              widget._user_codeMeli = value;
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 30),
                  InkWell(
                    onTap: validateAndSave,
                    child: Container(
                      alignment: Alignment.center,
                      width: 130,
                      padding: const EdgeInsets.all(8),
                      margin: const EdgeInsets.all(10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.black38,
                      ),
                      child: const Text(
                        "ارسال رمز",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Text(
                    widget.errorsForForm,
                    style: const TextStyle(
                      fontSize: 12,
                      height: 2,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      )),
    );
  }
}
