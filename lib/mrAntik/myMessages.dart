import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/AuctionRow.dart';

import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/navBarBottom.dart';
import 'widgets/navBarTop.dart';

class MyMessages extends StatefulWidget {
  const MyMessages({Key? key}) : super(key: key);

  @override
  State<MyMessages> createState() => _MyMessagesState();
}

class _MyMessagesState extends State<MyMessages> {
  late List<dynamic> items = [];
  var messenger_titleController = TextEditingController();
  var messenger_textMessageController = TextEditingController();
  String errorsForForm = '';

  @override
  void initState() {
    super.initState();
    getMoreItem(0);
  }

  sendMessageToSupport() async {
    ///Get Remaining time for refresh time in last 30 seconds of auction
    if (messenger_textMessageController.text.length < 3 || messenger_titleController.text.length < 3) {
      errorsForForm = 'لطفا عنوان و متن درخواست را کامل وارد کنید';
      setState(() {});
      return;
    }
    debugPrint('Messenger.sendMessageTosupport()... get last Bids');
    String result = await JJ().jjAjax('do=Messenger.sendMessageToSupport'
        '&messenger_title=${messenger_titleController.text}'
        '&messenger_textMessage=${messenger_textMessageController.text}');
    Map<String, dynamic> json = jsonDecode(result);
    if (json.isEmpty) {
      return;
    } else if (json['status'].toString() == "1") {
      Navigator.pop(context);
    }
    JJ.jjToast(json['comment']);
    setState(() {});
  }

  getMoreItem(int lastIndex) async {
    debugPrint('MyMessages.getMoreItem()');
    String result = await JJ().jjAjax('do=Messenger.getMyMessages&id=${JJ.localStorage!.getString('likedProductsList') ?? "0"}&lastIndex=$lastIndex');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('پایان لیست پیام های شما');
      return;
    }
    items.addAll(temp); //Add new items to available items
    setState(() {});
  }

  final TextEditingController user_messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  "ارسال متن به پشتیبانی",
                  style: TextStyle(color: Colors.blue, fontSize: 10, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: messenger_titleController,
                  keyboardType: TextInputType.text,
                  textAlign: TextAlign.right,
                  textAlignVertical: TextAlignVertical.center,
                  decoration: InputDecoration(
                      isDense: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                        ),
                      ),
                      filled: true,
                      fillColor: Colors.black12,
                      hintText: "عنوان سوال یا مشکل",
                      hintStyle: TextStyle(fontSize: 12)),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: messenger_textMessageController,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  minLines: 5,
                  textAlign: TextAlign.right,
                  textAlignVertical: TextAlignVertical.center,
                  decoration: InputDecoration(
                      isDense: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                        ),
                      ),
                      filled: true,
                      fillColor: Colors.black12,
                      hintText: "سوال یا مشکل خود را واضح و مشخص از بخش پشتیبانی وارد کنید",
                      hintStyle: TextStyle(fontSize: 12)),
                ),
                Text(errorsForForm,
                    style: const TextStyle(
                      fontSize: 12,
                      height: 2,
                      color: Colors.red,
                    )),
                InkWell(
                    onTap: () {
                      sendMessageToSupport();
                    },
                    child: Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.all(20),
                      padding: const EdgeInsets.all(8),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Color(0xffe8ac4f),
                      ),
                      child: Text(" ارسال به بخش پشتیبانی"),
                    )),
                Divider(),
                // 1st Section =========================================================
                //########################################
                for (int i = 0; i < items.length; i++)
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.all(8.0),
                      padding: const EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.black12,
                      ),
                      child: Text('گیرنده : ' + items[i]['rn'].toString() + ' ' + items[i]['rf'].toString() + '\n' + items[i]['messenger_title'].toString() + '\n\n' + items[i]['messenger_textMessage'].toString() + "\n")),
                //########################################
                const SizedBox(
                  height: 10,
                ),

                InkWell(
                  onTap: () {
                    getMoreItem(items.length);
                  },
                  child: const Center(
                    child: Icon(
                      Icons.add_circle_outlined,
                      color: Colors.black12,
                      size: 55,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        //Footer================================================
        bottomNavigationBar: const NavBarBottom());
  }
}
