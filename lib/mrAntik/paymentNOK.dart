
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'home.dart';
import 'rules.dart';

class PaymentOK extends StatelessWidget {
  const PaymentOK({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Scaffold(
          appBar: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.black,
              centerTitle: true,
              title: const Header()
          ),
            // 1st Section =========================================================
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                    ),
                    Center(
                      child: Container(
                        width: 250,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          color: Colors.deepOrange,
                        ),
                        child: Text("پرداخت ناموفق",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        border: Border.all(color: Colors.black54)
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: 10,),
                          Image.asset("images/paymentNOK.png",scale: 5,),
                          SizedBox(height: 10,),
                          Text("Order #3542154 ",style: TextStyle(color: Colors.green,fontFamily: 'Tahoma'),),
                          SizedBox(height: 10,),
                          Text.rich(
                            textAlign: TextAlign.justify,
                            TextSpan(
                                style: TextStyle(height: 1.5),
                                children: [
                              TextSpan(
                                text: "خریدارعزیز؛ بعداز دریافت کالا موظفید در صورت رضایت طی 48 ساعـت"
                                    " نسبـت به تاییـد و",
                              ),
                              TextSpan(
                                text:
                                    " آزادسـازی وجـه ",
                                style: const TextStyle(
                                    color: Color(0xffe8ac4f)),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    //show rules and conditions
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => AlertDialog(
                                          insetPadding: const EdgeInsets.symmetric(
                                              horizontal: 20, vertical: 40),
                                          shape: const RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.all(Radius.circular(5.0))),
                                          contentPadding: const EdgeInsets.all(0.0),
                                          content: Container(
                                            child: const Rules(),
                                          ),
                                        ));
                                    // Single tapped.
                                  },
                              ),
                              const TextSpan(
                                  text: "اقدام نمایید، درغیـر "
                                      "اینصورت عدم پاسخ شما به منزله‌ی تاییـد و رضایـت شمـا تلقـی "
                                      "می‌گردد و مستـرآنـتیـک نسبت به آزادسـازی وجـه به فروشنده "
                                      "اقدام می‌کند"),
                            ]),
                          ),

                        ],
                      )
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              // Navigator.push(context, MaterialPageRoute(builder: (context)=>const Payment('[{}]')));
                            },
                            child: Container(
                              height: 30,
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 10,right: 10),
                              margin:  EdgeInsets.only(left: 10,right: 10),
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage(
                                    "images/btnBgBack.png",
                                  ),
                                ),
                              ),
                              child: Text("مشاهده ی فاکتور"),
                            ),
                          ),
                        ),
                        Expanded(
                          child:InkWell(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>const HomePage()));
                            },
                            child: Container(
                              height: 30,
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 10,right: 10),
                              margin:  EdgeInsets.only(left: 10,right: 10),
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage(
                                    "images/btnBgForward.png",
                                  ),
                                ),
                              ),
                              child: Text("بازگشت به خانه"),
                            ),
                          ),
                        ),
                      ],
                    )

                  ],
                ),
              ),
            ),

        )
    );
  }
}
