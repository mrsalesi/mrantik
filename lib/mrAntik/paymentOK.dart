import 'dart:convert';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'home.dart';
import 'paymentAndFactor.dart';
import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/allertDialog.dart';

class PaymentOK extends StatefulWidget {
  final Map<String, dynamic> paymentJson;
  const PaymentOK({
    Key? key,
    required this.paymentJson,
  }) : super(key: key);

  @override
  State<PaymentOK> createState() => _PaymentOKState();
}

class _PaymentOKState extends State<PaymentOK> {
  @override
  initState() {
    super.initState();
    sendOKPaymentToServer(widget.paymentJson['id']);
  }

  sendOKPaymentToServer(String id) async {
    debugPrint('sendOKPaymentToServer() ...');
    String result =
        await JJ().jjAjax('do=Payment.sendOKPaymentToServer&id=$id');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('این محصول در جال حاضر وجود ندارد');
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Scaffold(
          appBar: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.black,
              centerTitle: true,
              title: const Header()),
          // 1st Section =========================================================
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  Center(
                    child: Container(
                      width: 250,
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        color: Colors.green,
                      ),
                      child: const Text(
                        "پرداخت شما با موفقیت انجام شد",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          border: Border.all(color: Colors.black54)),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          Image.asset(
                            "images/paymentOK.png",
                            scale: 5,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Order #${widget.paymentJson['id']} ",
                            style: const TextStyle(
                                color: Colors.green, fontFamily: 'Tahoma'),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text.rich(
                            textAlign: TextAlign.justify,
                            TextSpan(style: const TextStyle(height: 1.5), children: [
                              const TextSpan(
                                text:
                                    "خریدارعزیز؛ بعداز دریافت کالا موظفید در صورت رضایت طی 48 ساعـت"
                                    " نسبـت به تاییـد و",
                              ),
                              TextSpan(
                                text: " آزادسـازی وجـه ",
                                style:
                                    const TextStyle(color: Color(0xffe8ac4f)),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    //show rules and conditions
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => const jjAlertDialog(
                                              type: '',
                                              richText: Text(
                                                  'خریدار گرامی ضمن عرض تبریک جهت کالای خریداری شده  وبا تشکر از اعتماد شما به مجموعه وتمام به اطلاع میرساند جنابعالی موظفید که پس از تحویل جنس و بررسی محصول با مشخصات ارایه شده توسط فروشنده در صورت رضایت ظرف 48 ساعت نسبت به تایید، جهت آزادسازی وجه واریزی به فروشنده توسط وتمام اقدام کنید در صورت عدم تاییدیه شما به مدت 48 ساعت وتمام عدم پاسخ گویی شمارا به منزله رضایت شما تلقی میکند و سیستم شخصا نسبت به آزادسازی وجه و واریز آن به حساب فروشنده اقدام میکند.     '),
                                              title: 'آزادسازی وجه:',
                                            ));
                                    // Single tapped.
                                  },
                              ),
                              const TextSpan(
                                  text: "اقدام نمایید، درغیـر "
                                      "اینصورت عدم پاسخ شما به منزله‌ی تاییـد و رضایـت شمـا تلقـی "
                                      "می‌گردد و مستـرآنـتیـک نسبت به آزادسـازی وجـه به فروشنده "
                                      "اقدام می‌کند"),
                            ]),
                          ),
                        ],
                      )),
                  Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () async {

                            String params = "do=Factor.getPaymentAndFactor";
                            params +=
                                "&id=${widget.paymentJson['payment_factor_id']}";
                            String result = await JJ().jjAjax(params);
                            List<dynamic> jsonTemp = jsonDecode(result);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PaymentAndFactor(
                              paymentAndFacotrJSON : jsonTemp[0]
                                          )));
                          },//
                          child: Container(
                            height: 30,
                            alignment: Alignment.center,
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            margin: const EdgeInsets.only(left: 10, right: 10),
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                  "images/btnBgBack.png",
                                ),
                              ),
                            ),
                            child: const Text("مشاهده ی فاکتور"),
                          ),
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const HomePage()));
                          },
                          child: Container(
                            height: 30,
                            alignment: Alignment.center,
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            margin: const EdgeInsets.only(left: 10, right: 10),
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                  "images/btnBgForward.png",
                                ),
                              ),
                            ),
                            child: const Text("بازگشت به خانه"),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
