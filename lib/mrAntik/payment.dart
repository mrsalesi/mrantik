import 'dart:convert';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:url_launcher/url_launcher.dart';
import 'rules.dart';
import 'paymentOK.dart';
import 'tools/jjTools.dart';

class Payment extends StatefulWidget {
  final Map<String, dynamic> productJSON;
  final Map<String, dynamic> productDetailJSON;

  const Payment({
    Key? key,
    required this.productDetailJSON,
    required this.productJSON,
  }) : super(key: key);

  @override
  State<Payment> createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  final GlobalKey<FormState> _formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKey3 = GlobalKey<FormState>();
  String errorsForForm = "";
  String postBillAttach = "";
  String postArrivalTime = "";
  final TextEditingController addressController = TextEditingController();
  final TextEditingController postalCodeController = TextEditingController();

  bool userInSessionIsSeller = false; // if seller user open this page switch seller and customer header
  int saleStep = -1; //pre invoice

  bool checkedValue = false;
  int iKnowProductIsAvailable = 1;
  @override
  void initState() {
    debugPrint('Payment.initState()....');
    debugPrint('widget.productJSON${widget.productJSON}');
    addressController.text = JJ.user_address;
    postalCodeController.text = JJ.user_postalCode;
    // اگر در مرحله ی اولیه نبود این موارد نباید ست بشود
    super.initState();
    //check user in session is seller or customer
    if (widget.productJSON['product_creator'] == JJ.user_id) {
      //That means user in session is seller for this factor
      userInSessionIsSeller = true;
    }
  }

  Future<void> insertFactorProductApp() async {
    if (addressController.text.length < 10) {
      errorsForForm = ' آدرس دقیق پستی را وارد کنید';
      setState(() {});
      return;
    }
    if (postalCodeController.text.length < 10) {
      errorsForForm = ' کد پستی را  10 رقمی را بصورت دقیق وارد کنید';
      setState(() {});
      return;
    }
    String params = "do=Factor.insertFactorProductApp";
    params += "&product_factor_productId=${widget.productDetailJSON['id']}";
    params += "&product_factor_postalCode=${postalCodeController.text}";
    params += "&product_factor_address=${addressController.text}";
    params += "&product_factor_creator=${JJ.user_id}";
    String result = await JJ().jjAjax(params);
    Map<String, dynamic> jsonTemp = jsonDecode(result);
    if (jsonTemp['status'] == "0") {
      errorsForForm = jsonTemp['comment'];
      JJ.jjToast(jsonTemp['comment']);
      setState(() {}); // for refresh changes
    } else {
      //If price is successfully added bill and payment, now go to payment page
      saleStep = 1;

      final Uri url = Uri.parse('${jsonTemp['url']}');
      if (!await launchUrl(url, mode: LaunchMode.externalApplication)) {
        JJ.jjToast('در حال هدایت به صفحه ی پرداخت');
        throw Exception('Could not launch $url');
      }
      // Navigator.push(
      //     context,
      //     MaterialPageRoute(
      //         builder: (context) => PaymentOK(
      //               paymentJson: jsonTemp[0],
      //             )));
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    String price = "100";
    if (widget.productDetailJSON['product_isAuction'].toString() == "1") {
      price = widget.productDetailJSON['product_auctionImmediateSalePrice']; //For auction if user has been came whit immediate price in this page
    } else {
      price = widget.productDetailJSON['product_price1'];
    }
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Scaffold(
            appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
            // 1st Section Status of sale=========================================================
            body: SingleChildScrollView(
                child: Column(
              children: [
                Container(
                  height: 50,
                  padding: const EdgeInsets.all(10),
                  color: Colors.black87,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "images/saleStepPre1.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        "images/saleStepPre2.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        "images/saleStepPre3.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        "images/saleStepPre4.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        "images/saleStepPre5.png",
                        scale: 7,
                      ),
                      Image.asset(
                        "images/saleArrow.png",
                        scale: 6.2,
                      ),
                      Image.asset(
                        "images/saleStepPre6.png",
                        scale: 7,
                      ),
                    ],
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Column(children: [
                      const SizedBox(
                        height: 10,
                      ),
                      const Center(
                        child: Column(
                          children: [
                            Text(
                              "پیش فاکتور",
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),

                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.only(left: 2, top: 4, bottom: 4),
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(Radius.circular(8)),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                            ),
                            child: Column(
                              children: [
                                Align(
                                    alignment: Alignment.topRight,
                                    child: Image.asset(
                                      userInSessionIsSeller ? 'images/LabelSeller.png' : 'images/LabelCostomer.png',
                                      scale: 7,
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.person_rounded,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        ' ${JJ.user_name} ${JJ.user_family} ',
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.credit_card,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        " ${JJ.user_codeMeli} ",
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8, bottom: 10),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.location_on,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        " ${JJ.user_city} ",
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )),
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.only(right: 2, top: 4, bottom: 4),
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(Radius.circular(8)),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                            ),
                            child: Column(
                              children: [
                                Align(
                                    alignment: Alignment.topRight,
                                    child: Image.asset(
                                      userInSessionIsSeller ? "images/LabelCostomer.png" : "images/LabelSeller.png",
                                      scale: 7,
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.person_rounded,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        widget.productJSON['user_username'].toString().isEmpty ? '${widget.productJSON['user_name']} ${widget.productJSON['user_family']}' : widget.productJSON['user_username'].toString(),
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(top: 8, right: 8),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.credit_card,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        '**********',
                                        style: TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8, right: 8, bottom: 10),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.location_on,
                                        color: Colors.black54,
                                        size: 17,
                                      ),
                                      Text(
                                        widget.productJSON['user_city']??"-",
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )),
                        ],
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          border: Border.all(
                            color: Colors.black26,
                          ),
                        ),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 2,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                textAlign: TextAlign.right,
                                                ' ${widget.productDetailJSON['product_name']}',
                                                style: const TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Align(
                                                child: Text(
                                                  textAlign: TextAlign.right,
                                                  " کد:${widget.productDetailJSON['id']} ",
                                                  style: const TextStyle(
                                                    fontSize: 12,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Text(
                                        textDirection: TextDirection.rtl,
                                        softWrap: true,
                                        textAlign: TextAlign.right,
                                        "مشخصات کالا:",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        widget.productDetailJSON['products_content'],
                                        softWrap: true,
                                        textAlign: TextAlign.justify,
                                        style: const TextStyle(
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                            Expanded(
                                flex: 1,
                                child: Container(
                                    padding: const EdgeInsets.all(4),
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                    ),
                                    child: Image.network(
                                      '${JJ.server}upload/${widget.productDetailJSON['product_pic1']}',
                                      fit: BoxFit.fitWidth,
                                    ))),
                          ],
                        ),
                      ),
                      //Price container========================================
                      Container(
                        margin: const EdgeInsets.only(top: 5, bottom: 5),
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          border: Border.all(
                            color: Colors.black26,
                          ),
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 15, left: 5, right: 5),
                                    child: Image.asset("images/saleStep1.png", alignment: Alignment.centerLeft),
                                  ),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    height: 30,
                                    padding: const EdgeInsets.all(0),
                                    margin: const EdgeInsets.only(top: 20, left: 10, bottom: 2),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black54),
                                      borderRadius: const BorderRadius.all(Radius.circular(4)),
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 4,
                                          child: Container(
                                            height: 30,
                                            alignment: Alignment.center,
                                            child: const Text(
                                              " قیمت کالا:",
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                            flex: 4,
                                            child: Container(
                                              padding: const EdgeInsets.only(right: 10),
                                              alignment: Alignment.centerRight,
                                              height: 30,
                                              color: const Color(0xfff8e9d6),
                                              child: Text(
                                                textDirection: TextDirection.ltr,
                                                price.toString().seRagham(),
                                                style: const TextStyle(fontSize: 15),
                                              ),
                                            )),
                                        Expanded(
                                            flex: 1,
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(4), topLeft: Radius.circular(4)),
                                                color: Color(0xfff8e9d6),
                                              ),
                                              alignment: Alignment.center,
                                              height: 30,
                                              child: const Text(
                                                "(تومان)",
                                                style: TextStyle(fontSize: 10),
                                              ),
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                const Expanded(
                                  flex: 1,
                                  child: Text(""),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    height: 30,
                                    padding: const EdgeInsets.all(0),
                                    margin: const EdgeInsets.only(top: 20, left: 10, bottom: 20),
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(4)),
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 4,
                                          child: Container(
                                            decoration: const BoxDecoration(
                                              color: Color(0xfff8e9d6),
                                              borderRadius: BorderRadius.only(topRight: Radius.circular(4), bottomRight: Radius.circular(4)),
                                            ),
                                            height: 30,
                                            alignment: Alignment.center,
                                            child: const Text(
                                              " قابل پرداخت:",
                                              style: TextStyle(fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                            flex: 4,
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: AssetImage(
                                                    "images/textFielsBg.png",
                                                  ),
                                                ),
                                              ),
                                              padding: const EdgeInsets.only(right: 10),
                                              alignment: Alignment.centerRight,
                                              height: 30,
                                              child: Text(
                                                textDirection: TextDirection.ltr,
                                                (price.toString().seRagham()).toString(),
                                                style: const TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                            )),
                                        Expanded(
                                            flex: 1,
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: AssetImage(
                                                    "images/textFielsBg.png",
                                                  ),
                                                ),
                                                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(4), topLeft: Radius.circular(4)),
                                                color: Color(0xfff8e9d6),
                                              ),
                                              alignment: Alignment.center,
                                              height: 30,
                                              child: const Text(
                                                "(تومان)",
                                                style: TextStyle(fontSize: 10),
                                              ),
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      //Customer Address=======================================
                      Container(
                        margin: const EdgeInsets.only(top: 5, bottom: 5),
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          border: Border.all(
                            color: Colors.black26,
                          ),
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 15, left: 5, right: 5),
                                    child: Image.asset("images/saleStep4.png", alignment: Alignment.centerLeft),
                                  ),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    height: 30,
                                    padding: const EdgeInsets.all(0),
                                    margin: const EdgeInsets.only(top: 20, left: 10, bottom: 2),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 9,
                                          child: Container(
                                            height: 30,
                                            alignment: Alignment.centerRight,
                                            child: Image.asset(
                                              "images/LabelCustomerAddress.png",
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                const Expanded(
                                  flex: 1,
                                  child: Text(""),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: TextFormField(
                                      controller: addressController,
                                      maxLength: 500,
                                      enabled: !userInSessionIsSeller,
                                      keyboardType: TextInputType.multiline,
                                      maxLines: null,
                                      minLines: 5,
                                      decoration: const InputDecoration(
                                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                        border: OutlineInputBorder(),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                        ),
                                        hintText: " نشانی دقیق محل دریافت کالا ",
                                        hintStyle: TextStyle(color: Colors.black38, fontSize: 12),
                                        fillColor: Colors.black12,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                const Expanded(
                                  flex: 1,
                                  child: Text(""),
                                ),
                                const Expanded(
                                  flex: 2,
                                  child: Text(" کد پستی: "),
                                ),
                                Expanded(
                                  flex: 6,
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: TextFormField(
                                      enabled: !userInSessionIsSeller,
                                      controller: postalCodeController,
                                      textAlign: TextAlign.center,
                                      keyboardType: TextInputType.number,
                                      decoration: const InputDecoration(
                                        isDense: true,
                                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                        border: OutlineInputBorder(),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                        ),
                                        hintText: "کد پستی 10 رقمی",
                                        hintStyle: TextStyle(color: Colors.black38, fontSize: 12),
                                        fillColor: Colors.black12,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const Padding(
                              padding: EdgeInsets.only(left: 20, right: 20),
                              child: Divider(
                                color: Colors.black38,
                              ),
                            ),
                            Container(
                              alignment: Alignment.topRight,
                              padding: const EdgeInsets.only(top: 10, right: 20),
                              child: RichText(
                                text: TextSpan(children: [
                                  const TextSpan(
                                    text: "جزئیات ارسال:",
                                    style: TextStyle(color: Color(0xffe8ac4f)),
                                  ),
                                  TextSpan(text: " توسط فروشنده تا ${widget.productJSON['user_settingSendTime']} روز کاری"),
                                ]),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topRight,
                              padding: const EdgeInsets.only(top: 10, right: 20),
                              child: RichText(
                                text: TextSpan(children: [
                                  const TextSpan(
                                    text: "هزینه ارسال:",
                                    style: TextStyle(color: Color(0xffe8ac4f)),
                                  ),
                                  TextSpan(text: widget.productDetailJSON['product_postCostBySeller'] == '1' ? 'هزینه ی ارسال با فروشنده است' : "پس کرایه ( هزینه ارسال با خریدار)"),
                                ]),
                                textAlign: TextAlign.right,
                              ),
                            ),
                            Container(
                              alignment: Alignment.topRight,
                              padding: const EdgeInsets.all(20),
                              child: RichText(
                                text: TextSpan(style: const TextStyle(color: Colors.black, fontFamily: "B_Yekan", height: 1.7), children: [
                                  const TextSpan(
                                    text: "از طریق فرآیند",
                                  ),
                                  TextSpan(
                                    text: " ضمانت برگشت پول توسط وتمام ",
                                    style: const TextStyle(color: Color(0xffe8ac4f)),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        //show rules and conditions
                                        showDialog(
                                            context: context,
                                            useSafeArea: true,
                                            builder: (_) => const AlertDialog(insetPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 40), shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))), contentPadding: EdgeInsets.all(0.0), content: Rules()));
                                        // Single tapped.
                                      },
                                  ),
                                  const TextSpan(text: "کالای خریداری شده طبق مشخصات درج شده تحویل شما میگردد، در غیر اینصورت و عدم موافقت شما تمامی مبالغ واریز شما به حسابتان مرجوع میگردد"),
                                ]),
                              ),
                            ),
                            SizedBox(
                              height: 30,
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Checkbox(
                                      activeColor: Colors.green ,
                                      value: checkedValue,
                                      onChanged: (newValue) {
                                        setState(() {
                                          checkedValue = newValue!;
                                          if (checkedValue) {
                                            debugPrint("checkedValue....<>>>");
                                            iKnowProductIsAvailable = 1;
                                          } else {
                                            debugPrint("checkedValue....<>>>");
                                            iKnowProductIsAvailable = 0;
                                          }
                                        });
                                      },
                                    ),
                                  ),
                                  const Expanded(
                                    flex: 5,
                                    child: Text(style: TextStyle(fontSize: 14,color: Colors.green), " طی هماهنگی با فروشنده از موجود بودن کالا مطلع هستم "),
                                  )
                                ],
                              ),
                            ),
                            if (!userInSessionIsSeller) // seller user can not pay her/himself product
                              if (saleStep < 1)
                                Visibility(
                                  visible: checkedValue,
                                  child: InkWell(
                                    onTap: () async {
                                      insertFactorProductApp();
                                    },
                                    child: Container(
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.all(20),
                                      width: 220,
                                      padding: const EdgeInsets.all(8),
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: AssetImage(
                                            "images/textFielsBg.png",
                                          ),
                                        ),
                                      ),
                                      child: const Text(
                                        "پرداخت",
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.black87,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                          ],
                        ),
                      ),
                    ])),
                Text(
                  errorsForForm,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 12,
                    height: 2,
                    color: Colors.red,
                  ),
                ),
                const SizedBox(height: 75),
              ],
            ))));
  }
}
