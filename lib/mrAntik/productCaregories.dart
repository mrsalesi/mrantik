import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'package:vaTamam/mrAntik/widgets/navBarBottom.dart';
import 'package:page_transition/page_transition.dart';

import 'products.dart';
import 'productSubCaregories.dart';
import 'addNewProduct.dart';
import 'tools/jjTools.dart';

class ProductCategories extends StatelessWidget {
  final List<dynamic>? categoriesJSON;

  const ProductCategories({Key? key, required this.categoriesJSON}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('----------------------Categories----------------------------');
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Scaffold(
            appBar: AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Colors.black,
                centerTitle: true,
                title: const Header()),
            // 1st Section =========================================================
            body: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "دسته بندی را انتخاب کنید",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  for (int i = 0; i < (categoriesJSON?.length ?? 0); i++)
                    Column(
                      children: [
                        SizedBox(
                          height: 35,
                          child: InkWell(
                            onTap: () async {
                              await JJ()
                                  .jjAjax(
                                      'do=ProductCategory.getMenu&productCategory_parent=${categoriesJSON![i]['id']}')
                                  .then((result) async {
                                List<dynamic> temp = jsonDecode(result);
                                if (temp.isEmpty) {

                                  //If there was no sub categories go to add product page
                                  List<dynamic> featuresJsonArray ;
                                  await JJ()
                                      .jjAjax(
                                      'do=ProductCategoryFeature.getSelectOptionTags&CategoryFeature_categoryId=${categoriesJSON![i]['id']}')
                                      .then((result2) async {
                                    featuresJsonArray = jsonDecode(result2);
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type:
                                            PageTransitionType.leftToRight,
                                            child: Products(
                                              categoryId: categoriesJSON![i]
                                              ['id'],
                                              categoryTitle:
                                              "${categoriesJSON![i]['productCategory_title']}",
                                            )));
                                  });
                                } else {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          child: ProductSubCaregories(
                                            categoriesJSON: temp,
                                            parenCategoriyTitle:
                                                categoriesJSON![i]
                                                    ['productCategory_title'],
                                            parenCategoriyPic:
                                                categoriesJSON![i]
                                                    ['productCategory_pic'],
                                          )));
                                }
                              }); // AddNewProductByUser(id:widget.categoriesJSON![i]['id'])));
                            },
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: Image.network(
                                      JJ.server +
                                          'upload/' +
                                          (categoriesJSON![i]
                                              ['productCategory_pic']),
                                      scale: 3,
                                    )),
                                Expanded(
                                    flex: 7,
                                    child: Text(categoriesJSON![i]
                                        ['productCategory_title'])),
                                Expanded(
                                    flex: 1,
                                    child: Image.asset(
                                      "images/leftArrow.png",
                                      scale: 9,
                                    )),
                              ],
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.black38,
                          thickness: 1,
                        ),
                      ],
                    ),
                ],
              ),
            ),
            // 11th Section Footer================================================
            bottomNavigationBar: const NavBarBottom()));
  }
}
