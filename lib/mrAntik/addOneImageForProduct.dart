import 'dart:io';
import "dart:async";

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vaTamam/mrAntik/addNewProduct.dart';
import 'package:photo_view/photo_view.dart';
import 'tools/jjTools.dart';
import 'widgets/Header.dart';

class addOneImageForProduct extends StatefulWidget {
  final int picNo;

  const addOneImageForProduct({Key? key, required this.picNo}) : super(key: key);

  @override
  State<addOneImageForProduct> createState() => _addOneImageForProductState();
}

enum SingingCharacter { black, white } // This enum use for bg_remover option and need an other _character variable that is defined in below

class _addOneImageForProductState extends State<addOneImageForProduct> {
  XFile? selectedImageFileObj;
  final ImagePicker picker = ImagePicker();

  static Future<String> changeImageBgColor(String imageName, String bgColor) async {
    String params = "do=Pic.changeImageBgColor";
    params += '&imageName=$imageName';
    params += '&background=$bgColor';
    String result = await JJ().jjAjax(params);
    debugPrint('result ::::$result');
    return result.isEmpty ? imageName : result;
  }

  Future<String> getImageAndUpload(ImageSource media) async {
    if (!kIsWeb) {
      // If platform is not Android or iOS so it needs different way to upload
      selectedImageFileObj = await picker.pickImage(source: media,maxHeight: 1200,maxWidth: 1200);
      return await JJ.fileUpload(File(selectedImageFileObj!.path));
    } else {
      //In web file upload is like below
      XFile? selectedImageFileObj;
      final ImagePicker picker = ImagePicker();
      selectedImageFileObj = await picker.pickImage(source: media);
      if (selectedImageFileObj != null) {
        var f = await selectedImageFileObj.readAsBytes();
        Uint8List webImage = f;
        return await JJ.fileUploadWeb(webImage);
      } else {
        debugPrint("No file selected");
        return "";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    bool bgrProcessCheckBoxVal = AddNewProductByUser.picArray[widget.picNo - 1].contains("_bgr");
    bool waitForBGRProcess = false;
    String errorText = "";
    SingingCharacter? bgColor = AddNewProductByUser.picArray[widget.picNo - 1].contains("black") ? SingingCharacter.black : SingingCharacter.white;
    return Scaffold(
      appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
      body: SingleChildScrollView(
        child: StatefulBuilder(
          builder: (context, setState) {
            return Center(
              child: SizedBox(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.white,
                  child: Column(
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("images/bdDialogHeader.jpg"),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: const Icon(
                            Icons.circle_notifications_rounded,
                            size: 40,
                            color: Colors.black45,
                          )),
                      const Divider(
                        color: Colors.black26,
                        thickness: 2,
                        height: 0,
                      ),
                      const Text(
                        "حذف پس زمینه عکس توسط هوش مصنوعی:",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, height: 2),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 50,
                        height: MediaQuery.of(context).size.width - 50,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (_) {
                                return GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    }, // back on click
                                    child: PhotoView(
                                      minScale: 0.2,
                                      imageProvider: NetworkImage(
                                        "${JJ.server}upload/${AddNewProductByUser.picArray[widget.picNo - 1]}", // -1 => Index start from 0
                                      ),
                                    ));
                              },
                            ));
                          },
                          child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[widget.picNo - 1]}"),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: ListTile(
                              horizontalTitleGap: 0,
                              title: const Text('زمینه روشن'),
                              leading: Radio<SingingCharacter>(
                                value: SingingCharacter.white,
                                groupValue: bgColor,
                                onChanged: (SingingCharacter? value) {
                                  bgColor = value;
                                  if (bgrProcessCheckBoxVal) {
                                    AddNewProductByUser.picArray[widget.picNo - 1] = AddNewProductByUser.picArray[widget.picNo - 1].replaceAll("_bgr_white", "").replaceAll("_bgr_black", "");
                                    AddNewProductByUser.picArray[widget.picNo - 1] = AddNewProductByUser.picArray[widget.picNo - 1].replaceAll(".", "_bgr_white.");
                                  }
                                  setState(() {});
                                },
                              ),
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: ListTile(
                                horizontalTitleGap: 0,
                                title: const Text('زمینه تیره'),
                                leading: Radio<SingingCharacter>(
                                  value: SingingCharacter.black,
                                  groupValue: bgColor,
                                  onChanged: (SingingCharacter? value) {
                                    bgColor = value;
                                    if (bgrProcessCheckBoxVal) {
                                      AddNewProductByUser.picArray[widget.picNo - 1] = AddNewProductByUser.picArray[widget.picNo - 1].replaceAll("_bgr_white", "").replaceAll("_bgr_black", "");
                                      AddNewProductByUser.picArray[widget.picNo - 1] = AddNewProductByUser.picArray[widget.picNo - 1].replaceAll(".", "_bgr_black.");
                                    }
                                    setState(() {});
                                  },
                                ),
                              )),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          children: [
                            Expanded(
                              child: !waitForBGRProcess
                                  ? // This condition add to hide checkBox until process finished
                                  Checkbox(
                                      activeColor: const Color(0xffe8ac4f),
                                      value: AddNewProductByUser.picArray[widget.picNo - 1].contains("_bgr"),
                                      onChanged: (newValue) async {
                                        bgrProcessCheckBoxVal = newValue!;
                                        if (bgrProcessCheckBoxVal) {
                                          errorText = "لطفا کمی صبر کنید...";
                                          waitForBGRProcess = true;
                                          String background = bgColor == SingingCharacter.black ? "black" : "white";
                                          AddNewProductByUser.picArray[widget.picNo - 1] = AddNewProductByUser.picArray[widget.picNo - 1].replaceAll("_bgr_white", "").replaceAll("_bgr_black", ""); //Clear last efect
                                          setState(
                                            () {},
                                          );
                                          String result = await changeImageBgColor(AddNewProductByUser.picArray[widget.picNo - 1].replaceAll("_bgr_white", '').replaceAll("_bgr_black", ""), background);
                                          AddNewProductByUser.picArray[widget.picNo - 1] = AddNewProductByUser.picArray[widget.picNo - 1].replaceAll(".", "_bgr_$background.");
                                          setState(
                                            () {},
                                          );
                                        } else {
                                          AddNewProductByUser.picArray[widget.picNo - 1] = AddNewProductByUser.picArray[widget.picNo - 1].replaceAll("_bgr_white", "").replaceAll("_bgr_black", "");
                                        }
                                        errorText = "";
                                        waitForBGRProcess = false;
                                        setState(
                                          () {},
                                        );
                                      },
                                    )
                                  : const SizedBox(),
                            ),
                            const Expanded(
                              flex: 7,
                              child: Text(style: TextStyle(fontSize: 12), "می توانید با هوش مصنوعی زمینه ی تصویر بالا را حذف کنید"),
                            )
                          ],
                        ),
                      ),
                      Text(errorText, style: const TextStyle(color: Colors.red, fontSize: 12), textAlign: TextAlign.center),
                      const SizedBox(
                        height: 30,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          margin: const EdgeInsets.all(10),
                          padding: const EdgeInsets.all(10),
                          decoration: const BoxDecoration(color: Color(0xffe8ac4f), borderRadius: BorderRadius.all(Radius.circular(3))),
                          child: const Text("ثبت و ادامه"),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
