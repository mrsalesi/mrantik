import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/navBarTop.dart';
import 'package:vaTamam/mrAntik/widgets/productRow.dart';
import 'package:share_plus/share_plus.dart';

import 'login.dart';
import 'register.dart';
import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/navBarBottom.dart';

class Products extends StatefulWidget {
  final String categoryId;
  final String categoryTitle;
  final String? product_creator;
  final String? query;
  final String? productId;//When user comes from web site, we pass him/her to search

  const Products({
    Key? key,
    required this.categoryId,
    required this.categoryTitle,
    this.product_creator,
    this.query,
    this.productId,
  }) : super(key: key);

  @override
  State<Products> createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  bool showOrderBy = false;
  bool showFilterBy = false;
  String orderBy = "";
  String filterBy = "";
  late List<dynamic> items = [];

  @override
  void initState() {
    super.initState();
    getMoreItem(0);
  }

  getMoreItem(int lastIndex) async {
    debugPrint('getMoreItem()');
    String productCreatorCondition = widget.product_creator == null ? '' : '&product_creator=${widget.product_creator}';
    String result = await JJ().jjAjax('do=Product.getTopProducts'
        '&product_categoryId=${widget.categoryId}'
        '&orderBy=$orderBy'
        '&filterBy=$filterBy'
        '&query=${widget.query ?? ''}'
        '&productId=${widget.productId ?? ''}'
        '&lastIndex=$lastIndex'
        '$productCreatorCondition');
    List<dynamic> temp = jsonDecode(result);
    if (lastIndex == 0) {
      items.clear();
    }
    if (temp.isEmpty) {
      JJ.jjToast('محصول مورد نظر شما بارگذاری نشده است');
      return;
    }
    items.addAll(temp); //Add new items to available items
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const NavBarTop(),
                // 2nd Section =========================================================
                const SizedBox(
                  height: 20,
                ),
                if (JJ.user_token != "")// If user is  login show InviteFriends btn
                  InkWell(
                    onTap: (){
                      Share.share(
                          'سلام؛ من کاربر اپلیکیشن وتمام شدم، پیشنهاد میکنم  https://www.vatamam.com/');
                    },
                    child: Container(
                      margin: const EdgeInsets.only(bottom: 15),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        border: Border.all(),
                        color: Color(0xffe8ac4f),
                        // image: DecorationImage(
                        //   fit: BoxFit.cover,
                        //   image: AssetImage(
                        //     "images/textFielsBg.png",
                        //   ),
                        // )
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('دعوت از دوستان   ',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                          // Image.asset("images/myMenu_InviteFriends.png", scale: 5,),
                          Icon(Icons.share),
                        ],
                      ),
                    ),
                  ),
                if (JJ.user_token == "")// If user is not login show Login&Register btn
                  Row(
                    children: [
                      Expanded(
                          child: InkWell(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    useSafeArea: true,
                                    builder: (_) => AlertDialog(
                                          insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                          contentPadding: const EdgeInsets.all(0.0),
                                          content: Register(),
                                        ));
                              },
                              child: Image.asset("images/register.png"))),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: InkWell(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    useSafeArea: true,
                                    builder: (_) => AlertDialog(
                                          insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                          contentPadding: const EdgeInsets.all(0.0),
                                          content: Login(),
                                        ));
                              },
                              child: Image.asset("images/login.png"))),
                    ],
                  )
                else
                  Container(
                    height: 40,
                    margin: const EdgeInsets.all(0),
                    padding: const EdgeInsets.only(right: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textFielsBg.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 7,
                          child: Text(
                            '${widget.categoryTitle}',
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                // 3th Section =========================================================
                const SizedBox(
                  height: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.asset(
                    "images/temp/galleries.png",
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // 5th Section =========================================================
                Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          showOrderBy = false;//to hide right side tab the orderBy
                          if (showFilterBy == true) {
                            showFilterBy = false;
                          } else {
                            showFilterBy = true;
                          } //Toggle the variable 'showFilterBy'
                          debugPrint("showFilterBy:$showFilterBy");
                          setState(() {});
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.filter_alt,
                              color: Colors.black26,
                              size: 30.0,
                            ),
                            Text('فیلتر'),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                        child: InkWell(
                      onTap: () {
                          showFilterBy = false;
                        if (showOrderBy == true) {
                          showOrderBy = false;
                        } else {
                          showOrderBy = true;
                        } //Toggle the variable 'showOrderBy'
                        debugPrint("showOrderBy:$showOrderBy");
                        setState(() {});
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Icon(
                            Icons.sort,
                            color: Colors.black26,
                            size: 30.0,
                          ),
                          Text('مرتب سازی'),
                        ],
                      ),
                    )),
                    const SizedBox(
                      width: 15,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      //----------------------FilterBy column ----------------------
                      child: Visibility(
                          maintainSize: false,
                          maintainAnimation: true,
                          maintainState: true,
                          visible: showFilterBy,
                          child: Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  filterBy = "product_date DESC";
                                  getMoreItem(0); //to get product again that is sorted as user selection
                                  showFilterBy = false; // To hide filterBy column
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  alignment: Alignment.center,
                                  decoration: const BoxDecoration(
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                        "images/textFielsBg.png",
                                      ),
                                    ),
                                  ),
                                  child: DropdownSearch<String>(
                                    popupProps: const PopupProps.menu(
                                      showSelectedItems: true,
                                      showSearchBox: true,
                                    ),
                                    items: JJ.listOfCites,
                                    dropdownDecoratorProps: const DropDownDecoratorProps(
                                      textAlign: TextAlign.center,
                                      dropdownSearchDecoration: InputDecoration(
                                        fillColor: Color(0xdde8ac4f),
                                        focusedBorder: InputBorder.none,
                                        floatingLabelAlignment: FloatingLabelAlignment.center,
                                        border: InputBorder.none,
                                        isDense: true,
                                        hintText: "فیلتر شهر",
                                      ),
                                    ),
                                    onChanged: (value) {
                                      debugPrint(">>>>>" + value.toString());
                                      filterBy = " AND user_city='$value'";
                                      getMoreItem(0); //to get product again that is sorted as user selection
                                      showOrderBy = false; // To hide orderBy column
                                      showFilterBy = false; //To hide filterBy column
                                    },
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage(
                                      "images/textFielsBg.png",
                                    ),
                                  ),
                                ),
                                child: DropdownSearch<String>(
                                  popupProps: const PopupProps.menu(
                                    showSelectedItems: true,
                                    showSearchBox: false,
                                  ),
                                  items: [
                                    'در حد نو',
                                    'مستعمل',
                                    'مرمت شده',
                                    'لوکس جدید',
                                    'معیوب',
                                  ],
                                  dropdownDecoratorProps: const DropDownDecoratorProps(
                                    textAlign: TextAlign.center,
                                    dropdownSearchDecoration: InputDecoration(
                                      fillColor: Color(0xdde8ac4f),
                                      focusedBorder: InputBorder.none,
                                      floatingLabelAlignment: FloatingLabelAlignment.center,
                                      border: InputBorder.none,
                                      isDense: true,
                                      hintText: "وضعیت کالا",
                                    ),
                                  ),
                                  onChanged: (value) {
                                    filterBy = " AND product_val0='$value'";
                                    getMoreItem(0); //to get product again that is sorted as user selection
                                    showOrderBy = false; // To hide orderBy column
                                    showFilterBy = false; //To hide filterBy column
                                  },
                                  selectedItem: 'در حد نو',
                                ),
                              ),
                            ],
                          )),
                    ),
                    Expanded(
                      //----------------------OrderBy column ----------------------
                      child: Visibility(
                          maintainSize: false,
                          maintainAnimation: true,
                          maintainState: true,
                          visible: showOrderBy,
                          child: Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  orderBy = "product_date DESC";
                                  getMoreItem(0); //to get product again that is sorted as user selection
                                  showOrderBy = false; // To hide orderBy column
                                  showFilterBy = false;
                                },
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.all(10),
                                    alignment: Alignment.center,
                                    decoration: const BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage(
                                          "images/textFielsBg.png",
                                        ),
                                      ),
                                    ),
                                    child: Text("جدید ترین آگهی")),
                              ),
                              InkWell(
                                onTap: () {
                                  orderBy = "product_date ";
                                  getMoreItem(0); //to get product again that is sorted as user selection
                                  showOrderBy = false; // To hide orderBy column
                                  showFilterBy = false;
                                },
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.all(10),
                                    alignment: Alignment.center,
                                    decoration: const BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage(
                                          "images/textFielsBg.png",
                                        ),
                                      ),
                                    ),
                                    child: Text("قدیمی ترین آگهی")),
                              ),
                              InkWell(
                                onTap: () {
                                  orderBy = "product_price1 DESC";
                                  getMoreItem(0); //to get product again that is sorted as user selection
                                  showOrderBy = false; // To hide orderBy column
                                  showFilterBy = false;
                                },
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.all(10),
                                    alignment: Alignment.center,
                                    decoration: const BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage(
                                          "images/textFielsBg.png",
                                        ),
                                      ),
                                    ),
                                    child: Text("بیش ترین قیمت")),
                              ),
                              InkWell(
                                onTap: () {
                                  orderBy = "product_price1 ";
                                  getMoreItem(0); //to get product again that is sorted as user selection
                                  showOrderBy = false; // To hide orderBy column
                                  showFilterBy = false;
                                },
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.all(10),
                                    alignment: Alignment.center,
                                    decoration: const BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage(
                                          "images/textFielsBg.png",
                                        ),
                                      ),
                                    ),
                                    child: Text("کم ترین قیمت")),
                              ),
                            ],
                          )),
                    ),
                  ],
                ),
                // 6th Section. All top products in for loop=========================================================
                //########################################
                for (int i = 0; i < items.length; i++) ProductRow(productJSON: (items[i])),
                //########################################
                const SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () {
                    getMoreItem(items.length);
                  },
                  child: const Center(
                    child: Icon(
                      Icons.add_circle_outlined,
                      color: Colors.black12,
                      size: 55,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        //Footer================================================
        bottomNavigationBar: const NavBarBottom());
  }
}
