import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';
import 'rules.dart';
import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/allertDialog.dart';

class Setting extends StatefulWidget {
  Setting({Key? key}) : super(key: key);

  @override
  State<Setting> createState() => _SettingState();


}

class _SettingState extends State<Setting> {
  String errorsForForm = "";
  String _user_username = "";
  String _user_name = "";
  String _user_family = "";
  String _user_codeMeli = "";
  String _user_mobile = "";
  String _user_city = "";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  //This function send request to server
  Future<void> validateAndSave() async {
    final FormState? form = _formKey.currentState;
    if (form!.validate()) {
      debugPrint('Form is valid');
      String params = "do=Access_User.userSettingUpdate";
      params+= "&user_settingSendTime=${_user_username}" ;

      errorsForForm = 'در حال ارسال اطلاعات ...';
      setState(() {});

      await JJ().jjAjax(params).then((result) async {
        debugPrint("======$result");
        Map<String, dynamic> json = jsonDecode(result);
        SharedPreferences? localStorage;
        localStorage = await SharedPreferences.getInstance();
        if (json['status'] == "0") {
          errorsForForm = json['comment'];
          setState(() {});// for refresh changes
          JJ.user_token = "";
          localStorage.setString("user_token", "");
        }else{
          errorsForForm = json['comment'];
          JJ.jjToast(json['comment']);
          setState(() {});
        }
      });
    } else {
      setState(() {});
      debugPrint('Form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.black,
          centerTitle: true,
          title: const Header()),
      // 1st Section =========================================================
      body: SizedBox(
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              decoration: BoxDecoration(
                color: const Color(0xfef7edff),
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(color: Colors.white54),
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 100,
                  ),
                  Stack(
                    children: [
                      Container(
                        height: 30,
                        margin: const EdgeInsets.only(
                            left: 20, right: 20, bottom: 10),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                              "images/textfieldBackGroundGold.png",
                            ),
                          ),
                        ),
                        child: Row(
                          children: [
                            const Expanded(
                              flex: 2,
                              child: Text(
                                textAlign: TextAlign.center,
                                "تنظیمات زمان ارسال: ",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  contentPadding: EdgeInsets.only(bottom: 20),
                                  errorStyle: TextStyle(
                                    height: 0,
                                  ),
                                ),
                                validator: (value) {
                                  errorsForForm ='';
                                  if (value == null ||
                                      value!.isEmpty ||
                                      !value!.isNumeric()) {
                                    errorsForForm +=
                                        'فقط عدد وارد کنید' +
                                            '\n';
                                    return '';
                                  }
                                 _user_username = value!;
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  Text(
                    errorsForForm,
                    style: const TextStyle(
                      fontSize: 12,
                      height: 2,
                      color: Colors.red,
                    ),
                  ),
                  const SizedBox(height: 15),
                  InkWell(
                    onTap: validateAndSave,
                    child: Container(
                      alignment: Alignment.center,
                      width: 120,
                      padding: const EdgeInsets.all(8),
                      margin: const EdgeInsets.all(8),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.black38,
                      ),
                      child: const Text(
                        "ثبت تنظیمات من",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
