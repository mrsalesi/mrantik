import 'dart:convert';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/auctionsOfOneGallery.dart';

import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/auctionCategory.dart';
import 'widgets/sellerAndLastAuctionRow.dart';
import 'widgets/navBarBottom.dart';
import 'widgets/navBarTop.dart';
import 'registerVIP.dart';

class AuctionsBySeller extends StatefulWidget {
  final String categoryId;
  final String categoryTitle;
  final String? product_creator;

  const AuctionsBySeller({Key? key, required this.categoryId, required this.categoryTitle, this.product_creator}) : super(key: key);

  @override
  State<AuctionsBySeller> createState() => _AuctionsBySellerState();
}

class _AuctionsBySellerState extends State<AuctionsBySeller> {
  bool showOrderBy = false;
  bool showFilterBy = false;
  String orderBy = "";
  String filterBy = "";
  late List<dynamic> items = [];

  @override
  void initState() {
    super.initState();
    getMoreItem(0);
  }

  getMoreItem(int lastIndex) async {
    debugPrint('AuctionsBySeller.getMoreItem()');
    String productCreatorCondition = widget.product_creator == null ? '' : '&product_creator=${widget.product_creator}';
    String result = await JJ().jjAjax('do=Product.getTopAuctionsBySeller'
        '&product_categoryId=${widget.categoryId}'
        '&orderBy=$orderBy'
        '&filterBy=$filterBy'
        '&lastIndex=$lastIndex$productCreatorCondition');
    if (lastIndex == 0) {
      items.clear();
    }
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('آیتم دیگری وجود ندارد');
      return;
    }
    items.addAll(temp); //Add new items to available items
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const NavBarTop(),
                // VIP Btn =============================================================================
                if (JJ.user_grade != "VIP") InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const RegisterVIP()));
                      },
                      child: Image.asset(
                        "images/AuctionHeader.png",
                      ),
                    ) else InkWell(
                      onTap: () async {
                        debugPrint('AuctionsBySeller.getUserInSessionAuctions()');
                        String result = await JJ().jjAjax('do=Product.getTopAuctionsBySeller'
                            '&product_creator=${JJ.user_id}');
                        List<dynamic> temp = jsonDecode(result);
                        if (temp.isNotEmpty) {
                          Map<String,dynamic> jsonObj = temp[0];//
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>  AuctionsOfOneGallery(sellerAndLastAuctionJSON: jsonObj)));
                        }else{
                          JJ.jjToast("شما کالای برای مزایده نگذاشته اید");
                        }
                      },
                      child: Image.asset(
                        "images/AuctionHeaderForVIPs.png",
                      ),
                    ),
                // 2th Section =========================================================
                const SizedBox(
                  height: 10,
                ),
                const AuctionCategory(categoryId: 0),
                const SizedBox(
                  height: 20,
                ),
                // 3th Section =========================================================
                for (int i = 0; i < items.length; i++) SellerAndLastAuctionRow(productJSON: items[i]),
                const SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () {
                    getMoreItem(items.length);
                  },
                  child: const Center(
                    child: Icon(
                      Icons.add_circle_outlined,
                      color: Colors.black12,
                      size: 55,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        //Footer================================================
        bottomNavigationBar: const NavBarBottom());
  }
}
