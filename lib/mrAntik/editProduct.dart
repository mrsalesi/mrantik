import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:persian_datetimepickers/persian_datetimepickers.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'addNewProduct.dart';
import 'commisionsTable.dart';
import 'registerVIP.dart';
import 'editProductImages.dart';
import 'tools/jjTools.dart';
import 'tools/mrAnticCalculator.dart';
import 'widgets/Header.dart';
import 'widgets/allertDialog.dart';
import 'widgets/navBarBottom.dart';

class EditProductByUser extends StatefulWidget {



  final List<dynamic> productDetailJSON;

  const EditProductByUser({super.key, required this.productDetailJSON});

  @override
  State<EditProductByUser> createState() => _EditProductByUser();
}

class _EditProductByUser extends State<EditProductByUser> {
  var product_price1Controller = TextEditingController();
  var product_auctionInitPriceController = TextEditingController();
  var product_auctionImmediateSalePriceController = TextEditingController();
  var product_auctionMinPriceController = TextEditingController();
  String product_price1 = '';
  String product_name = '';
  String product_dimension1 = '';
  String product_dimension2 = '';
  String product_dimension3 = '';
  String product_quantity = '';
  String products_content = '';
  List<String> product_prop = [];
  List<String> product_val = [];
  DateTime product_auctionStartDate = DateTime.now();
  DateTime product_auctionEndDate = DateTime.now().add(Duration(days: 3));
  String product_auctionInitPrice = '';
  String product_auctionImmediateSalePrice = '';
  String product_auctionMinPrice = '';
  int product_postCostBySeller = 1;
  List<dynamic> featuresJsonArray = [];

  String label = '';
  String errorsForForm = '';
  bool product_isAuction = false;
  bool checkedValue = true;
  Future<SharedPreferences> localStorage = SharedPreferences.getInstance();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  initState() {
    AddNewProductByUser.picArray[0] = widget.productDetailJSON[0]['product_pic1'];
    AddNewProductByUser.picArray[1] = widget.productDetailJSON[0]['product_pic2'];
    AddNewProductByUser.picArray[2] = widget.productDetailJSON[0]['product_pic3'];
    AddNewProductByUser.picArray[3] = widget.productDetailJSON[0]['product_pic4'];
    AddNewProductByUser.picArray[4] = widget.productDetailJSON[0]['product_pic5'];
    AddNewProductByUser.picArray[5] = widget.productDetailJSON[0]['product_pic6'];
    AddNewProductByUser.picArray[6] = widget.productDetailJSON[0]['product_pic7'];
    AddNewProductByUser.picArray[7] = widget.productDetailJSON[0]['product_pic8'];
    AddNewProductByUser.picArray[8] = widget.productDetailJSON[0]['product_pic9'];
    product_isAuction = widget.productDetailJSON[0]['product_isAuction'] == '1' ? true : false;
    product_dimension1 = widget.productDetailJSON[0]['product_dimension'].toString().split("*")[0];
    product_dimension2 = widget.productDetailJSON[0]['product_dimension'].toString().split("*")[1];
    product_dimension3 = widget.productDetailJSON[0]['product_dimension'].toString().split("*")[2];
    product_price1Controller.text = widget.productDetailJSON[0]['product_price1'] ;
    JJ()
        .jjAjax('do=ProductCategoryFeature.getSelectOptionTags'
            '&CategoryFeature_categoryId=${widget.productDetailJSON[0]['product_categoryId']}')
        .then((result2) {
      featuresJsonArray = jsonDecode(result2);
      setState(() {});
    });

    super.initState();
  }

  ///This function send request to server
  Future<void> validateAndSave() async {
    errorsForForm = '';
    final FormState? form = _formKey.currentState;
    bool otherErrors = AddNewProductByUser.picArray[0].isNotEmpty; //If pic 1 is uploaded
    if (!otherErrors) {
      errorsForForm += 'حداقل یک تصویر برای کالا باید ثبت کنید' + "\n";
    }
    if (form!.validate() && otherErrors) {
      debugPrint('Form is valid');
      String params = "do=Product.editProductByUser";
      params += '&id=${widget.productDetailJSON[0]['id']}';
      params += '&user_token=${JJ.user_token}';
      params += '&product_name=${product_name}';
      params += '&product_dimension=${product_dimension1}*${product_dimension2}*${product_dimension3}';
      params += '&product_quantity=${product_quantity}';
      params += '&product_price1=${product_price1Controller.text.replaceAll(RegExp("\\D"), "")}';
      params += '&products_content=${products_content}';

      params += '&product_pic1=${AddNewProductByUser.picArray[0]}';
      params += '&product_pic2=${AddNewProductByUser.picArray[1]}';
      params += '&product_pic3=${AddNewProductByUser.picArray[2]}';
      params += '&product_pic4=${AddNewProductByUser.picArray[3]}';
      params += '&product_pic5=${AddNewProductByUser.picArray[4]}';
      params += '&product_pic6=${AddNewProductByUser.picArray[5]}';
      params += '&product_pic7=${AddNewProductByUser.picArray[6]}';
      params += '&product_pic8=${AddNewProductByUser.picArray[7]}';
      params += '&product_pic9=${AddNewProductByUser.picArray[8]}';
      params += '&product_postCostBySeller=${product_postCostBySeller}';
      // Serialize all prop and values
      for (int i = 0; i < product_prop.length; i++) {
        params += '&product_prop$i=${product_prop[i]}';
        params += '&product_val$i=${product_val[i]}';
      }
      //data for auction
      params += '&product_isAuction=${product_isAuction}';
      params += '&product_auctionInitPrice=${product_auctionInitPrice}';
      params += '&product_auctionImmediateSalePrice=${product_auctionImmediateSalePrice}';
      params += '&product_auctionMinPrice=${product_auctionMinPrice}';
      params += '&product_auctionStartDate=${product_auctionStartDate.millisecondsSinceEpoch}';
      params += '&product_auctionEndDate=${product_auctionEndDate.millisecondsSinceEpoch}';
      errorsForForm = 'در حال ارسال اطلاعات ...';
      setState(() {});
      String result = await JJ().jjAjax(params);
      Map<String, dynamic> json = jsonDecode(result);
      SharedPreferences? localStorage;
      localStorage = await SharedPreferences.getInstance();
      if (json['status'] == "0") {
        errorsForForm = json['comment'];
        setState(() {}); // for refresh changes
        JJ.user_token = "";
        localStorage.setString("user_token", "");
      } else {
        Navigator.of(context, rootNavigator: true).pop();
        Navigator.of(context, rootNavigator: true).pop();//go tow step back
        errorsForForm = json['comment'];
        JJ.jjToast(json['comment']);
        setState(() {});
      }
    } else {
      setState(() {});
      debugPrint('Form is invalid');
    }
  }  ///This function send request to server
  Future<void> deleteProductByOwnerUser() async {
    errorsForForm = '';
    final FormState? form = _formKey.currentState;
    bool otherErrors = AddNewProductByUser.picArray[0].isNotEmpty; //If pic 1 is uploaded
    if (!otherErrors) {
      errorsForForm += 'حداقل یک تصویر برای کالا باید ثبت کنید' + "\n";
    }
    if (form!.validate() && otherErrors) {
      debugPrint('Form is valid');
      String params = "do=Product.deleteProductByOwnerUser";
      params += '&id=${widget.productDetailJSON[0]['id']}';
      params += '&user_token=${JJ.user_token}';;
      errorsForForm = 'در حال ارسال اطلاعات ...';
      setState(() {});
      String result = await JJ().jjAjax(params);
      Map<String, dynamic> json = jsonDecode(result);
      SharedPreferences? localStorage;
      localStorage = await SharedPreferences.getInstance();
      if (json['status'] == "0") {
        errorsForForm = json['comment'];
        setState(() {}); // for refresh changes
        JJ.user_token = "";
        localStorage.setString("user_token", "");
      } else {
        Navigator.of(context, rootNavigator: true).pop();
        Navigator.of(context, rootNavigator: true).pop();//go tow step back
        errorsForForm = json['comment'];
        JJ.jjToast(json['comment']);
        setState(() {});
      }
    } else {
      setState(() {});
      debugPrint('Form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Form(
          key: _formKey,
          child: Scaffold(
              appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
              // 1st Section =========================================================
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 15,
                    ),
                    const Center(
                        child: Text(
                      "فروش کالا",
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    )),
                    if (widget.productDetailJSON[0]['product_status'].toString() == 'فروش رفته' )
                    Container(
                      height: 45,
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.only(right: 10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textFielsBg.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 7,
                            child: Text.rich(
                              TextSpan(children: [
                                 TextSpan(text: "محصول فروش رفته قابل ویرایش نیست"),
                              ]),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Divider(
                      color: Colors.black26,
                      thickness: 2,
                    ),
                    const Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.only(right: 20.0),
                          child: Text(
                            "تصاویر کالا",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        )),
                    Row(
                      children: [
                        Expanded(
                            child: InkWell(
                          onTap: () {
                            debugPrint(widget.productDetailJSON[0]['product_status'].toString() + ":"+ widget.productDetailJSON[0]['product_isAuction'].toString());
                            if (widget.productDetailJSON[0]['product_status'].toString() != 'فروش رفته' && widget.productDetailJSON[0]['product_isAuction'].toString()=="0") {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => const EditProductImages()));
                            } else {}
                          },
                          child: Container(
                            margin: const EdgeInsets.all(5),
                            padding: const EdgeInsets.all(2),
                            decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                            child: AddNewProductByUser.picArray[0].isEmpty //If image is not set
                                ? //If file not uploaded yet
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    padding: const EdgeInsets.all(20),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.asset("images/addimage.png", scale: 1.5, height: 70))
                                : Container(
                                    margin: const EdgeInsets.all(2),
                                    padding: const EdgeInsets.all(0),
                                    decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), shape: BoxShape.rectangle, color: Colors.black38),
                                    child: Image.network("${JJ.server}upload/${AddNewProductByUser.picArray[0]}", scale: 1.5, height: 110)),
                          ),
                        )),
                        Expanded(
                            child: Container(
                          padding: const EdgeInsets.all(10),
                          height: 150,
                          decoration: const BoxDecoration(
                              border: Border(
                            right: BorderSide(width: 3),
                          )),
                          child: Text.rich(
                            style: TextStyle(
                              height: 1,
                            ),
                            TextSpan(children: [
                              const TextSpan(text: "\n"),
                              const TextSpan(text: "(حد اکثر 9 عکس)", style: TextStyle(fontSize: 10)),
                              const TextSpan(text: "\n"),
                              const TextSpan(text: "\n"),
                              const TextSpan(text: "لطفا عکس از زوایای مختلف محصول را ثبت کنید"),
                              const TextSpan(text: "\n"),
                              const TextSpan(text: "\n"),
                              TextSpan(
                                text: "استاندارد و قوانین سایت برای عکس",
                                style: const TextStyle(color: Color(0xffe8ac4f)),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    //show rules and conditions
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => const jjAlertDialog(
                                          type: '',
                                          richText: Text(
                                              'از آنجایی که زاویه ، وضوح و شفافیت و رنگ عکس کالا جهت نمایش و جذابیت هرچه بیشتر  در سایت بسیار مهم می باشد و میتواند به فروش سریعتر شما کمک کند و همچنین اختلافات بعدی  را به هنگام تحویل  کالا کاهش دهد، روش گرفتن عکس درست از کالاهایتان را همراه با تصویر نمونه از عکس درست و نادرست، بیان نموده ایم.'),
                                          title: 'استاندارد و قوانین سایت برای عکس:',
                                        ));
                                    // Single tapped.
                                  },
                              ),
                            ]),
                            softWrap: true,
                            
                          ),
                        )),
                      ],
                    ),
                    const Divider(
                      color: Colors.black26,
                      thickness: 2,
                    ),
                    const Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.only(right: 20.0, bottom: 10),
                          child: Text(
                            "عنوان",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        )),
                    Container(
                      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      child: TextFormField(
                          initialValue: widget.productDetailJSON[0]['product_name'],
                          textAlignVertical: TextAlignVertical.center,
                          decoration: const InputDecoration(
                            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                            border: OutlineInputBorder(),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey, width: 1.0),
                            ),
                            hintText: 'عنوان کالا',
                            contentPadding: EdgeInsets.only(right: 10),
                          ),
                          validator: (value) {
                            errorsForForm += '';
                            if (value == null || value.isEmpty || value.length < 10) {
                              errorsForForm += 'عنوان کالا از 10 کاراکتر کوچکتر نباید باشد' + '\n';
                              return '';
                            }
                            product_name = value;
                          }),
                    ),
                    const Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.only(right: 20.0, bottom: 10),
                          child: Text(
                            "توضیحات تکمیلی",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        )),
                    Container(
                      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      child: TextFormField(
                        initialValue: widget.productDetailJSON[0]['products_content'],
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        minLines: 5,
                        decoration: const InputDecoration(
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                          border: OutlineInputBorder(),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey, width: 1.0),
                          ),
                          contentPadding: EdgeInsets.all(10),
                          errorStyle: TextStyle(
                            height: 0,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.length < 10) {
                            errorsForForm += 'در قسمت توضیحات کالا حداقل یک جمله با 20 حرف وارد کنید' + '\n';
                            return '';
                          }
                          products_content = value;
                        },
                      ),
                    ),
                    const Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.only(right: 40.0, top: 20, bottom: 10),
                          child: Text(
                            "مشخصات:",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        )),
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black12),
                        borderRadius: const BorderRadius.all(Radius.circular(8)),
                        image: const DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textFielsBg.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Container(
                              height: 30,
                              alignment: Alignment.center,
                              decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(8)),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage(
                                      "images/textFielsBg.png",
                                    ),
                                  )),
                              child: const Text.rich(
                                TextSpan(children: [
                                  TextSpan(
                                      text: ('*'),
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold,
                                      )),
                                  TextSpan(text: 'ابعاد'),
                                ]),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: TextFormField(
                              initialValue: product_dimension1,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                textDirection: TextDirection.ltr,
                                decoration: const InputDecoration(
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                  ),
                                  hintText: 'طول',
                                  contentPadding: EdgeInsets.only(right: 10),
                                ),
                                validator: (value) {
                                  errorsForForm += '';
                                  if (value == null || value.isEmpty || value.length ==0) {
                                    errorsForForm += 'ابعاد کالا حداقل طول * عرض را وارد کنید' + '\n';
                                    return '';
                                  }
                                  product_dimension1 = value;
                                }),
                          ),Expanded(
                            flex: 4,
                            child: TextFormField(
                              initialValue: product_dimension2,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                textDirection: TextDirection.ltr,
                                decoration: const InputDecoration(
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                  ),
                                  hintText: 'طول',
                                  contentPadding: EdgeInsets.only(right: 10),
                                ),
                                validator: (value) {
                                  errorsForForm += '';
                                  if (value == null || value.isEmpty || value.length ==0) {
                                    errorsForForm += 'ابعاد کالا حداقل طول * عرض را وارد کنید' + '\n';
                                    return '';
                                  }
                                  product_dimension2 = value;
                                }),
                          ),Expanded(
                            flex: 4,
                            child: TextFormField(
                              initialValue: product_dimension3,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                textDirection: TextDirection.ltr,
                                decoration: const InputDecoration(
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.grey, width: 1.0),
                                  ),
                                  hintText: 'ارتفاع',
                                  contentPadding: EdgeInsets.only(right: 10),
                                ),
                                validator: (value) {
                                  errorsForForm += '';
                                  if (value == null || value.isEmpty || value.length ==0) {
                                    product_dimension3='';
                                    return null;
                                  }
                                  product_dimension3 = value;
                                }),
                          ),
                        ],
                      ),
                    ),
                    for (int i = 0; i < (featuresJsonArray.length); i++)
                      Container(
                        height: 30,
                        margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12),
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          image: const DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                              "images/textFielsBg.png",
                            ),
                          ),
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Container(
                                height: 30,
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                        "images/textFielsBg.png",
                                      ),
                                    )),
                                child: Text.rich(
                                  TextSpan(children: [
                                    TextSpan(
                                        text: ('${featuresJsonArray[i]['CategoryFeature_isNecessary']}' == '1' ? '*' : ''),
                                        style: TextStyle(
                                          color: Colors.red,
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold,
                                        )),
                                    TextSpan(text: '${featuresJsonArray[i]['CategoryFeature_title']}'),
                                  ]),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: DropdownButtonFormField(
                                borderRadius: const BorderRadius.all(Radius.circular(8)),
                                hint: const SizedBox(
                                    width: 160,
                                    child: Text(
                                      'انتخاب کنید',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 12,
                                      ),
                                    )),
                                alignment: Alignment.center,
                                menuMaxHeight: 500,
                                isDense: true,
                                dropdownColor: const Color(0xffe8ac4f),
                                onChanged: (value) {

                                },
                                value: widget.productDetailJSON[0]['product_val${i}'].toString(),
                                validator: (value) {
                                  if ('${featuresJsonArray[i]['CategoryFeature_isNecessary']}' == '1') {
                                    if (value == null || value.isEmpty) {
                                      errorsForForm += '${featuresJsonArray[i]['CategoryFeature_title']}'
                                              ' را انتخاب کنید' +
                                          '\n';
                                      return '';
                                    }
                                  }
                                  product_prop.add(featuresJsonArray[i]['CategoryFeature_title']);
                                  product_val.add(value ?? '');
                                },
                                items: JJ.getListFromJson(jsonDecode(featuresJsonArray[i]['product_category_feature_options']), 'productCategoryFeatureOption_title').map((String val) {
                                  return DropdownMenuItem(
                                    alignment: Alignment.center,
                                    value: val,
                                    child: Text(
                                      style: const TextStyle(
                                        fontSize: 12,
                                      ),
                                      val,
                                    ),
                                  );
                                }).toList(),
                                decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  isDense: true,
                                  errorStyle: TextStyle(height: 0),
                                  contentPadding: EdgeInsets.all(0),
                                  floatingLabelAlignment: FloatingLabelAlignment.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    const SizedBox(
                      height: 40,
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      padding: const EdgeInsets.all(10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textFielsBg.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 3,
                            child: Text(
                              textAlign: TextAlign.center,
                              "نحوه ی فروش و قیمت",
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: DropdownButtonFormField(
                              value: widget.productDetailJSON[0]['product_isAuction'] == '1' ? 'فروش مزایده' : 'فروش عادی',
                              hint: const SizedBox(
                                  width: 100,
                                  child: Text(
                                    'انتخاب کنید',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 12,
                                    ),
                                  )),
                              alignment: Alignment.center,
                              menuMaxHeight: 500,
                              isDense: true,
                              dropdownColor: const Color(0xffe8ac4f),
                              onChanged: (value) {
                                debugPrint(">>>>>" + value.toString() + "  ${JJ.user_grade}");
                                setState(() {
                                  if (value.toString() == "فروش مزایده") {
                                    if (JJ.user_grade == "VIP") {
                                      product_isAuction = true;
                                    } else {
                                      showDialog(
                                          context: context,
                                          useSafeArea: true,
                                          builder: (_) => jjAlertDialog(
                                            type: '',
                                            richText: Text('دوست عزیز فضای مزایده یک فضای حرفه ای و ویژه ی اعضای VIP است. از شما دعوت میکنیم عضویت خود را تکمیل و کاربر ویژه ی ما شوید.'
                                              ,style: TextStyle(fontSize: 13,height: 3),
                                            ),
                                            title: 'قسمت مزایده ویژه ی کاربران VIP:',
                                            btnTitle: 'عضویت VIP',
                                            btnFunction: () {
                                              Navigator.push(context, MaterialPageRoute(builder: (context) => const RegisterVIP()));
                                            },
                                          ));
                                    }
                                  } else {
                                    product_isAuction = false;
                                  }
                                });},
                              items: [
                                "فروش عادی",
                                "فروش مزایده",
                              ].map((String val) {
                                return DropdownMenuItem(
                                  alignment: Alignment.center,
                                  value: val,
                                  child: Text(
                                    style: const TextStyle(
                                      fontSize: 12,
                                    ),
                                    val,
                                  ),
                                );
                              }).toList(),
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                isDense: true,
                                contentPadding: EdgeInsets.all(0),
                                floatingLabelAlignment: FloatingLabelAlignment.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //فروش عادی Ordinary Sale =======================
                    const Divider(
                      color: Colors.black38,
                      thickness: 2,
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Visibility(
                      maintainSize: false,
                      maintainAnimation: true,
                      maintainState: true,
                      visible: !product_isAuction,
                      child: Column(
                        children: [
                          const Center(
                              child: Text(
                                "فروش عادی",
                                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                              )),
                          const SizedBox(
                            height: 30,
                          ),
                          Container(
                            height: 30,
                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black12),
                              borderRadius: const BorderRadius.all(Radius.circular(8)),
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: Container(
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: AssetImage(
                                            "images/textFielsBg.png",
                                          ),
                                        )),
                                    child: const Text(
                                      textAlign: TextAlign.center,
                                      "قیمت کالا:",
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: TextFormField(
                                    controller: product_price1Controller,
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.center,
                                    textDirection: TextDirection.ltr,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: const InputDecoration(
                                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                      border: InputBorder.none,
                                      contentPadding: EdgeInsets.only(bottom: 20),
                                      errorStyle: TextStyle(
                                        height: 0,
                                      ),
                                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                                    ),
                                    onChanged: (String value) {
                                      // set 1000 separator on add value
                                      if (value.replaceAll('\D', '').isNotEmpty) {
                                        product_price1Controller.text = '${JJ.jjSeRagham(value.replaceAll(RegExp('\\D'), ''))} ';
                                        product_price1Controller.selection = TextSelection.collapsed(offset: (product_price1Controller.text.length - 1));
                                        setState(() {});
                                      } else {
                                        product_price1Controller.text = '';
                                        TextSelection.collapsed(offset: product_price1Controller.text.length);
                                      }
                                    },
                                    validator: (value) {
                                      if (!product_isAuction) {
                                        if (value == null || value.replaceAll('\D', '').isEmpty) {
                                          errorsForForm += 'قیمت کالا را مشخص کنید' + '\n';
                                          return '';
                                        } else if (JJ.jjToInt(value) < 200000) {
                                          errorsForForm += 'قیمت کالا کمتر از 200 هزار تومان نمی تواند باشد' + '\n';
                                          return '';
                                        }
                                        product_price1 = value..replaceAll(RegExp('\\D'), '');
                                      }
                                    },
                                  ),
                                ),
                                const Text(
                                  " (تومان)",
                                  style: TextStyle(fontSize: 10),
                                )
                              ],
                            ),
                          ),
                          Container(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                children: [
                                  const Align(
                                      alignment: FractionalOffset.topRight,
                                      child: Text(
                                        "کارمزد وتمام:",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          height: 3,
                                        ),
                                      )),
                                  Text.rich(
                                      style: const TextStyle(fontSize: 14, height: 2),
                                      TextSpan(children: [
                                        const TextSpan(text: "فروشنده ی گرامی، در صورتیکه خریدار درخواست نماید که معامله"),
                                        const TextSpan(text: " با نظارت وتمام ", style: TextStyle(fontWeight: FontWeight.bold)),
                                        const TextSpan(text: "انجام شود "),
                                        TextSpan(
                                            text: "مبلغ " // get data from product_price1 textField remove other fields and calculate commision
                                                "${JJ.jjSeRagham(MrAnticCalculator.commission(JJ.jjToInt(product_price1Controller.text)).toString())}"
                                                " تومان ",
                                            style: const TextStyle(fontWeight: FontWeight.bold)),
                                        TextSpan(
                                            text: "طبق جدول تعرفه ها ",
                                            style: const TextStyle(color: Color(0xffe8ac4f)),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                showDialog(
                                                    context: context,
                                                    useSafeArea: true,
                                                    builder: (_) => AlertDialog(
                                                      insetPadding: const EdgeInsets.symmetric( vertical: 40),
                                                      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                                      contentPadding: const EdgeInsets.all(0.0),
                                                      content: const CommissionsTable(),
                                                    ));
                                              }),
                                        const TextSpan(text: "از مبلغ اعلامی شما به عنوان کارمزد کسر میشود"),
                                      ])),
                                  const Align(
                                      alignment: FractionalOffset.topRight,
                                      child: Text(
                                        "زمان ارسال:",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          height: 3,
                                        ),
                                      )),
                                  const Text(
                                    "شما باید کالای فروش رفته را حداکثر تا 48 ساعت برای خریدار ارسال کنید.",
                                    softWrap: true,
                                  ),
                                  const Align(
                                      alignment: FractionalOffset.topRight,
                                      child: Text(
                                        "توضیحات:",
                                        style: TextStyle(
                                          height: 2,
                                        ),
                                      )),
                                  const Text(
                                    "جهت تغییر در زمان ارسال می توانید از قسمت گالری من>تنظیمات>تنظیمات زمان ارسال، زمان ارسال خود را تغییر دهید",
                                    softWrap: true,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // Container(
                          //   height: 30,
                          //   margin: const EdgeInsets.only(
                          //       left: 20, right: 20, bottom: 10),
                          //   child: Row(
                          //     children: [
                          //       Expanded(
                          //         flex: 1,
                          //         child: TextFormField(
                          //           initialValue: '1',
                          //           keyboardType: TextInputType.number,
                          //           maxLines: 1,
                          //           textAlign: TextAlign.center,
                          //           textAlignVertical: TextAlignVertical.center,
                          //           decoration: const InputDecoration(
                          //             focusedBorder: OutlineInputBorder(
                          //                 borderSide: BorderSide(
                          //                     color: Color(0xffe8ac4f))),
                          //             border: OutlineInputBorder(
                          //                 borderSide: BorderSide(
                          //                     color: Color(0xffe8ac4f))),
                          //             contentPadding:
                          //                 EdgeInsets.only(bottom: 20),
                          //             errorStyle: TextStyle(
                          //               height: 0,
                          //             ),
                          //           ),
                          //           validator: (value) {
                          //             String pattern = r'(^\d{1,2}$)';
                          //             RegExp regex = new RegExp(pattern);
                          //             if (value == null ||
                          //                 !regex.hasMatch(value.toString())) {
                          //               errorsForForm +=
                          //                   'تعداد واقعی وارد کنید';
                          //               return '';
                          //             } else {
                          //               product_quantity = value;
                          //               return null;
                          //             }
                          //           },
                          //         ),
                          //       ),
                          //       const Expanded(
                          //         flex: 4,
                          //         child: Text(
                          //           textAlign: TextAlign.right,
                          //           " تعداد ",
                          //         ),
                          //       ),
                          //     ],
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                    //فروش مزایده Auction Sale =======================
                    Visibility(
                      maintainSize: false,
                      maintainAnimation: true,
                      maintainState: true,
                      visible: product_isAuction,
                      child: Column(
                        children: [
                          const Center(
                              child: Text(
                                "فروش مزایده",
                                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                              )),
                          const SizedBox(
                            height: 30,
                          ),
                          Container(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                children: const [
                                  Align(
                                      alignment: FractionalOffset.topRight,
                                      child: Text(
                                        "زمان ارسال:",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          height: 3,
                                        ),
                                      )),
                                  Text(
                                    "شما میبایست کالای فروش رفته را حداکثر تا 48 ساعت برای خریدار ارسال کنید.",
                                    softWrap: true,
                                  ),
                                  Align(
                                      alignment: FractionalOffset.topRight,
                                      child: Text(
                                        "توضیحات:",
                                        style: TextStyle(
                                          height: 2,
                                        ),
                                      )),
                                  Text(
                                    "جهت تغییر در زمان ارسال می توانید از قسمت گالری من>تنظیمات>تنظیمات زمان ارسال، زمان ارسال خود را تغییر دهید",
                                    softWrap: true,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            height: 30,
                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black12),
                              borderRadius: const BorderRadius.all(Radius.circular(8)),
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: Container(
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: AssetImage(
                                            "images/textFielsBg.png",
                                          ),
                                        )),
                                    child: const Text(
                                      style: TextStyle(fontSize: 13),
                                      textAlign: TextAlign.right,
                                      "قیمت پایه:",
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: TextFormField(
                                    controller: product_auctionInitPriceController,
                                    keyboardType: TextInputType.number,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: const InputDecoration(
                                      suffixText: " تومان ",suffixStyle: TextStyle(fontSize: 12),
                                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                      border: InputBorder.none,
                                      contentPadding: EdgeInsets.only(bottom: 20),
                                      errorStyle: TextStyle(
                                        height: 0,
                                      ),
                                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                                    ),
                                    onChanged: (String value) {
                                      // set 1000 separator on add value
                                      if (value.replaceAll('\D', '').isNotEmpty) {
                                        product_auctionInitPriceController.text = '${JJ.jjSeRagham(value.replaceAll(RegExp('\\D'), ''))}';
                                        product_auctionInitPriceController.selection = TextSelection.collapsed(offset: product_auctionInitPriceController.text.length);
                                      } else {
                                        product_auctionInitPriceController.text = '';
                                        TextSelection.collapsed(offset: product_auctionInitPriceController.text.length);
                                      }
                                    },
                                    validator: (value) {
                                      if (product_isAuction) {
                                        if (value == null || value.replaceAll(RegExp('\\D'), '').isEmpty) {
                                          errorsForForm += 'قیمت پایه را مشخص کنید' + '\n';
                                          return '';
                                        } else if (int.parse(value.replaceAll(RegExp('\\D'), '')) < 200000) {
                                          errorsForForm += 'قیمت پایه کمتر از 200 هزار تومان نمی تواند باشد' + '\n';
                                          return '';
                                        }
                                        product_auctionInitPrice = value.replaceAll(RegExp('\\D'), '');
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const Text(
                            "قیمت پایه از 200,000 تومان نمی تواند کمتر باشد ",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                          const Divider(
                            color: Colors.black26,
                            thickness: 1,
                          ),
                          const Padding(
                            padding: EdgeInsets.all(20),
                            child: Align(
                                alignment: FractionalOffset.topRight,
                                child: Text(
                                  "تاریخ شروع مزایده",
                                  style: TextStyle(fontWeight: FontWeight.bold, height: 1),
                                )),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                  height: 30,
                                  margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black12),
                                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          height: 30,
                                          alignment: Alignment.center,
                                          decoration: const BoxDecoration(
                                              borderRadius: BorderRadius.all(Radius.circular(8)),
                                              image: DecorationImage(
                                                fit: BoxFit.cover,
                                                image: AssetImage(
                                                  "images/textFielsBg.png",
                                                ),
                                              )),
                                          child: const Text(
                                            style: TextStyle(fontSize: 12),
                                            textAlign: TextAlign.center,
                                            "تاریخ:",
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                          flex: 4,
                                          child: InkWell(
                                            child: Text(
                                              product_auctionStartDate.toPersianDate(),
                                              textAlign: TextAlign.center,
                                            ),
                                            onTap: () async {
                                              final DateTime? date = await showPersianDatePicker(
                                                context: context,
                                              );
                                              product_auctionStartDate = date!;
                                              debugPrint('$product_auctionStartDate');
                                              setState(() {});
                                            },
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 30,
                                  margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black12),
                                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          height: 30,
                                          alignment: Alignment.center,
                                          decoration: const BoxDecoration(
                                              borderRadius: BorderRadius.all(Radius.circular(8)),
                                              image: DecorationImage(
                                                fit: BoxFit.cover,
                                                image: AssetImage(
                                                  "images/textFielsBg.png",
                                                ),
                                              )),
                                          child: const Text(
                                            style: TextStyle(fontSize: 12),
                                            textAlign: TextAlign.center,
                                            "ساعت:",
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: InkWell(
                                            child: Text(
                                              '${product_auctionStartDate.hour}:${product_auctionStartDate.minute}',
                                              textAlign: TextAlign.center,
                                            ),
                                            onTap: () async {
                                              final TimeOfDay? time = await showPersianTimePicker(
                                                initialTime: TimeOfDay.now(),
                                                context: context,
                                              );
                                              product_auctionStartDate = DateTime(product_auctionStartDate.year, product_auctionStartDate.month, product_auctionStartDate.day, time?.hour ?? 8, time?.minute ?? 0, 0);
                                              setState(() {});
                                            }),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          const Divider(
                            color: Colors.black26,
                            thickness: 1,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Stack(
                            children: [
                              Container(
                                height: 30,
                                margin: const EdgeInsets.only(left: 40, right: 20, bottom: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black12),
                                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 5,
                                      child: Container(
                                        height: 30,
                                        alignment: Alignment.center,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(8)),
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: AssetImage(
                                                "images/textFielsBg.png",
                                              ),
                                            )),
                                        child: const Text(
                                          style: TextStyle(fontSize: 12),
                                          textAlign: TextAlign.center,
                                          "قیمت فروش فوری:",
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        controller: product_auctionImmediateSalePriceController,
                                        keyboardType: TextInputType.number,
                                        textDirection: TextDirection.ltr,
                                        textAlign: TextAlign.center,
                                        textAlignVertical: TextAlignVertical.center,
                                        decoration: const InputDecoration(
                                          suffixText: " تومان ",suffixStyle: TextStyle(fontSize: 12),
                                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                          border: InputBorder.none,
                                          contentPadding: EdgeInsets.only(bottom: 20),
                                          errorStyle: TextStyle(
                                            height: 0,
                                          ),
                                          errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                                        ),
                                        onChanged: (String value) {
                                          // set 1000 separator on add value
                                          if (value.replaceAll(RegExp('\\D'), '').isNotEmpty) {
                                            product_auctionImmediateSalePriceController.text = '${JJ.jjSeRagham(value.replaceAll(RegExp('\\D'), ''))} تومان';
                                            product_auctionImmediateSalePriceController.selection = TextSelection.collapsed(offset: (product_auctionImmediateSalePriceController.text.length));
                                            setState(() {});
                                          } else {
                                            product_auctionImmediateSalePriceController.text = '';
                                            TextSelection.collapsed(offset: product_auctionImmediateSalePriceController.text.length);
                                          }
                                        },
                                        validator: (value) {
                                          if (product_isAuction) {
                                            if (value == null) {
                                              product_auctionInitPrice = product_auctionInitPrice;
                                              return null;
                                            } else if (int.parse(value.replaceAll(RegExp('\\D'), '')) < int.parse(product_auctionInitPrice)) {
                                              errorsForForm += 'قیمت فروش فوری کمتر از قیمت پایه است' + '\n';
                                              return '';
                                            }
                                            product_auctionImmediateSalePrice = value.replaceAll(RegExp('\\D'), '');
                                          }
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                left: 10,
                                top: 5,
                                child: InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => const jjAlertDialog(
                                          type: '',
                                          richText: Text('شما میتوانید قیمتی را به دلخواه با عنوان قیمت فروش فوری'
                                              ' در این کادر وارد کنید که در صورت تمایل، خریداران بتوانند'
                                              ' خارج از فرآیند مزایده سریعا نسبت به خرید آن اقدام کنند '
                                              'لازم بذکراست خریداران تا زمانی میتوانند از این قابلیت استفاده '
                                              'کنند که قیمتهای پیشنهادی به 50درصد حداقل قیمت فروش قطعی نرسیده باشد. '),
                                          title: 'قیمت فروش فوری:',
                                        ));
                                  },
                                  child: const CircleAvatar(
                                    radius: 10,
                                    backgroundColor: Colors.lightBlueAccent,
                                    child: Icon(
                                      Icons.question_mark_rounded,
                                      color: Colors.white,
                                      size: 13,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const Text(
                            "با این قیمت یک خریدار میتواند این کالا را مستقیم خریداری و از مزایده خارج کند",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Divider(
                            color: Colors.black26,
                            thickness: 1,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Stack(
                            children: [
                              Container(
                                height: 30,
                                margin: const EdgeInsets.only(left: 40, right: 20, bottom: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black12),
                                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 5,
                                      child: Container(
                                        height: 30,
                                        alignment: Alignment.center,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(8)),
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: AssetImage(
                                                "images/textFielsBg.png",
                                              ),
                                            )),
                                        child: const Text(
                                          style: TextStyle(fontSize: 12),
                                          textAlign: TextAlign.center,
                                          "حداقل قیمت فروش قطعی:",
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        controller: product_auctionMinPriceController,
                                        keyboardType: TextInputType.number,
                                        textDirection: TextDirection.ltr,
                                        textAlign: TextAlign.center,
                                        textAlignVertical: TextAlignVertical.center,
                                        decoration: const InputDecoration(
                                          suffixText: " تومان ",suffixStyle: TextStyle(fontSize: 12),
                                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                          border: InputBorder.none,
                                          contentPadding: EdgeInsets.only(bottom: 20),
                                          errorStyle: TextStyle(
                                            height: 0,
                                          ),
                                          errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                                        ),
                                        onChanged: (String value) {
                                          // set 1000 separator on add value
                                          if (value.replaceAll(RegExp('\\D'), '').isNotEmpty) {
                                            product_auctionMinPriceController.text = '${JJ.jjSeRagham(value.replaceAll(RegExp('\\D'), ''))}';
                                            product_auctionMinPriceController.selection = TextSelection.collapsed(offset: product_auctionMinPriceController.text.length);
                                          } else {
                                            product_auctionMinPriceController.text = '';
                                            TextSelection.collapsed(offset: product_auctionMinPriceController.text.length);
                                          }
                                        },
                                        validator: (value) {
                                          if (product_isAuction) {
                                            if (value == null || value.replaceAll(RegExp('\\D'), '').isEmpty) {
                                              errorsForForm += 'قیمت فروش قطعی را وارد کنید' + '\n';
                                              return '';
                                            }
                                            product_auctionMinPrice = value.replaceAll(RegExp('\\D'), '');
                                          }
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                left: 10,
                                top: 5,
                                child: InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => const jjAlertDialog(
                                          type: '',
                                          richText: Text(
                                              'شما در زمان  بارگذاری و ارایه کالایتان  میتوانید برای مثال قیمت خرید خود را با عنوان حداقل قیمت فروش قطعی در بخش تنظیمات  وارد کنید که اگر قیمت پیشنهادهای مزایده  به این قیمت نرسید برای جلوگیری از ضرر وزیان شما میتوانید ملزم به فروش و ارسال جنس نباشید .لازم بذکر است  قیمتی که شما ثبت میکنید به چیزهای زیادی اعم از قیمت خریدتان، میزان سودی که در نظر دارید، زیر قیمت بودن روز،صرفا فروش کالا به هر قیمتی  و یا هر دلیل دیگری که متاثر از نگرش و سیاستهای فروش شما هست دارد.'),
                                          title: 'حداقل قیمت فروش قطعی:',
                                        ));
                                  },
                                  child: const CircleAvatar(
                                    radius: 10,
                                    backgroundColor: Colors.lightBlueAccent,
                                    child: Icon(
                                      Icons.question_mark_rounded,
                                      color: Colors.white,
                                      size: 13,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const Text(
                            "اگر کالا به این قیمت برسد فروشنده موظف به ارسال است",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          const Divider(
                            color: Colors.black26,
                            thickness: 1,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                            padding: const EdgeInsets.all(10),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                  "images/textFielsBg.png",
                                ),
                              ),
                            ),
                            child: Row(
                              children: [
                                const Expanded(
                                  flex: 3,
                                  child: Text(
                                    textAlign: TextAlign.center,
                                    "مدت مزایده",
                                  ),
                                ),
                                Expanded(
                                  flex: 3,
                                  child: DropdownButtonFormField(
                                    hint: const SizedBox(
                                        width: 50,
                                        child: Text(
                                          'انتخاب کنید',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 12,
                                          ),
                                        )),
                                    isDense: true,
                                    alignment: Alignment.center,
                                    menuMaxHeight: 500,
                                    dropdownColor: const Color(0xaae8ac4f),
                                    items: [
                                      "1",
                                      "2",
                                      "3",
                                      "4",
                                      "5",
                                      "6",
                                      "7",
                                    ].map((String val) {
                                      return DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: val,
                                        child: Text(
                                          style: const TextStyle(
                                            fontSize: 12,
                                          ),
                                          val,
                                        ),
                                      );
                                    }).toList(),
                                    //
                                    decoration: const InputDecoration(
                                      border: InputBorder.none,
                                      isDense: true,
                                      errorStyle: TextStyle(height: 0),
                                      contentPadding: EdgeInsets.all(0),
                                      floatingLabelAlignment: FloatingLabelAlignment.center,
                                    ),
                                    validator: (value) {
                                      if (product_isAuction) {
                                        if (value == null || value.isEmpty) {
                                          errorsForForm += 'مدت زمان اعتبار مزایده را مشخص کنید' + '\n';
                                          return '';
                                        }
                                        product_auctionEndDate = product_auctionStartDate.add(Duration(days: int.parse(value)));
                                      }
                                    },
                                    onChanged: (String? value) {
                                      debugPrint(">>>>>" + value.toString());
                                    },
                                  ),
                                ),
                                const Expanded(
                                  flex: 3,
                                  child: Text(
                                    textAlign: TextAlign.center,
                                    "روز",
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          const Divider(
                            color: Colors.black26,
                            thickness: 1,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                    //===========End of Auction And Ordinary sale
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Checkbox(
                              activeColor: const Color(0xffe8ac4f),
                              value: checkedValue,
                              onChanged: (newValue) {
                                setState(() {
                                  checkedValue = newValue!;
                                  if (checkedValue) {
                                    debugPrint("checkedValue....<>>>");
                                    product_postCostBySeller=1;
                                  } else {
                                    debugPrint("uncheckedValue....<>>>");
                                    product_postCostBySeller=0;
                                  }
                                });
                              },
                            ),
                          ),
                          const Expanded(
                            flex: 4,
                            child: Text(style: TextStyle(fontSize: 14), " هزینه ی ارسال با خودم "),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Text(
                      errorsForForm,
                      style: const TextStyle(
                        fontSize: 12,
                        height: 2,
                        color: Colors.red,
                      ),
                    ),
                    if (
                    (widget.productDetailJSON[0]['product_status'].toString() == 'ثبت اولیه' || widget.productDetailJSON[0]['product_status'].toString() == 'تایید ادمین') && (widget.productDetailJSON[0]['product_isAuction'].toString() == '0') // while simple product goes to be an auction
                        )
                      TextButton(
                        onPressed: validateAndSave,
                        // To get data I wrote an extension method bellow,
                        child: Container(
                          alignment: Alignment.center,
                          width: 120,
                          margin: const EdgeInsets.all(20),
                          padding: const EdgeInsets.all(8),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Color(0xffe8ac4f),
                          ),
                          child: Text(
                            "ثبت تغییرات",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),

                      TextButton(
                        onPressed: deleteProductByOwnerUser,
                        // To get data I wrote an extension method bellow,
                        child: Container(
                          alignment: Alignment.center,
                          width: 120,
                          margin: const EdgeInsets.all(20),
                          padding: const EdgeInsets.all(8),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Colors.red,
                          ),
                          child: const Text(
                            "حذف",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
              // 11th Section Footer================================================
              bottomNavigationBar: const NavBarBottom()),
        ));
  }
}
