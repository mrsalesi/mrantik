import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';


import 'commisionsTable.dart';

class Rules extends StatelessWidget {
  const Rules({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: TextStyle(color:Colors.black,fontFamily: 'B_Yekan',height: 2,fontSize: 20),
      child: Center(
        child: Container(
            width: MediaQuery.of(context).size.width - 10,
            height: MediaQuery.of(context).size.height - 10 ,
            child: Container(
              width: MediaQuery.of(context).size.width - 5,
              height: MediaQuery.of(context).size.height - 20 ,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(color: Color(0xffe8ac4f) ,width: 4 ),
              ),
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    children: [
                      Icon(Icons.circle_notifications_rounded,size:40, color: Color(0xffe8ac4f),),
                      Divider(color: Color(0xffe8ac4f),thickness: 2,),
                      Text(
                        "متن قوانین و مقررات وتمام :",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      // Image.asset("images/login.png", scale: 6),
                      SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                          child: Text.rich(
                        style: const TextStyle(fontSize: 16, height: 2),
                        TextSpan(children: [
                          TextSpan(
                              text: "\n جدول تعرفه ها \n",
                              style: const TextStyle(
                                  color: Color(0xffe8ac4f),
                                  fontWeight: FontWeight.bold,
                                fontSize: 18
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  showDialog(
                                      context: context,
                                      useSafeArea: true,
                                      builder: (_) => AlertDialog(
                                            insetPadding:
                                                const EdgeInsets.symmetric(
                                                    horizontal: 20,
                                                    vertical: 40),
                                            shape: const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5.0))),
                                            contentPadding:
                                                const EdgeInsets.all(0.0),
                                            content: Container(
                                              child: const CommissionsTable(),
                                            ),
                                          ));
                                }),
                          const TextSpan(
                              text: "توافقنامه کاربری:" "\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text:
                                  "به قسمت توافق نامه کاربر اپلیکیشن وتمام خوش آمدید. خدماتی که در دسترس شما میباشد توسط شخص حقیقی مهندس آرمین پروین ارائه می گردد. در صورت عدم توافق با این شرایط شما قادر به استفاده از خدمات سایت نخواهید بود"),
                          const TextSpan(
                              text: "\nماده 1- طرفین توافق نامه\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text:
                                  "این توافق نامه فی ما بین اپلیکیشن وتمام از یک طرف و کاربران اینترنتی اعم از خریداران و فروشندگان از طرف دیگر در جهت تسهیل در معاملات و افزایش امنیت در فضای اینترنتی منعقد می‏گردد و کلیه حقوق مادی و معنوی آن به این شخص تعلق دارد"),
                          const TextSpan(
                              text: "\nماده 2- موضوع توافق نامه\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text:
                                  "موضوع قرارداد عبارت است از ایجاد و ارائه شبکه ‏ای مطمئن که در آن کاربران اینترنت، اعم از خریداران و فروشندگان، با استفاده از تسهیلات سایت و اپ اقدام به خرید و فروش و مزایده اینترنتی می نمایند. کاربران اینترنتی می‏توانند در هر جای ایران، .مورد مبادله قرار دهند  www.vatamam.com مورد نظر خود را با استفاده اپلیکیشن و سایت "),
                          const TextSpan(
                              text: "\nماده 3- مدت اعتبار توافق نامه\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text:
                                  "این توافق نامه از هنگامی که کاربر با آن موافقت می نماید تا زمانی که به یکی از دلایل قراردادی یا قانونی فسخ شود معتبر خواهد بود ."),
                          const TextSpan(
                              text: "\nماده 4- شروط توافق نامه\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text: "         " "4-1- شرایط عضویت کاربر" "\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text:
                                  "در مورد اشخاص حقیقی، تنها اشخاص بالای 18 سال که مقیم ایران باشند می‌توانند به عضویت وتمام درآمده و از خدمات آن در مقام خریدار یا فروشنده استفاده نمایند ."
                                  "\n"
                                  " اشخاص حقوقی که در دفاتر ثبت شرکت‌ها و موسسات غیرتجاری جمهوری اسلامی ایران به ثبت رسیده باشند نیز می‌توانند عضو این سایت شده و از خدمات سایت استفاده نمایند ."),
                          const TextSpan(
                              text: "\n         "
                                  "4-2- ثبت نام و تکمیل شناسه کاربری"
                                  "\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text:
                                  "عضویت و ثبت نام در بخش گالری انلاین 400.000 تومان میباشد که درصورت دعوت 2نفر از دوستانتان با ارسال لینک اپلیکیشن به آنها شما میتوانید بصورت رایگان از این بخش جهت معاملات انلاین استفاده کنید. جهت استفاده از خدمات اپ وسایت، کاربر می بایست هنگام ثبت نام مشخصات کاربری خود را بصورت کاملا صحیح  تکمیل نماید. توجه داشته باشید با انتخاب نام کاربری با هر اسم مستعاری ،این نام در فضای اپ به  کاربران دیگر نشان داده میشود و در غیر اینصورت نام و نام خانوادگی شما قابل مشاهده میباشد. تاکید میگردد که بایستی کد ملی و شماره همراه شخص  کاربر با دقت وارد گردد چون  پس از تایید مراحل ثبت نام این دو گزینه غیر قابل ویرایش است و چنانچه بعدا محرز شود که خطایی صورت گرفته این هویت در لیست سیاه قرار گرفته و به لحاظ قانونی قابل تعقیب و پیگیری میباشد. بعد از تکمیل مراحل ثبت نام، کاربر به عضویت سایت درآمده و مسئولیت کلیه امور انجام شده توسط وی در اپلیکیشن، به عهده ایشان خواهد بود در غیر این صورت مجاز به استفاده از خدمات وتمام نبوده و هرگونه دسترسی یا استفاده ، به معنای تجاوز به حقوق وتمام بوده که قابلیت پیگرد قانونی خواهد داشت. توجه داشته باشید حتما شناسه کاربری و کلمه عبور خود را در اپلیکیشن تخصصی وتمام و صفحاتی که دارای آدرس وتمام دات شاپ می باشد، وارد نمایند بدیهی است در صورت وارد کردن اطلاعات در آدرس هایی بجز آدرس  فوق مسئولیت و عواقب از دست رفتن اطلاعات و یا سرقت اطلاعات با کاربر مورد نظر میباشد. کاربر متعهد می شود تا رمز عبور مناسبی برای حساب کاربری خود تعیین نموده و به منظور حصول امنیت در دسترسی به حساب کاربری خود، شخصا مسئولیت حفظ و نگهداری آن را به عهده گیرد، در صورت عدم تعهد، سایت در قبال آن مسئولیتی نخواهد داشت. در صورت نیاز به رمز عبور جدید، کاربر موظف می باشد تا همه معیارها و فرایندهای حفاظتی شرکت را برای احراز هویت طی نموده و در صورتی که نتواند هویت خود را اثبات نماید سایت این حق را برای خود محفوظ می دارد تا از دسترسی جزیی و یا کلی کاربر مربوطه ، جلوگیری نماید. در هنگام انتخاب نام کاربری انتخاب هر گونه نام کاربری که با شئونات اجتماعی، مذهبی، سیاسی و فرهنگی کشور مغایر باشد، سایت مجاز خواهد بود تا نسبت به ابطال آن اقدام نموده و کاربر حق هیچگونه اعتراض نخواهد داشت."),
                          const TextSpan(
                              text: "\n         "
                                  "تکمیل احراز هویت برای اعضاء حرفه ای در بخش مزایده-3-4"
                                  "\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text:
                                  "کاربران علاقمند به آنتیک که تمایل دارند بصورت حرفه ای بخش مزایده را دنبال کنند با عضویت وی آی پی بایستی بخش احراز هویت را تکمیل کنند که در اولین قدم یک گالری اختصاصی به این دوستان تعلق میگیرد که باید اسم گالری خودرا در کادر مربوطه وارد کنند. در گزینه بعدی جهت درستی و احراز کد ملی بایستی تصویر کارت ملی خود را آپلود کند که میتوانند از گوشی دوربین خود و یابصورت سیو شده در گالریشان مراحل آپلود را انجام دهند. در مرحله بعد جهت بالا بردن اعتبار خود در وتمام و جلب اعتماد خریداران میتوانید سوابق فروشندگی و خدمات خود را عنوان کنند. در مراحل پایانی کاربر مکلف است متن تعهدنامه را حتما مطالعه و آن را تایید نماید که بمنزله عمل به مفاد تعهدنامه است. مرحله بعد واریز وجه جهت استفاده از خدمات اپلیکیشن و سایت در بخش مزایده است که در سطور پایین به این خدمات اشاره میشود :"),
                          const TextSpan(
                              text:
                                  "\n1-قابلیت ایجاد یک گالری اختصاصی در فضای مزایده"),
                          const TextSpan(
                              text:
                                  "\n2-اولویت نمایش اجناس بارگذاری شده توسط کاربر در طول دوره فعالیت."),
                          const TextSpan(
                              text:
                                  "\n3-نشان دادن ویترین کاری و فضای گالری همراه با لوگوی کاربر در صفحه اول مزایدات."),
                          const TextSpan(
                              text:
                                  "\n4- گرفتن اولین مدال اعتبار برای شرکت در مزایدات و نمایش به خریداران."),
                          const TextSpan(
                              text:
                                  "\n5- محصولات مزایده مربوط به کاربران وی ای پی بطور اتوماتیک به مشتریان علاقه مند معرفی میشود."),
                          const TextSpan(
                              text:
                                  "\n6-- محصولات مزایده مربوط به کاربران وی ای پی در سرچ گو گل به علاقه مندان نشان داده میشود."),
                          const TextSpan(
                              text:
                                  "\n7- انجام خدمات طراحی بنر، لوگو و فتوشاپ مربوط به گالری فروشندگان وی ای پی در فضای اپلیکیشن رایگان انجام میشود."),
                          const TextSpan(
                              text:
                                  "\n8-اطلاع رسانی با عکس مزایده با  پیام ( بزودی ) قبل از تاریخ شروع مزایده. "),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 1- کاربر حق فروش، انتقال و استفاده از نام کاربری دیگران را نداشته و در صورت اثبات این موضوع ، سایت بدون اجازه کاربر حق ابطال آن را خواهد داشت"),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 2- وتمام مجاز است به صورت مستقیم یا غیرمستقیم اطلاعاتی را که برای تایید هویت کاربر ضروری بوده کسب نماید. پرسش از خود كاربر برای دریافت اطلاعات تکمیلی ، درخواست تأیید پست الکترونیکی ، استعلام و تحقیق در مورد صحت اطلاعات مرتبط با کارت‌ها یا حساب‌های معرفی‌شده ، برخی از مصادیق این امر تلقی می‌شوند"),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 3- اگر شماره‌ کارت یا شماره حساب كاربر به هر دلیل تغییر نماید، در اسرع وقت موظف خواهد بود اطلاعات مربوطه را دراپلیکیشن یا سایت به روزرسانی نموده در غیر این صورت مسئولیتی متوجه مجموعه مسترانتیک نخواهد بود. این اطلاعات در قسمت ویرایش مشخصات بانکی قابل دسترس میباشد."),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 4- وتمام اطلاعات دریافت شده از اعضا خود را فقط در امور مرتبط معاملاتی استفاده نموده و به هیچ وجه در اختیار سایرین قرار نخواهد داد، همچنین در صورت نیاز به انجام مقاصد تجاری با کسب اجازه از کاربر نسبت به بهره برداری از این اطلاعات، اقدام خواهد نمود."),
                          const TextSpan(
                              text:
                                  "\n" "        " "4-4- امنیت اطلاعات كاربران",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 5- در صورتی که کاربر، عملی خارج از قوانین و مقررات تعیین شده انجام دهد، سایت مجاز خواهد بود بدون اخطار پیشین عضویت کاربر را به حالت تعلیق در آورده و یا آن را ابطال نماید و طبق قوانین و مقررات با ایشان برخورد خواهد شد. کاربر با قبول این توافق نامه موافقت خود را مبنی بر ایفای تعهدات مندرج در قوانین و مقررات و قبول تمامی مطالب درج شده در راهنما اعلام می دارد."),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 6- با قبول این توافق نامه، شرکت مجاز است تا در صورت وقوع هرگونه تقلب، یا کشف احتمال تقلب و کلاهبرداری، عضویت کاربر را معلق و یا ابطال نماید و کاربر حق هیچگونه اعتراضی در این خصوص ندارد."),
                          const TextSpan(
                              text: "\n"
                                  "       "
                                  "4-5- ارتباط میان کاربران با یکدیگر و سایت",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text: "\n"
                                  "ارتباط با وتمام از طریق ایکن پیامها که لینک آن در پایین صفحه اول اپلیکیشن موجود است برقرار می‌گردد. ضمنا  هرکاربر میتواند از طریق آیکن خرید مستقیم از فروشنده در بخش خرید در گالری انلاین و آیکن مشخصات فروشنده در بخش مزایده ها درارتباط باشد."),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 7- بر اساس این توافقنامه كاربر قبول می نماید کلیه اخطارها ، تذکرات و مکاتبات مربوطه، مندرج دراپلیکیشن و سایت  ارسال شده به نشانی پست الکترونیک و یا نشانی پستی درج‌شده هنگام تکمیل درخواست عضویت، به رویت وی می‏رسد. در این صورت پس از درج اخطار بر روی اپ یا ارسال آن به نشانی پست الکترونیک وی یا پیامک ، اخطار، ابلاغ شده تلقی گردیده و در صورت ارسال از طریق پست، اخطار مذکور پس از گذشت سه (۳) روز کاری ابلاغ شده تلقی می‌گردد. اگر كاربر پس از گذشت این مدت از ادامه‌ ارتباط الکترونیکی با موتمام منصرف گردد، سایت مجاز خواهد بود که کلیه حقوق کاربری وی را محدود و یا معلق نماید."),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 8- اگر علیرغم تلاش شبانه روزی پرسنل در جهت فعال بودن سایت، در بعضی از موارد به دلایل غیر ارادی و یا فورس ماژور دسترسی 24 ساعته کاربر به سایت مقدور نباشد، با پذیرش این توافقنامه کاربر حق هیچگونه اعتراض و اقامه دعوی در خصوص قطع تماس احتمالی و همچنین زیانها و خسارات احتمالی وارده ناشی از موارد مذکور را نداشته و این حق را از خود سلب می نماید.",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 9- چنانچه علیرغم تلاش پرسنل مجموعه وتمام جهت کنترل پیامها، هرگونه پیام شامل توهین، تمسخر و غیره..، توسط کاربر دیگری دریافت شود، این مجموعه هیچ گونه مسئولیتی در قبال پیام ارسال شده و یا دریافت شده ندارد."),
                          const TextSpan(
                              text: "\n"
                                  "تبصره10-اوتمام حق دارد نسبت به ویرایش وضعیت نو،دست دوم یا معیوب و اصالت کالای فروشندگان بدون اعلام قبلی اقدام نماید"),
                          const TextSpan(
                              text: "\n" "4-6- شرح کالا",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(
                              text: "\n"
                                  "برای عرضه کالا می توانید از متن، عکس و گرافیک در گروه های مناسب استفاده کرده و مسئولیت استفاده از هر نوع ابزار فوق که مغایر با شئونات مذهبی، اجتماعی ،سیاسی و فرهنگی کشور باشد منحصرا متوجه کاربر خواهد بود. فروشنده می بایست ضمن پرکردن آیتمهای ویژگی کالا توضیحات بیشتر کالا را در شرح آن درج کند."),
                          const TextSpan(
                              text: "\n"
                                  "تبصره 11- مسنرآنتیک توضیحات محصولات، قیمت آن‌ها و آمار مربوط به فروشندگان یا سایر موارد را تأیید نمی‌کند. همچنین ادعا نمی‌کند که نتایج جست‌وجو لزوما کامل، دقیق، قابل‌اعتماد و به‌ روز باشد. این خدمت برای آگاهی بیشتر و کمک به خریداران طراحی شده و به معنای تایید یا تضمین کالاها و خدمات فروشندگان نیست."),
                          const TextSpan(
                              text: "\n" "",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(text: "\n" ""),
                          const TextSpan(
                              text: "\n" "",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const TextSpan(text: "\n" ""),
                          const TextSpan(text: "\n"),
                        ]),
                        // softWrap: true,
                        // textAlign: TextAlign.justify,
                      )),
                      const Text(
                        "",
                        style: TextStyle(fontSize: 16),
                      ),
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }
}
