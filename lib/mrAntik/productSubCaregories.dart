import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'package:vaTamam/mrAntik/widgets/navBarBottom.dart';
import 'package:page_transition/page_transition.dart';

import 'products.dart';
import 'tools/jjTools.dart';

/**
    This class get one categoury (as parent of subCategoury) and show all sub categouries
 *
 */

class ProductSubCaregories extends StatelessWidget {
  final List<dynamic> categoriesJSON;
  final String parenCategoriyTitle;
  final String parenCategoriyPic;

  ProductSubCaregories(
      {Key? key,
      required this.categoriesJSON,
      required this.parenCategoriyTitle,
      required this.parenCategoriyPic})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Scaffold(
            appBar: AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Colors.black,
                centerTitle: true,
                title: const Header()),
            // 1st Section =========================================================
            body: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      if (!parenCategoriyPic.isEmpty)
                        Expanded(
                          flex: 1,
                          child: Image.network(
                            '${JJ.server}upload/${parenCategoriyPic}',
                            scale: 3,
                          ),
                        ),
                      if (parenCategoriyPic.isEmpty)
                        const Expanded(
                          flex: 1,
                          child: Text(''),
                        ),
                      Expanded(
                        flex: 7,
                        child: Text(
                          "زیر دسته های ${parenCategoriyTitle}",
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  for (int i = 0; i < categoriesJSON.length; i++)
                    Column(
                      children: [
                        SizedBox(
                          height: 35,
                          child: Padding(
                            padding: const EdgeInsets.only(
                              left: 5,
                            ),
                            child: InkWell(
                              onTap: ()  {
                                 JJ()
                                    .jjAjax(
                                        'do=ProductCategory.getMenu&productCategory_parent=${categoriesJSON[i]['id']}')
                                    .then((result) async {
                                  List<dynamic> temp = jsonDecode(result);
                                  if (temp.isEmpty) {
                                    List<dynamic> featuresJsonArray;
                                    JJ().jjAjax(
                                            'do=ProductCategoryFeature.getSelectOptionTags&CategoryFeature_categoryId=${categoriesJSON[i]['id']}')
                                        .then((result2) {
                                      featuresJsonArray = jsonDecode(result2);
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType
                                                  .leftToRight,
                                              child: Products(
                                                categoryId: categoriesJSON[i]
                                                    ['id'],
                                                categoryTitle:
                                                    "$parenCategoriyTitle / ${categoriesJSON[i]['productCategory_title']}",
                                              )));
                                    });
                                    //If there was no sub categories go to add product page
                                  } else {
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type:
                                                PageTransitionType.leftToRight,
                                            child: ProductSubCaregories(
                                              categoriesJSON: temp,
                                              //For example [clock > koko clock]
                                              parenCategoriyTitle:
                                                  "$parenCategoriyTitle / ${categoriesJSON[i]['productCategory_title']}",
                                              parenCategoriyPic:
                                                  parenCategoriyPic,
                                            )));
                                  }
                                });
                              },
                              child: Row(
                                children: [
                                  const Expanded(
                                    flex: 1,
                                    child: Text(""),
                                  ),
                                  Expanded(
                                      flex: 7,
                                      child: Text(categoriesJSON[i]
                                          ["productCategory_title"])),
                                  Expanded(
                                      flex: 1,
                                      child: Image.asset(
                                        "images/leftArrow.png",
                                        scale: 9,
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.black38,
                          thickness: 1,
                        ),
                      ],
                    ),
                ],
              ),
            ),
            // 11th Section Footer================================================
            bottomNavigationBar: const NavBarBottom()));
  }
}
