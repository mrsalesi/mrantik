import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/myWorkBook.dart';
import 'package:vaTamam/mrAntik/setting.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'package:page_transition/page_transition.dart';
import 'package:share_plus/share_plus.dart';

import 'home.dart';
import 'myBalance.dart';
import 'registerVIP.dart';
import 'auctionsOfOneGallery.dart';
import 'products.dart';
import 'myLikedProducts.dart';
import 'rules.dart';
import 'userProfile.dart';
import 'myMessages.dart';
import 'mySalesAndBuys.dart';
import 'tools/jjTools.dart';

class LeftMenu extends StatelessWidget {
  const LeftMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.black,
            centerTitle: true,
            title: const Header()),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  decoration: const BoxDecoration(color: Colors.black87),
                  padding: const EdgeInsets.all(10),
                  alignment: Alignment.center,
                  child: Text(".: ${JJ.user_name} ${JJ.user_family} :.", style: const TextStyle(color: Color(0xffe8ac4f), fontWeight: FontWeight.bold),),
                ),
                if (JJ.user_grade != "VIP") InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const RegisterVIP()));
                  },
                  child: Image.asset(
                    "images/AuctionHeader.png",
                  ),
                ) else
                  InkWell(
                    onTap: () async {
                      debugPrint('AuctionsBySeller.getUserInSessionAuctions()');
                      String result = await JJ().jjAjax('do=Product.getTopAuctionsBySeller'
                          '&product_creator=${JJ.user_id}');
                      List<dynamic> temp = jsonDecode(result);
                      if (temp.isNotEmpty) {
                        Map<String, dynamic> jsonObj = temp[0]; //
                        Navigator.push(context, MaterialPageRoute(builder: (context) => AuctionsOfOneGallery(sellerAndLastAuctionJSON: jsonObj)));
                      } else {
                        JJ.jjToast("شما کالای برای مزایده نگذاشته اید");
                      }
                    },
                    child: Image.asset(
                      "images/AuctionHeaderForVIPs.png",
                    ),
                  ),

                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MySalesAndBuys(onlySales: true,))
                            );
                          },
                          child: InkWell(
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                  child: Image.asset("images/myMenu_Sold.png"),
                                ),
                                const Text('فروش ها', style: TextStyle(fontSize: 12),)
                              ],
                            ),
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const MyMessages())
                            );
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_Comments.png"),
                              ),
                              const Text('پیام ها', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MySalesAndBuys(onlyBuys: true,))
                            );
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_Buy.png"),
                              ),
                              const Text('خرید ها', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                  ],


                ),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => UserProfile())
                            );
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_EditProfile.png"),
                              ),
                              const Text('ویرایش مشخصات کاربری', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MyBalance()
                                )
                            );
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_balance.png"),
                              ),
                              const Text('حساب با وتمام', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MyWorkBook()
                                )
                            );
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_Workbook.png"),
                              ),
                              const Text('کارنامه معاملات', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.leftToRight,
                                    child: Products(
                                      categoryId: '',
                                      categoryTitle: JJ.user_username.isEmpty
                                          ? '${JJ.user_name} ${JJ.user_family}'
                                          : JJ.user_username,
                                      product_creator: JJ.user_id,
                                    )));
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_MyProducts.png"),
                              ),
                              const Text('مشاهده گالری من', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Share.share(
                                'سلام؛ من کاربر اپلیکیشن وتمام شدم، پیشنهاد میکنم  https://www.vatamam.com/');
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_InviteFriends.png"),
                              ),
                              const Text('دعوت از دوستان', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.bottomToTop,
                                    child: const MyLikedProducts()
                                )
                            );
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_Fav.png"),
                              ),
                              const Text('علاقمندی ها', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () async {
                            await JJ().jjAjax('&do=Access_User.signOut').then((result) async {
                              debugPrint(result);
                            });
                            JJ.user_id = '';
                            JJ.user_token = '';
                            JJ.user_token = "";
                            JJ.user_username = "";
                            JJ.user_codeMeli = "";
                            JJ.user_attachPicPersonnelCard = "";
                            JJ.user_id = "";
                            JJ.user_name = "";
                            JJ.user_family = "";
                            JJ.user_mobile = "";
                            JJ.user_postalCode = "";
                            JJ.user_address = "";
                            JJ.user_city = "تهران";
                            JJ.user_email = "";
                            JJ.user_grade = "";
                            JJ.userBalance = ""; //Fill from Payment.java in main.dart
                            JJ.user_AccountInformation = "";
                            JJ.localStorage?.remove('user_token');
                            JJ.localStorage?.setString('likedProductsList', '');
                            Map<String, String> temp = {};
                            temp['0'] = '0';
                            JJ.localStorage!.setString("autoIncreasePriceList", jsonEncode(temp));


                            await Future.delayed(const Duration(seconds: 2));
                            if (context.mounted) {
                              Navigator.of(context).pop();
                            }

                            Navigator.of(context, rootNavigator: true)
                                .pushAndRemoveUntil(
                              MaterialPageRoute(
                                builder: (BuildContext context) {
                                  return const HomePage();
                                },
                              ),
                                  (_) => false,
                            );
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_Exit.png"),
                              ),
                              const Text('خروج', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            showDialog(
                                context: context,
                                useSafeArea: true,
                                builder: (_) =>
                                    AlertDialog(
                                      insetPadding:
                                      const EdgeInsets.symmetric(
                                          horizontal: 20,
                                          vertical: 40),
                                      shape:
                                      const RoundedRectangleBorder(
                                          borderRadius:
                                          BorderRadius.all(
                                              Radius.circular(
                                                  5.0))),
                                      contentPadding:
                                      const EdgeInsets.all(0.0),
                                      content: Container(
                                        child: const Rules(),
                                      ),
                                    ));
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_Help.png"),
                              ),
                              const Text('قوانین وتمام', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Setting())
                            );
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0, bottom: 7),
                                child: Image.asset("images/myMenu_Setting.png"),
                              ),
                              const Text('تنظیمات', style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        )),
                  ],
                ),
              ],
            ),
          ),
        )
    );
  }
}
