import 'package:flutter/material.dart';

class jjAlertDialog extends StatelessWidget {
  final String? type;
  final String? title;
  final String? btnTitle;
  final String? btn2Title;
  final VoidCallback? btnFunction;
  final VoidCallback? btn2Function;
  final Text richText;

  const jjAlertDialog({
    required this.type,
    required this.title,
    required this.richText,
    Key? key,
    this.btnTitle,
    this.btnFunction,
    this.btn2Title,
    this.btn2Function,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 100),
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
        contentPadding: const EdgeInsets.all(0.0),
        content: DefaultTextStyle(
          style: const TextStyle(color: Colors.black, fontFamily: 'B_Yekan', height: 2.5, fontSize: 16),
          child: Center(
            child: SizedBox(
                width: MediaQuery.of(context).size.width - 10,
                child: Container(
                  width: MediaQuery.of(context).size.width - 5,
                  height: MediaQuery.of(context).size.height - 160,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.white, width: 5),
                  ),
                  child: SingleChildScrollView(
                      child: Column(
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width - 5,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("images/bdDialogHeader.jpg"),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: const Icon(
                            Icons.circle_notifications_rounded,
                            size: 40,
                            color: Colors.black45,
                          )),
                      const Divider(
                        color: Colors.black26,
                        thickness: 2,
                        height: 0,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(color: Colors.white),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "$title",
                                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                                  ),
                                  // Image.asset("images/login.png", scale: 6),
                                  const SizedBox(
                                    height: 10,
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              SizedBox(child: richText),
                              const Text("", style: TextStyle(fontSize: 17, height: 2), textAlign: TextAlign.justify),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  if (btnTitle != null && btnTitle!.isNotEmpty)
                                    InkWell(
                                        onTap: () {
                                          btnFunction!();
                                          debugPrint("@@@@@@@@@@@@@@  jjAlertDialog.btnFunction ");
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          // width: 120,
                                          margin: const EdgeInsets.all(20),
                                          padding: const EdgeInsets.all(8),
                                          decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(8)),
                                            color: Color(0xffe8ac4f),
                                          ),
                                          child: Text(btnTitle!),
                                        )),
                                  if (btn2Title != null && btn2Title!.isNotEmpty)
                                    InkWell(
                                        onTap: () {
                                          btn2Function!();
                                          debugPrint("@@@@@@@@@@@@@@  jjAlertDialog.btnFunction ");
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          // width: 120,
                                          margin: const EdgeInsets.all(20),
                                          padding: const EdgeInsets.all(8),
                                          decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(8)),
                                            color: Colors.grey,
                                          ),
                                          child: Text(btn2Title!),
                                        )),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                  ),
                )),
          ),
        ));
  }
}
