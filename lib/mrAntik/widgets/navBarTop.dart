import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import '../products.dart';
import '../register.dart';
import '../productCaregories.dart';
import '../leftMenu.dart';
import '../tools/jjTools.dart';

class NavBarTop extends StatefulWidget {
  const NavBarTop({Key? key}) : super(key: key);

  @override
  State<NavBarTop> createState() => _NavBarTopState();
}
var searchController = TextEditingController();
class _NavBarTopState extends State<NavBarTop> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: InkWell(
            onTap: () async {
              String params = "do=ProductCategory.getMenu&productCategory_parent=0";
              await JJ().jjAjax(params).then((result) async {
                List<dynamic> jsonArray = jsonDecode(result);
                Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ProductCategories(categoriesJSON: jsonArray)));
              });
            },
            child: const Image(
              image: AssetImage('images/categories.png'),
              height: 20,
            ),
          ),
        ),
        Expanded(
          flex: 10,
          child: SizedBox(
            height: 40,
            child: TextField(
              controller: searchController,
              textInputAction: TextInputAction.go,
              decoration: InputDecoration(
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black54, width: 1),
                  ),
                  suffixIcon:searchController.text.length >2 ? InkWell(//show search icon after type 3 character
                      onTap: () {
                        if (searchController.text.isNotEmpty) {
                          Navigator.push(
                              context,
                              PageTransition(
                                  type: PageTransitionType.leftToRight,
                                  child: Products(
                                    categoryId: '',
                                    categoryTitle: 'جستجو برای : ${searchController.text}',
                                    query: searchController.text,
                                  )));
                        }
                      },
                      child: const Image(image: AssetImage("images/search.png"))) : null,
                  border: const OutlineInputBorder(),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1.0),
                  ),
                  labelText: 'جستجو ...',
                  labelStyle: const TextStyle(
                    color: Colors.black54,
                  ),
                  filled: true,
                  fillColor: Colors.black12),
              onChanged: (val){
                setState(() {});
              },
              onSubmitted: (value) {
                if (searchController.text.isNotEmpty) {
                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.leftToRight,
                          child: Products(
                            categoryId: '',
                            categoryTitle: 'جستجو برای : ${searchController.text}',
                            query: searchController.text,
                          )));
                }
              },
            ),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Expanded(
          flex: 2,
          child: InkWell(
            onTap: () {
              if (JJ.user_token.isEmpty) {
                showDialog(
                    context: context,
                    useSafeArea: true,
                    builder: (_) => AlertDialog(
                          insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                          contentPadding: const EdgeInsets.all(0.0),
                          content: Register(),
                        ));
              } else {
                Navigator.push(
                    context,
                    PageTransition(
                      type: PageTransitionType.leftToRight,
                      child: const LeftMenu(),
                    ));
              }
            },
            child: const Image(
              image: AssetImage('images/menu.png'),
              height: 20,
            ),
          ),
        ),
      ],
    );
  }
}
