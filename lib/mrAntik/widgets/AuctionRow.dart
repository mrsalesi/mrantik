
import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:share_plus/share_plus.dart';
import 'package:slide_countdown/slide_countdown.dart';

import '../auction.dart';
import '../tools/jjTools.dart';
import '../tools/mrAnticCalculator.dart';

class AuctionRow extends StatelessWidget {
  final Map<String, dynamic> productJSON;

  const AuctionRow({Key? key, required this.productJSON}) : super(key: key);

  bool checkProductIsLiked() {
    bool isLiked = false;
    String? likedProducts = JJ.localStorage!.getString('likedProductsList');
    debugPrint("likedProducts:$likedProducts");
    if (likedProducts!.contains(',${productJSON['id']},')) {
      isLiked = true;
    }
    return isLiked;
  }

  @override
  Widget build(BuildContext context) {
    DateTime endDate = JJ.toDateTime(productJSON['product_auctionEndDate'].toString());
    Duration dur = endDate.difference(DateTime.now());
    debugPrint('-----------${productJSON['auctionBid_userId']}:${JJ.user_id}');
    // Duration dur = DateTime.now().difference(endDate);
    // print('-----------'+endDate.toString());
    // print(endDate.difference(DateTime.now()));
    // print(productJSON['product_auctionEndDate'].toString().toPersianDate());

    return Column(children: [
      Container(
        margin: const EdgeInsets.only(top: 10),
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        // child: Image.asset('images/auction.jpg'),
        decoration: BoxDecoration(shape: BoxShape.rectangle, color: Colors.white54, borderRadius: BorderRadius.circular(8), border: Border.all(color: Colors.black12)),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        if (productJSON['product_status'] == "ثبت اولیه") // User can delete Auction if only it is in this state
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Auction(
                                            productJSON: productJSON,
                                          )));
                            },
                            child: const Icon(Icons.delete_outline, color: Colors.redAccent),
                          ),
                        const SizedBox(
                          height: 150,
                        ),
                        InkWell(
                          onTap: () {
                            Share.share('${productJSON['product_name']} https://mrAntik.shop/Product/${productJSON['id']}');
                          },
                          child: const Icon(
                            Icons.share_outlined,
                            color: Colors.black26,
                            size: 30.0,
                          ),
                        ),
                      ],
                    )),
                Expanded(
                  flex: 4,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Auction(
                                    productJSON: productJSON,
                                  )));
                    },
                    child: Image.network(
                      '${JJ.server}upload/${productJSON['product_pic1']}',
                    ),
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Image.asset('images/auctionActiveIcon.png', scale: 1.5, alignment: Alignment.topCenter),
                        const SizedBox(
                          height: 150,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Auction(
                                          productJSON: productJSON,
                                        )));
                          },
                          child: checkProductIsLiked()
                              ? const Icon(
                                  Icons.favorite_outlined,
                                  color: Colors.redAccent,
                                  size: 30.0,
                                )
                              : const Icon(
                                  Icons.favorite_outline_outlined,
                                  color: Colors.black26,
                                  size: 30.0,
                                ),
                        )
                      ],
                    )),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Column(
              children: [
                SlideCountdownSeparated(
                  textStyle: const TextStyle(fontFamily: "Tahoma", color: Colors.white, fontWeight: FontWeight.bold),
                  width: 40,
                  decoration: const BoxDecoration(color: Colors.black54),
                  textDirection: TextDirection.rtl,
                  duration: dur,
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                children: [
                  Expanded(
                      flex: 3,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Auction(
                                        productJSON: productJSON,
                                      )));
                        },
                        child: Row(
                          children: [
                            Text(
                              ' ${productJSON['product_name']}',
                              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ],
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Row(
                        children: [
                          Text("کد: ${productJSON['id']}"),
                        ],
                      ))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 25, top: 20),
              child: Row(
                children: [
                  const Expanded(flex: 2, child: Text("تعداد پیشنهاد ها:", style: TextStyle(fontSize: 14))),
                  Expanded(flex: 3, child: Text('${productJSON['count_bid']}', style: const TextStyle(fontSize: 14))),
                  const Expanded(flex: 1, child: Text('')),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 25, top: 20),
              child: Row(
                children: [
                  const Expanded(flex: 2, child: Text("آخرین پیشنهاد:", style: TextStyle(fontSize: 14))),
                  Expanded(flex: 3, child: Text(textDirection: TextDirection.ltr, '${productJSON['auctionBid_price'].toString().seRagham()}', style: const TextStyle(fontSize: 14))),
                  const Expanded(flex: 1, child: Text('تومان', style: TextStyle(fontSize: 12))),
                  Expanded(flex: 2, child: '${productJSON['auctionBid_userId']}' == JJ.user_id ? const Text('شما', style: TextStyle(fontSize: 14, color: Colors.green)) : const Text('دیگران', style: TextStyle(fontSize: 14, color: Colors.redAccent)))
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Auction(
                              productJSON: productJSON,
                            )));
              },
              borderRadius: const BorderRadius.all(Radius.circular(5)),
              highlightColor: Colors.black,
              child: Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.all(4),
                height: 30,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.black,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'پیشنهاد بده:   ',
                      style: TextStyle(color: Colors.white),
                    ),Text(
                      textDirection: TextDirection.ltr,
                      MrAnticCalculator.nextBid(productJSON['auctionBid_price'].toString(), productJSON['product_auctionInitPrice'].toString()).toString().seRagham(),
                      style: const TextStyle(color: Colors.white),
                    ),const Text(
                      '   تومان    ',
                      style: TextStyle(color: Colors.white),
                    ),
                    Image.asset('images/auctionActiveIcon.png', alignment: Alignment.centerLeft),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    ]);
  }
}
