import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../AuctionsBySeller.dart';
import '../auctionsOfOneCategory.dart';
import '../tools/jjTools.dart';

class AuctionCategory extends StatefulWidget {
  final int categoryId;
  final bool? isActive;

  const AuctionCategory({required this.categoryId, this.isActive, Key? key})
      : super(key: key);

  @override
  State<AuctionCategory> createState() => _AuctionCategoryState();
}

class _AuctionCategoryState extends State<AuctionCategory> {
  List<dynamic>? categoriesJSON = ['0'];

  @override
  void initState() {
    debugPrint('AuctionCategory.categoryId=${widget.categoryId}');
    super.initState();
    getMenu(0);
  }

  getMenu(int productCategory_parent) async {
    debugPrint('AuctionCategory.initState()->getMenu() ...');
    String result = await JJ().jjAjax(
        'do=ProductCategory.getMenu&productCategory_parent=$productCategory_parent');
    // topProductList=[1];
    categoriesJSON?.addAll(jsonDecode(result));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    int categoryId = widget.categoryId;
    return Container(
      //note: Category of Auctions in Sliders using carousel slider
      color: Colors.black,
      child: CarouselSlider(
        options: CarouselOptions(
          pageSnapping: false,
          viewportFraction: 0.27,
          // how many slide in one page
          initialPage: 0,
          enableInfiniteScroll: false,
          padEnds: false,
          height: 100,
          reverse: true,
        ),
        items: categoriesJSON?.map((item) {
          return Builder(
            builder: (BuildContext context) {
              return item == '0'// The first item is all
                  ? InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: const AuctionsBySeller(categoryId: '',
                                  categoryTitle: 'جدید ترین ها',)));
                      },
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.all(5),
                              height: 70,
                              width: 70,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border:
                                    Border.all(color: Colors.white, width: categoryId==0?1:0.5),
                              ),
                              child: Image.asset(
                                'images/auctionIcon.png',
                                scale: 1.8,
                                color: Colors.white.withOpacity(categoryId==0?1:0.5),
                                colorBlendMode: BlendMode.modulate,
                              )),
                          Text(
                            'همه',
                            style:
                                TextStyle(color: Colors.white.withOpacity(categoryId==0?1:0.5)),
                          )
                        ],
                      ),
                    )
                  : InkWell(//For second item to end
                      onTap: () {
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: AuctionsOfOneCategory(
                                    categoryId: int.parse(item['id']))));
                      },
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.all(5),
                              height: 70,
                              width: 70,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border:
                                    Border.all(color: Colors.white.withOpacity('$categoryId'==item['id']?1:0.5), width: 1),
                              ),
                              child: Image.network(
                                '${JJ.server}upload/${item['productCategory_pic']}',
                                scale: 1.8,
                                color: Colors.white.withOpacity('$categoryId'==item['id']?1:0.5),
                                colorBlendMode: BlendMode.modulate,
                              )),
                          Text(
                            '${item['productCategory_title']}',
                            style:
                                TextStyle(color: Colors.white.withOpacity('$categoryId'==item['id']?1:0.5)),
                          )
                        ],
                      ),
                    );
            },
          );
        }).toList(),
      ),
    );
  }
}
