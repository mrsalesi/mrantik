import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vaTamam/mrAntik/home.dart';
import 'package:page_transition/page_transition.dart';

import '../myLikedProducts.dart';
import '../register.dart';
import '../myAuctions.dart';
import '../myMessages.dart';
import '../addNewProductCategories.dart';
import '../tools/jjTools.dart';

class NavBarBottom extends StatefulWidget {
  const NavBarBottom({Key? key}) : super(key: key);

  @override
  State<NavBarBottom> createState() => _NavBarBottomState();
}

class _NavBarBottomState extends State<NavBarBottom> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 58,
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.black,
        ),
        child: Row(
          children: [
            Expanded(
                child: InkWell(
              onTap: () {
                // Navigator.push(context,
                //     MaterialPageRoute(builder: (context) => const HomePage()));
                context.push('/');
              },
              child: Image.asset(
                "images/footer_home.png",
                scale: 1.5,
              ),
            )),
            Expanded(
                child: InkWell(
              onTap: () async {
                if (JJ.user_token.isEmpty) {
                  showDialog(
                      context: context,
                      useSafeArea: true,
                      builder: (_) =>
                          AlertDialog(
                            insetPadding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 40),
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0))),
                            contentPadding: const EdgeInsets.all(0.0),
                            content: Register(),
                          ));
                } else {
                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.bottomToTop,
                          child: const MyLikedProducts()
                      )
                  );
                }
              },
              child: Image.asset(
                "images/footer_favourites.png",
                scale: 1.5,
              ),
            )),
            Expanded(
                child: InkWell(
              onTap: () async {
                if (JJ.user_token.isEmpty) {
                  showDialog(
                      context: context,
                      useSafeArea: true,
                      builder: (_) => AlertDialog(
                            insetPadding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 40),
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0))),
                            contentPadding: const EdgeInsets.all(0.0),
                            content: Register(),
                          ));
                } else {
                  String params =
                      "do=ProductCategory.getMenu&productCategory_parent=0";
                  await JJ().jjAjax(params).then((result) async {
                    List<dynamic> jsonArray = jsonDecode(result);
                    Navigator.push(
                        context,
                        PageTransition(
                            type: PageTransitionType.bottomToTop,
                            child: Categories(categoriesJSON: jsonArray)
                        )
                    );
                  });
                }
              },
              child: Image.asset(
                "images/footer_sale.png",
                scale: 1.5,
              ),
            )),
            Expanded(
                child: TextButton(
              onPressed: () {
                if (JJ.user_token.isEmpty) {
                  showDialog(
                      context: context,
                      useSafeArea: true,
                      builder: (_) => AlertDialog(
                            insetPadding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 40),
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0))),
                            contentPadding: const EdgeInsets.all(0.0),
                            content: Register(),
                          ));
                } else {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          // builder: (context) => const MyAuctions()));
                          builder: (context) => const MyMessages()));
                }
              },
              child: Image.asset(
                "images/footer_messages.png",
                scale: 1.5,
              ),
            )),
            Expanded(
                child: InkWell(
              onTap: () {
                if (JJ.user_token.isEmpty) {
                  showDialog(
                      context: context,
                      useSafeArea: true,
                      builder: (_) => AlertDialog(
                            insetPadding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 40),
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0))),
                            contentPadding: const EdgeInsets.all(0.0),
                            content: Register(),
                          ));
                } else {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const MyAuctions()));
                }
              },
              child: Image.asset(
                "images/footer_myAuctions.png",
                scale: 1.5,
              ),
            )),
          ],
        ),
      ),
    );
  }
}
