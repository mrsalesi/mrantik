import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../auctionsOfOneGallery.dart';
import '../tools/jjTools.dart';

class SellerAndLastAuctionRow extends StatefulWidget {
  final Map<String, dynamic> productJSON;

  const SellerAndLastAuctionRow({Key? key, required this.productJSON}) : super(key: key);

  @override
  State<SellerAndLastAuctionRow> createState() => _SellerAndLastAuctionRowState();
}
  bool isFollowedByThisUser = true;

class _SellerAndLastAuctionRowState extends State<SellerAndLastAuctionRow> {




  Future<void> toggleFollow(String id) async {
      isFollowedByThisUser = isFollowedByThisUser ? false : true;//FOR toggle btn
    debugPrint('isFollowedByThisUser:::$isFollowedByThisUser');
    String params = 'do=Access_User.toglleFollow&id=$id';
    String result = await JJ().jjAjax(params);
    Map<String, dynamic> json = jsonDecode(result);
    SharedPreferences? localStorage;
    localStorage = await SharedPreferences.getInstance();
    if (json['status'] == "0") {
      JJ.jjToast(json['comment']);
    } else {
      // JJ.jjToast(json['comment']);
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    debugPrint('isFollowedByThisUser:::$isFollowedByThisUser');
    if (widget.productJSON['user_followers'].toString().contains(',${JJ.user_id},')) {
      isFollowedByThisUser = true;
    } else {
      isFollowedByThisUser = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      const SizedBox(
        height: 20,
      ),
      Row(
        children: [
          const SizedBox(
            width: 30,
          ),
          Expanded(
              flex: 5,
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AuctionsOfOneGallery(
                                    sellerAndLastAuctionJSON: widget.productJSON,
                                  )));
                    },
                    child: Container(
                      height: 110,
                      // child: Image.asset('images/auction.jpg'),
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.black12,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: '${widget.productJSON['user_attachPicPersonal']}'.isNotEmpty ? Image.network('${JJ.server}upload/${widget.productJSON['user_pic1']}') : null,
                    ),
                  ),
                  Stack(children: [
                    Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(top: 7, bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.black12,
                        shape: BoxShape.rectangle,
                        border: Border.all(
                          color: Colors.black12,
                          width: 1,
                        ),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Row(
                        children: [
                          InkWell(
                              onTap: () {
                                toggleFollow(widget.productJSON['product_creator']);
                              },
                              child: Image.asset(
                                isFollowedByThisUser
                                    ? // if this row is followed by user then che color of follow btn
                                    ('images/unFollowEmpty.png')
                                    : ('images/follow${'${widget.productJSON['user_badge']}'.isEmpty ? 'Empty' : '${widget.productJSON['user_badge']}'}.png'),
                                scale: 1.3,
                                fit: BoxFit.fitHeight,
                              )),
                          Container(
                            padding: const EdgeInsets.only(right: 10),
                            child: Text(
                              'رتبه اعتباری '
                              '${widget.productJSON['user_credit']}',
                              style: const TextStyle(
                                fontSize: 11,
                                color: Colors.black45,
                                height: 2,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: 7,
                      top: 7,
                      child: Image.asset(
                        'images/badge${'${widget.productJSON['user_badge']}'.isEmpty ? 'Empty' : '${widget.productJSON['user_badge']}'}.png',
                        scale: 1.6,
                      ),
                    )
                  ]),
                ],
              )),
          Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.centerLeft,
                child: CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.black12,
                  backgroundImage: '${widget.productJSON['user_attachPicPersonal']}'.isNotEmpty ? NetworkImage('${JJ.server}upload/${widget.productJSON['user_attachPicPersonal']}') : null,
                ),
              ))
        ],
      ),
    ]);
  }
}
