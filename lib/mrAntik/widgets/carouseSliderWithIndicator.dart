import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:photo_view/photo_view.dart';

import '../tools/jjTools.dart';


class CarouselSliderWithIndicator extends StatefulWidget {
  final  List<String> imagesList;
  const CarouselSliderWithIndicator({Key? key, required this.imagesList}) : super(key: key);

  @override
  State<CarouselSliderWithIndicator> createState() =>
      _CarouselSliderWithIndicatorState();
}

class _CarouselSliderWithIndicatorState
    extends State<CarouselSliderWithIndicator> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(border: Border.all(color: Colors.black12)),
          child: AspectRatio(
            aspectRatio: 1,
            child: CarouselSlider(
              options: CarouselOptions(
                aspectRatio: 1,
                viewportFraction: 1,
                autoPlay: false,
                // enlargeCenterPage: true,
                //scrollDirection: Axis.vertical,
                onPageChanged: (index, reason) {
                  setState(
                    () {
                      _currentIndex = index;
                    },
                  );
                },
              ),
              items: widget.imagesList
                  .map(
                    (item) => GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (_) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop();
                              }, // back on click
                              child:
                              PhotoView(
                                minScale: 0.2,
                                imageProvider: NetworkImage(
                                    '${JJ.server}upload/$item'),
                                backgroundDecoration: BoxDecoration(color: Colors.black54),
                              ),
                            );
                          },
                        ));
                      },
                      child: Image.network(
                          '${JJ.server}upload/$item'),
                    ),
                  )
                  .toList(),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: widget.imagesList.map((urlOfItem) {
            int index = widget.imagesList.indexOf(urlOfItem);
            return Container(
              width: 10.0,
              height: 10.0,
              margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _currentIndex == index
                    ? const Color.fromRGBO(0, 0, 0, 0.8)
                    : const Color.fromRGBO(0, 0, 0, 0.3),
              ),
            );
          }).toList(),
        )
      ],
    );
  }
}
