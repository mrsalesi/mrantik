import 'package:flutter/material.dart';

class Header extends StatefulWidget {
  const Header({Key? key}) : super(key: key);

  @override
  State<Header> createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [//
        const Expanded(
          flex: 1,
          child: Text(""),
        ),
        const Expanded(
          flex: 10,
          child: Image(
            image: AssetImage('images/logo.png'),
            height: 75,
          ),
        ),
        Expanded(
          flex: 1,
          child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(Icons.arrow_forward_rounded),color: Colors.white,),
        ),
      ],
    );
  }
}

