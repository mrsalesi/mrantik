import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

import '../product.dart';
import '../tools/jjTools.dart';

class ProductRow extends StatelessWidget {
  final Map<String, dynamic> productJSON;
  const ProductRow({Key? key, required this.productJSON}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      const Divider(
        height: 2,
        thickness: 1,
        indent: 0,
        endIndent: 0,
        color: Colors.black12,
      ),
      const SizedBox(
        height: 5,
      ),
      Row(
        children: [
          Expanded(
              flex: 3,
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>  Product(productJSON: productJSON,)));
                    },
                    child: Row(
                      children: [
                        Text(
                           ' ${productJSON['product_name']}',
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            height: 3,
                          ),
                        ),
                        if(productJSON['product_status'].toString()!='تایید ادمین')
                        Text(
                          ' ${productJSON['product_status']}',
                          style: const TextStyle(
                              color: Colors.grey, fontSize: 12, height: 3),
                        )
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      const Icon(
                        Icons.calendar_month_outlined,
                        color: Colors.black26,
                        size: 20.0,
                      ),
                      Text(
                        ' ${DateTime.parse(productJSON['product_date']).toPersianDateStr()}',
                        style: const TextStyle(
                            color: Colors.grey, fontSize: 12, height: 3),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      const Icon(
                        Icons.location_on_outlined,
                        color: Colors.black26,
                        size: 20.0,
                      ),
                      Text(
                        ' ${productJSON['user_city']}',
                        style: const TextStyle(
                            color: Colors.grey, fontSize: 12, height: 3),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          border: Border.all(
                            color: Colors.black12,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Row(
                          children: [
                            Text(
                                textDirection: TextDirection.ltr,
                                productJSON['product_price1'].toString() == "0"
                                ? 'توافقی'
                                : productJSON['product_price1'].toString().seRagham()),
                            if (productJSON['product_price1'].toString() != "0")
                              const Text(
                                'تومان',
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              )),
          Expanded(
              flex: 2,
              child: AspectRatio(
                aspectRatio: 1,
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>  Product(productJSON: productJSON,)));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.black12,
                      border: Border.all(
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(3),
                    ),
                    child: productJSON['product_pic1'].toString() == ""
                        ? const Image(
                            image: AssetImage('images/temp/cat02.png'),
                          )
                        : Image.network(
                            '${JJ.server}upload/${productJSON['product_pic1']}'),
                  ),
                ),
              )
          )
        ],
      ),
    ]);
  }
}
