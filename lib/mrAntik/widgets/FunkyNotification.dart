import 'package:flutter/material.dart';

class ExpandableText extends StatefulWidget {
  ExpandableText(this.text);
  final String text;
  bool isExpanded = false;
  @override
  _ExpandableTextState createState() => _ExpandableTextState();
}

class _ExpandableTextState extends State<ExpandableText>
    with TickerProviderStateMixin<ExpandableText> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      AnimatedSize(
          duration: const Duration(milliseconds: 200),
          child: ConstrainedBox(
              constraints: widget.isExpanded
                  ? const BoxConstraints()
                  : const BoxConstraints(maxHeight: 35.0),
              child: Text(
                widget.text,
                softWrap: true,
                overflow: TextOverflow.fade,
              ))),
      widget.isExpanded
          ? IconButton(
              icon: const Icon(Icons.expand_less_outlined,
                  color: Colors.grey,
                  size: 30,
                  textDirection: TextDirection.ltr),
              onPressed: () => setState(() => widget.isExpanded = false))
          : IconButton(
              icon: const Icon(Icons.expand_more_outlined,
                  color: Colors.grey,
                  size: 30,
                  textDirection: TextDirection.ltr),
              onPressed: () => setState(() => widget.isExpanded = true))
    ]);
  }
}
