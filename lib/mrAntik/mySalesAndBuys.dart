import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/AuctionRow.dart';
import 'package:page_transition/page_transition.dart';

import 'paymentAndFactor.dart';
import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/navBarBottom.dart';
import 'widgets/navBarTop.dart';

class MySalesAndBuys extends StatefulWidget {
  bool? onlySales;
  bool? onlyBuys;

  MySalesAndBuys({
    Key? key,
    this.onlyBuys,
    this.onlySales,
  }) : super(key: key);
  @override
  State<MySalesAndBuys> createState() => _MySalesAndBuysState();
}

class _MySalesAndBuysState extends State<MySalesAndBuys> {
  late List<dynamic> items = [];

  @override
  void initState() {
    if (widget.onlySales == null && widget.onlySales == null) {
      widget.onlyBuys = true;
      widget.onlySales = false;
    }
    super.initState();
    getMoreItem(0);
  }

  getMoreItem(int lastIndex) async {
    debugPrint('MySalesAndBuys.getMoreItem()');
    String params = '';
    if (widget.onlySales!) {
      params =
          'do=Factor.MySalesAndBuys&onlySales=1&product_factor_creator=${JJ.user_id}&lastIndex=$lastIndex';
    } else {
      params =
          'do=Factor.MySalesAndBuys&onlyBuys=1&product_factor_userId=${JJ.user_id}&lastIndex=$lastIndex';
    }
    String result = await JJ().jjAjax(params);
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('فاکتور خرید یا فروشی برای شما وجود ندارد');
      return;
    }
    items.addAll(temp); //Add new items to available items
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.black,
            centerTitle: true,
            title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                // 1st Section =========================================================
                //########################################
                for (int i = 0; i < items.length; i++)
                  InkWell(
                    onTap: () async {
                      String params = "do=Factor.getPaymentAndFactor";
                      params += "&id=${items[i]['payment_factor_id']}";
                      String result = await JJ().jjAjax(params);
                      List<dynamic> jsonTemp = jsonDecode(result);
                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.bottomToTop,
                              child:  PaymentAndFactor(
                                  paymentAndFacotrJSON: jsonTemp[0])));
                    },
                            // items[i]['product_name'].toString() +
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                            Radius.circular(8)),
                        border: Border.all(
                          color: Colors.black26,
                        ),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                              flex: 2,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment
                                    .start,
                                children: [
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(
                                            8.0),
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              textAlign: TextAlign.right,
                                              ' ${items[i]['product_name']}',
                                              style: const TextStyle(
                                                  fontWeight:
                                                  FontWeight.bold),
                                            ),

                                            Align(
                                              child: Text(
                                                textAlign: TextAlign
                                                    .right,
                                                " کد:${items[i]['product_factor_productId']} ",
                                                style: const TextStyle(
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                   Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      textDirection: TextDirection.rtl,
                                      softWrap: true,
                                      textAlign: TextAlign.right,
                                      "سریال فاکتور: ${items[i][
                                      'id']}"
                                      ,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      items[i][
                                      'product_factor_statusLog'].toString().replaceAll("%23A%23", "\n"),
                                      softWrap: true,
                                      textAlign: TextAlign.justify,
                                      style: const TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                          Expanded(
                              flex: 1,
                              child: Container(
                                  padding: const EdgeInsets.all(4),
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(8.0)),
                                  ),
                                  child: Image.network(
                                    '${JJ.server}upload/${items[i]['product_pic1']}',
                                    fit: BoxFit.fitWidth,
                                  ))),
                        ],
                      ),
                    ),
                  ),
                //########################################
                const SizedBox(
                  height: 10,
                ),
                const Center(
                  child: Icon(
                    Icons.add_circle_outlined,
                    color: Colors.black12,
                    size: 55,
                  ),
                )
              ],
            ),
          ),
        ),
    );
  }
}
