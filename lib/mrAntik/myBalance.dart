import 'dart:convert';

import 'package:flutter/material.dart';

import 'tools/jjTools.dart';
import 'widgets/Header.dart';

class MyBalance extends StatefulWidget {
  const MyBalance({
    Key? key,
  }) : super(key: key);

  @override
  State<MyBalance> createState() => _MyBalanceState();
}

class _MyBalanceState extends State<MyBalance> {
  List<dynamic>? resultJson ;
  @override
  void initState() {
    super.initState();
    getUserWorkBook();
  }

  getUserWorkBook() async {
    String params = '';
    params = 'do=Factor.getMyBalance'; //user ID is in session
    params += '&user_Id=${JJ.user_id}'; //user ID is in session
    String result = await JJ().jjAjax(params);
    debugPrint('Factor.getMyBalance()>>'+result);
    resultJson = jsonDecode(result);
    if (resultJson!.isEmpty) {
      JJ.jjToast('خرید و فروشی برای شما ثبت نشده');
      return;
    }
    ; //Add new items to available items
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
      // 1st Section =========================================================
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              // 1st Section =========================================================
              //########################################
              Column(
                children: [
                  const Text(
                    "حساب با وتمام",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "میزان قابل برداشت:",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['sum_payment'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "مجموع پرداخت من: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['sum_payment'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "مجموع فروش من: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['sum_payment'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "مجموع خرید من: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            resultJson==null?"-": resultJson![0]['sum_payment'],
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
              //########################################
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
