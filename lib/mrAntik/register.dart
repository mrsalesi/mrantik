import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';
import 'rules.dart';
import 'tools/jjTools.dart';
import 'widgets/allertDialog.dart';

class Register extends StatefulWidget {
  Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();


}

class _RegisterState extends State<Register> {
  String errorsForForm = "";
  String _user_username = "";
  String _user_name = "";
  String _user_family = "";
  String _user_codeMeli = "";
  String _user_mobile = "";
  String _user_city = "";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  //This function send request to server
  Future<void> validateAndSave() async {
    final FormState? form = _formKey.currentState;
    if (form!.validate()) {
      debugPrint('Form is valid');
      String params = "do=Access_User.registerUser";
      params+= "&user_username=${_user_username}" ;
      params+= "&user_name=${_user_name}";
      params+= "&user_family=${_user_family}";
      params+= "&user_codeMeli=${_user_codeMeli}";
      params+= "&user_mobile=${_user_mobile}";
      params+= "&user_city=${_user_city}";
      errorsForForm = 'در حال ارسال اطلاعات ...';
      setState(() {});

      await JJ().jjAjax(params).then((result) async {
        debugPrint("======$result");
        Map<String, dynamic> json = jsonDecode(result);
        SharedPreferences? localStorage;
        localStorage = await SharedPreferences.getInstance();
        if (json['status'] == "0") {
          errorsForForm = json['comment'];
          setState(() {});// for refresh changes
          JJ.user_token = "";
          localStorage.setString("user_token", "");
        }else{
          Navigator.of(context, rootNavigator: true).pop();
          errorsForForm = json['comment'];
          JJ.jjToast(json['comment']);
          setState(() {});
        }
      });
    } else {
      setState(() {});
      debugPrint('Form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Container(
              decoration: BoxDecoration(
                color: const Color(0xfef7edff),
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(color: Colors.white54),
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset("images/register.png",
                          width: 140, alignment: Alignment.topRight),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Stack(
                    children: [
                      Container(
                        height: 30,
                        margin: const EdgeInsets.only(
                            left: 20, right: 20, bottom: 10),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                              "images/textfieldBackGroundGold.png",
                            ),
                          ),
                        ),
                        child: Row(
                          children: [
                            const Expanded(
                              flex: 2,
                              child: Text(
                                textAlign: TextAlign.center,
                                "نام کاربری: ",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: TextFormField(
                                textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  contentPadding: EdgeInsets.only(bottom: 20),
                                  errorStyle: TextStyle(
                                    height: 0,
                                  ),
                                ),
                                validator: (value) {
                                  errorsForForm ='';
                                  if (value != null &&
                                      value.isNotEmpty &&
                                      value.length < 2) {
                                    errorsForForm +=
                                        'نام کاربری یا تهی باشد یا از 2 کارکتر کوچکتر نباشد' +
                                            '\n';
                                    return '';
                                  }
                                 _user_username = value!;
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        left: 0,
                        top: 5,
                        child: InkWell(
                          onTap: () {
                            showDialog(
                                context: context,
                                useSafeArea: true,
                                builder: (_) => const jjAlertDialog(type: '',
                                  richText: Text(''
                                      'نام کاربری شما معرف شما در فضای معاملات اپلیکیشن میباشد که اختیاری میباشد در صورت پرنکردن ، نام و نام خانوادگی شما به کاربران نشان داده میشود.')
                                  ,title: 'نام کاربری چیست:',));
                          },
                          child: const CircleAvatar(
                            radius: 10,
                            backgroundColor: Colors.lightBlueAccent,
                            child: Icon(
                              Icons.question_mark_rounded,
                              color: Colors.white,
                              size: 13,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 30,
                    margin:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "نام: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: const InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(bottom: 20),
                              errorStyle: TextStyle(
                                height: 0,
                              ),
                            ),
                            validator: (value) {
                              String pattern = r'[\u0600-\u06FF]+';
                              RegExp regex = RegExp(pattern);
                              if (value == null || value.isEmpty) {
                                errorsForForm =
                                    "وارد کردن نام طبق کارت ملی الزامی است" +
                                        '\n';
                                return '';
                              } else if (!regex.hasMatch(value)) {
                                errorsForForm +=
                                    "نام واقعی کاربر طبق کارت ملی" + '\n';
                                return '';
                              }
                              _user_name = value;
                            },
                          ),
                        ),
                        const Text(
                          " * ",
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    margin:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "نام خانوادگی: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: TextFormField(
                              textAlign: TextAlign.center,
                              textAlignVertical: TextAlignVertical.center,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(bottom: 20),
                                errorStyle: TextStyle(
                                  height: 0,
                                ),
                              ),
                              validator: (value) {
                                String pattern = r'[\u0600-\u06FF]+';
                                RegExp regex = RegExp(pattern);
                                if (value == null || value.isEmpty) {
                                  errorsForForm +=
                                      "وارد کردن نام خانوادگی طبق کارت ملی الزامی است" +
                                          '\n';
                                  return '';
                                } else if (!regex.hasMatch(value)) {
                                  errorsForForm +=
                                      'نام خانوادگی کاربر طبق کارت ملی' + '\n';
                                  return '';
                                }
                                _user_family = value;
                              },
                              ),
                        ),
                        const Text(
                          " * ",
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                  Stack(children: [
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(
                          left: 20, right: 20, bottom: 10),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "images/textfieldBackGroundGold.png",
                          ),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 2,
                            child: Text(
                              textAlign: TextAlign.center,
                              "کد ملی: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              textAlignVertical: TextAlignVertical.center,
                              decoration: const InputDecoration(
                                hintText: '(غیر قابل ویرایش)',
                                hintStyle: TextStyle(
                                  fontSize: 10,
                                ),
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(bottom: 20),
                                errorStyle: TextStyle(
                                  height: 0,
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  errorsForForm +=
                                      'وارد کردن کد ملی الزامی است' + '\n';
                                  return '';
                                } else if (!value
                                    .isValidIranianNationalCode()) {
                                  errorsForForm +=
                                      'کد ملی صحیح نیست' + '\n';
                                  return '';
                                }
                              _user_codeMeli = value;
                              },
                            ),
                          ),
                          const Text(
                            " * ",
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    Positioned(
                      left: 0,
                      top: 5,
                      child: InkWell(
                        onTap: () {
                          showDialog(
                              context: context,
                              useSafeArea: true,
                              builder: (_) => const jjAlertDialog(type: '',
                                richText: Text(''
                                    'دقت کنید کادر های ستاره دار را که متعلق به مشخصات فردی شماست صحیح وارد کنید، زیرا غیر قابل ویرایش است و فقط از طریق پشتیبانی تغییر می کند')
                                ,title: 'کد ملی در وتمام:',));
                        },
                        child: const CircleAvatar(
                          radius: 10,
                          backgroundColor: Colors.lightBlueAccent,
                          child: Icon(
                            Icons.question_mark_rounded,
                            color: Colors.white,
                            size: 13,
                          ),
                        ),
                      ),
                    ),
                  ]),
                  Container(
                    height: 30,
                    margin:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,

                            "شماره همراه: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.center,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: const InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(bottom: 20),
                              errorStyle: TextStyle(
                                height: 0,
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                errorsForForm +=
                                    'وارد کردن شماره همراه الزامی است' + '\n';
                                return '';
                              } else if (!value.replaceAll(" ", '')
                                  .isValidIranianMobileNumber()) {
                                errorsForForm +=
                                    'شماره همراه صحیح نیست' + '\n';
                                return '';
                              }
                              _user_mobile = value.replaceAll(" ", '');
                            },
                          ),
                        ),
                        const Text(
                          " * ",
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  // city = شهر
                  Container(
                    height: 30,
                    margin:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          "images/textfieldBackGroundGold.png",
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 2,
                          child: Text(
                            textAlign: TextAlign.center,
                            "شهر: ",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: DropdownSearch<String>(
                            popupProps: const PopupProps.menu(
                              showSelectedItems: true,
                              showSearchBox: true,
                            ),
                            items: JJ.listOfCites,
                            dropdownDecoratorProps: const DropDownDecoratorProps(
                              textAlign: TextAlign.center,
                              textAlignVertical: TextAlignVertical.center,
                              baseStyle: TextStyle(height: 0,),
                              dropdownSearchDecoration: InputDecoration(
                                fillColor: Color(0xdde8ac4f),
                                focusedBorder: InputBorder.none,
                                floatingLabelAlignment: FloatingLabelAlignment.center,
                                border: InputBorder.none,
                                isDense: true,
                                hintText: "شهرستان یا شهر",
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                errorsForForm +=
                                    'شهر محل سکونت خود را انتخاب کنید' + '\n';
                                return '';
                              }
                              _user_city = value;
                            },
                            onChanged: (value) {
                              debugPrint(">>>>>" + value.toString());
                            },
                            selectedItem: JJ.user_city.isEmpty?"تهران" : JJ.user_city,
                          )
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                      width: 250,
                      child: Text.rich(
                        TextSpan(
                            style: const TextStyle(fontSize: 12),
                            children: [
                              const TextSpan(
                                  text:
                                      "ثبت نام شما در وتمام به منزله پذیرفتن"),
                              const TextSpan(text: "\n"),
                              TextSpan(
                                text: " حریم خصوصی و شرایط و قوانین",
                                style:
                                    const TextStyle(color: Color(0xffe8ac4f)),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    //show rules and conditions
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => AlertDialog(
                                              insetPadding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 20,
                                                      vertical: 40),
                                              shape:
                                                  const RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5.0))),
                                              contentPadding:
                                                  const EdgeInsets.all(0.0),
                                              content: Container(
                                                child: const Rules(),
                                              ),
                                            ));
                                    // Single tapped.
                                  },
                              ),
                              const TextSpan(text: " می باشد"),
                            ]),
                        maxLines: (2),
                        softWrap: true,
                        textAlign: TextAlign.center,
                      )),

                  const SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                      width: 250,
                      child: Text.rich(
                        TextSpan(
                            style: const TextStyle(fontSize: 12),
                            children: [
                              const TextSpan(text: "قبلاَ ثبت نام کرده ام. "),
                              TextSpan(
                                text: "ورود (Login)",
                                style:
                                    const TextStyle(color: Color(0xffe8ac4f)),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.pop(
                                        context); //Remove last dialog boxes
                                    //show rules and conditions
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => AlertDialog(
                                            insetPadding: const EdgeInsets.symmetric(
                                                horizontal: 20, vertical: 40),
                                            shape: const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5.0))),
                                            contentPadding: const EdgeInsets.all(0.0),
                                            content: Login()));
                                    // Single tapped.
                                  },
                              ),
                            ]),
                        softWrap: true,
                        textAlign: TextAlign.center,
                      )),

                  const SizedBox(height: 15),
                  Text(
                    errorsForForm,
                    style: const TextStyle(
                      fontSize: 12,
                      height: 2,
                      color: Colors.red,
                    ),
                  ),
                  const SizedBox(height: 15),
                  InkWell(
                    onTap: validateAndSave,
                    child: Container(
                      alignment: Alignment.center,
                      width: 120,
                      padding: const EdgeInsets.all(8),
                      margin: const EdgeInsets.all(8),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.black38,
                      ),
                      child: const Text(
                        "ثبت نام",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
