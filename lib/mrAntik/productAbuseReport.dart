import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/AuctionRow.dart';

import 'tools/jjTools.dart';
import 'widgets/Header.dart';
import 'widgets/navBarBottom.dart';
import 'widgets/navBarTop.dart';

class ProductAbuseReport extends StatefulWidget {
  final String productId;
  final String productTitle;
  const ProductAbuseReport({Key? key, required this.productId, required this.productTitle}) : super(key: key);

  @override
  State<ProductAbuseReport> createState() => _ProductAbuseReportState();
}

class _ProductAbuseReportState extends State<ProductAbuseReport> {

  var messenger_textMessageController = TextEditingController();
  String errorsForForm = '';


  sendAbuseRepor() async {
    ///Get Remaining time for refresh time in last 30 seconds of auction
    if (messenger_textMessageController.text.length < 3 ) {
      errorsForForm = 'لطفا علت اینکه این محصول را مغایر قوانین وتمام میدانید را شرح دهید';
      setState(() {});
      return;
    }
    debugPrint('Messenger.sendMessageTosupport()... get last Bids');
    String result = await JJ().jjAjax('do=Messenger.sendMessageToSupport'
        '&messenger_title=گزارش تخلف کالای ${widget.productId} ${widget.productTitle}}'
        '&messenger_textMessage=${messenger_textMessageController.text}');
    Map<String, dynamic> json = jsonDecode(result);
    if (json.isEmpty) {
      return;
    } else if (json['status'].toString() == "1") {
      Navigator.pop(context);
    }
    JJ.jjToast(json['comment']);
    setState(() {});
  }



  final TextEditingController user_messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),

                const Text(
                  "ارسال متن به پشتیبانی",
                  style: TextStyle(color: Colors.blue, fontSize: 10, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.all(Radius.circular(8))

                  ),
                    child: Text("گزارش تخلف کالای کد ${widget.productId} : ${widget.productTitle}")),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: messenger_textMessageController,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  minLines: 5,
                  textAlign: TextAlign.right,
                  textAlignVertical: TextAlignVertical.center,
                  decoration: InputDecoration(
                      isDense: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                        ),
                      ),
                      filled: true,
                      fillColor: Colors.black12,
                      hintText: "لطفا علت اینکه این محصول را مغایر قوانین وتمام میدانید را شرح دهید",
                      hintStyle: TextStyle(fontSize: 12)),
                ),
                Text(errorsForForm,
                    style: const TextStyle(
                      fontSize: 12,
                      height: 2,
                      color: Colors.red,
                    )),
                InkWell(
                    onTap: () {
                      sendAbuseRepor();
                    },
                    child: Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.all(20),
                      padding: const EdgeInsets.all(8),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Color(0xffe8ac4f),
                      ),
                      child: Text(" ارسال به بخش پشتیبانی"),
                    )),
                Divider(),
                // 1st Section =========================================================
                //########################################



              ],
            ),
          ),
        ),
        //Footer================================================
        bottomNavigationBar: const NavBarBottom());
  }
}
