import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/FunkyNotification.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'package:vaTamam/mrAntik/widgets/carouseSliderWithIndicator.dart';
import 'package:vaTamam/mrAntik/widgets/navBarBottom.dart';
import 'package:vaTamam/mrAntik/widgets/navBarTop.dart';
import 'package:page_transition/page_transition.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:share_plus/share_plus.dart';
import 'package:slide_countdown/slide_countdown.dart';

import 'productAbuseReport.dart';
import 'registeredSeller.dart';
import 'auctionBids.dart';
import 'payment.dart';
import 'tools/jjTools.dart';
import 'tools/mrAnticCalculator.dart';
import 'widgets/allertDialog.dart';

class Auction extends StatefulWidget {
  final Map<String, dynamic> productJSON; //basic data of product
  Map<String, dynamic>? productJSONDetail; //basic data of product
  Auction({Key? key, required this.productJSON, this.productJSONDetail}) : super(key: key);

  @override
  State<Auction> createState() => _AuctionState();
}

class _AuctionState extends State<Auction> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late DateTime endDate;
  List<dynamic> lastBidsJSON = [];
  late List<dynamic> productDetailJSON = [];
  late List<dynamic> relatedProductsJSON = [];
  late List<String> imagesList = [];
  late Map<String, String> propAndValueList = {};
  bool isLiked = false;
  late int nextBid = MrAnticCalculator.nextBid(widget.productJSON['auctionBid_price'].toString(), widget.productJSON['product_auctionInitPrice'].toString());

  String errorsForForm = "";
  String _price = "";
  late String _autoIncreasePrice = "";
  var nextBidController = TextEditingController();
  var autoIncreasePiceController = TextEditingController();

  @override
  initState() {
    debugPrint('Auction.initState(); JJ.user_grade=${JJ.user_grade}');
    debugPrint('widget.productJSON[product_auctionEndDate].toString()=${widget.productJSON['auctionBid_price'].toString()}');
    endDate = DateTime.parse(widget.productJSON['product_auctionEndDate'].toString());
    if (JJ.user_grade == "VIP") {
      // If user grade is not VIP he/she can not visit Auction
      super.initState();
      getProduct(widget.productJSON['id']);
      checkProductIsLiked();
      setAutoIncreasePriceFromLocalStorage();
      getLastAuctionBids(0, '0');
      nextBidController.text = '${nextBid.toString().seRagham()}';
    } else {
      JJ.jjToast('دوست عزیز فضای مزایده یک فضای حرفه ای و ویژه ی اعضای VIP است. از شما دعوت میکنیم عضویت خود را تکمیل و کاربر ویژه ی ما شوید.');
      Navigator.pop(context);
      return;
    }
  }

  ///  To check if product is liked before or not
  checkProductIsLiked() {
    String? likedProducts = JJ.localStorage!.getString('likedProductsList');
    debugPrint("likedProducts:$likedProducts");
    if (likedProducts!.contains(',${widget.productJSON['id']},')) {
      isLiked = true;
    }
  }

  /// This function get auto Increment price for this Auction that is seted by this user and set to TextFormFiled
  setAutoIncreasePriceFromLocalStorage() {
    int productId = int.parse(widget.productJSON['id'].toString());
    String? tempStr = JJ.localStorage?.getString("autoIncreasePriceList");
    debugPrint(">>>>>>setAutoIncreasePriceFromLocalStorage$tempStr");
    if (tempStr!.isEmpty) {
      tempStr = "{'0':'0'}";
    }
    Map<String, String> autoPricesMap = Map<String, String>.from(jsonDecode(tempStr!));
    autoIncreasePiceController.text = autoPricesMap['$productId'] == null ? '' : '${autoPricesMap['$productId'].toString().seRagham()}';
  }

  getProduct(String id) async {
    debugPrint('getProduct() ...');
    String result = await JJ().jjAjax('do=Product.getProduct&id=$id');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('این محصول در جال حاضر وجود ندارد');
    } else {
      productDetailJSON = temp;
      debugPrint("%%%%%" + productDetailJSON[0]['products_content']);
      for (int i = 1; i <= 9; i++) {
        if (productDetailJSON[0]['product_pic$i'].toString().isNotEmpty) {
          imagesList.add(productDetailJSON[0]['product_pic$i']);
        }
      }
      for (int i = 0; i < 20; i++) {
        // Creation a list of filled prop and values
        if (productDetailJSON[0]['product_val$i'].toString().isNotEmpty && productDetailJSON[0]['product_prop$i'].toString().isNotEmpty) {
          propAndValueList[productDetailJSON[0]['product_prop$i']] = productDetailJSON[0]['product_val$i'];
        }
      }
      debugPrint(')))))))))))))))))))>>>$propAndValueList');
      setState(() {});
      getRelatedProducts(widget.productJSON['id']); //Get related product
    }
  }

  getRelatedProducts(String id) async {
    debugPrint('getRelatedProducts() ...');
    String result = await JJ().jjAjax('do=Product.getRelatedProducts&id=$id&product_isAuction=1');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isNotEmpty) {
      relatedProductsJSON = temp;
      debugPrint("%%%%%${relatedProductsJSON[0]['product_pic1']}");
      setState(() {});
    }
  }

  deleteProductByUser(int id) async {
    debugPrint('ProductAuctionBid.deleteProductByUser()... get last Bids');
    String result = await JJ().jjAjax('do=Product.deleteProductByUser&id=$id');
    Map<String, dynamic> json = jsonDecode(result);
    if (json.isEmpty) {
      return;
    } else if (json['status'].toString() == "1") {
      Navigator.of(context).pop();
      Navigator.of(context).pop();
    }
    JJ.jjToast(json['comment']);
  }

  ///Get last 10 AuctionBids To calculate who is winner
  getLastAuctionBids(int lastIndex, String lastBidId) async {
    debugPrint('ProductAuctionBid.getLastAuctionBids()... get last Bids');
    String result = await JJ().jjAjax('do=ProductAuctionBid.getLastAuctionBids'
        '&auctionBid_productId=${widget.productJSON['id']}'
        '&lastIndex=$lastIndex'
        '&lastBidId=$lastBidId');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      return;
    }
    lastBidsJSON.insertAll(0, temp); //Add new items to available items
    nextBid = MrAnticCalculator.nextBid(lastBidsJSON[0]['auctionBid_price'].toString(), widget.productJSON['product_auctionInitPrice'].toString());
    nextBidController.text = '${nextBid.toString().seRagham()}';
    setState(() {});
  }

  ///Get Remaining time for refresh time in last 30 seconds of auction
  getRemainingTime(String productId) async {
    debugPrint('ProductAuctionBid.getLastAuctionBids()... get last Bids');
    String result = await JJ().jjAjax('do=Product.getRemainingTime'
        '&id=$productId');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      return;
    }
    endDate = JJ.toDateTime(temp[0]['product_auctionEndDate'].toString());
    setState(() {});
  }

  //This function send new AuctionBid request to server
  Future<void> validateAndSave() async {
    final FormState? form = _formKey.currentState;
    if (form!.validate()) {
      debugPrint('Form is valid');
      String params = "do=ProductAuctionBid.insert";
      params += "&auctionBid_productId=${widget.productJSON['id']}";
      params += "&auctionBid_userId=${JJ.user_id}";
      params += "&user_token=${JJ.user_token}";
      params += "&auctionBid_price=${_price.replaceAll(RegExp('\\D'), '')}";
      params += "&auctionBid_autoIncreasePrice=${_autoIncreasePrice.replaceAll('\\D', '')}";
      errorsForForm = 'در حال ارسال اطلاعات ...';
      setState(() {});

      String result = await JJ().jjAjax(params);
      Map<String, dynamic> json = jsonDecode(result);
      if (json['status'] == "0") {
        errorsForForm = json['comment'];
        JJ.jjToast(json['comment']);
        setState(() {}); // for refresh changes
        // JJ.user_token = "";
        // localStorage.setString("user_token", "");
      } else {
        //If price is successfully added and if autoIncreasePrice is set by user must save id in local storage to show in form for next visist of auction
        int productId = int.parse(widget.productJSON['id'].toString());
        String? tempStr = JJ.localStorage?.getString('autoIncreasePriceList');
        if (tempStr!.isEmpty) {
          tempStr = "{'0':'0'}";
        }
        debugPrint('@@@@@@@tempStr:$tempStr');
        Map<String, String> autoPricesMap = Map<String, String>.from(jsonDecode(tempStr!));
        debugPrint('--------------------------$tempStr------${_autoIncreasePrice.replaceAll('\\D', '')}-------------------');
        if (_autoIncreasePrice.replaceAll('\\D', '').isNotEmpty) {
          //Set autoIncreasePrice if available
          autoPricesMap['$productId'] = _autoIncreasePrice.replaceAll('\\D', '');
          JJ.localStorage!.setString('autoIncreasePriceList', jsonEncode(autoPricesMap));
          autoIncreasePiceController.text = '${_autoIncreasePrice.replaceAll('\\D', '').seRagham()} تومان';
          debugPrint('------------------$productId--------$autoPricesMap------------------');
        } else {
          // If user was set some price for auto incise before and now this price is empty,
          autoPricesMap.remove('$productId'); // we must remove it from localStorage
          JJ.localStorage!.setString('autoIncreasePriceList', jsonEncode(autoPricesMap));
          autoIncreasePiceController.text = '';
          debugPrint('------------------$productId--------$autoPricesMap------------------');
        }
        errorsForForm = json['comment'];
        JJ.jjToast(json['comment'] + json['auctionBid_price'].toString().seRagham() + ' تومان ');
        getLastAuctionBids(lastBidsJSON.length, lastBidsJSON.isEmpty ? 0 : lastBidsJSON[0]['id']);
        setState(() {});
      }
    } else {
      setState(() {});
      debugPrint('Form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    if (endDate != null) {
      //do nothing
    } else if (productDetailJSON.isEmpty) {
      endDate = JJ.toDateTime(widget.productJSON['product_auctionEndDate'].toString());
    } else {
      // to Get Updated duration remaining
      endDate = JJ.toDateTime(productDetailJSON[0]['product_auctionEndDate'].toString());
    }
    Duration dur = endDate.difference(DateTime.now());
    return Scaffold(
        appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const NavBarTop(),
                // 2nd Section Categories of Auction=========================================================
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(
                        "وتمام",
                        style: TextStyle(color: Colors.black38),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Colors.black,
                        size: 20.0,
                      ),
                    ],
                  ),
                ),
                // 3th Section =========================================================
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Column(
                          children: [
                            if (widget.productJSON['product_status'] == "ثبت اولیه") // User can delete Auction if only it is in this state
                              InkWell(
                                onTap: () {
                                  deleteProductByUser(int.parse(widget.productJSON['id'].toString()));
                                },
                                child: const Icon(Icons.delete_outline, color: Colors.redAccent),
                              ),
                            const SizedBox(
                              height: 150,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: InkWell(
                                onTap: () {
                                  Share.share('${productDetailJSON[0]['product_name']} https://vatamam.com/Product/${productDetailJSON[0]['id']}');
                                },
                                child: const Icon(
                                  Icons.share_outlined,
                                  color: Colors.black,
                                  size: 30.0,
                                ),
                              ),
                            ),
                          ],
                        )),
                    Expanded(
                      flex: 4,
                      child: (imagesList.isEmpty || imagesList.length == 1) //while the product detail is caching from server
                          ? CarouselSliderWithIndicator(
                              //load carouselSlider with first image from productJson
                              imagesList: ['${widget.productJSON['product_pic1']}'])
                          : CarouselSliderWithIndicator(
                              imagesList: imagesList,
                            ),
                    ),
                    Expanded(
                        flex: 1,
                        child: Column(
                          children: [
                            const SizedBox(
                              height: 180,
                            ),
                            InkWell(
                              onTap: () {
                                isLiked = isLiked == true ? false : true; //toggle isLiked flag
                                // Add this product id to one string list in local storage
                                if (isLiked) {
                                  if (JJ.localStorage!.getString('likedProductsList') == null) {
                                    // for first like
                                    JJ.localStorage!.setString('likedProductsList', ',${widget.productJSON['id']},');
                                  } else {
                                    String? likedProducts = JJ.localStorage!.getString('likedProductsList');
                                    likedProducts = '${likedProducts!},${widget.productJSON['id']},';
                                    JJ.localStorage!.setString('likedProductsList', likedProducts);
                                  }
                                } else {
                                  // remove this id from likedProductsList
                                  if (JJ.localStorage!.getString('likedProductsList') != null) {
                                    // if not null, this may not be accord
                                    String? likedProducts = JJ.localStorage!.getString('likedProductsList');
                                    likedProducts = likedProducts!.replaceAll(',${widget.productJSON['id']},', '');
                                    JJ.localStorage!.setString('likedProductsList', likedProducts);
                                  }
                                }
                                setState(() {});
                              },
                              child: isLiked
                                  ? const Icon(
                                      Icons.favorite_outlined,
                                      color: Colors.redAccent,
                                      size: 30.0,
                                    )
                                  : const Icon(
                                      Icons.favorite_outline_outlined,
                                      color: Colors.black,
                                      size: 30.0,
                                    ),
                            ),
                          ],
                        )),
                  ],
                ),
                // 4th Reverse Timer=========================================================
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Image.asset(
                          'images/timer.png',
                          scale: 1.5,
                        )),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: [
                          const SizedBox(height: 5),
                          if (!dur.isNegative)
                            SlideCountdownSeparated(
                              textStyle: const TextStyle(fontFamily: "Tahoma", color: Colors.white, fontWeight: FontWeight.bold),
                              width: 40,
                              onChanged: (value) {
                                if (value.inMinutes < 15 && value.inMinutes > 0) {
                                  // In 15 last minutes to finish auction time refresh bids every 15 minutes
                                  if (value.inSeconds % 10 == 0) {
                                    // Every 10 seconds
                                    debugPrint("%%%%%% SlideCountdownSeparated.onChanged: $value");
                                    setState(() {
                                      // Refresh last bids from other users
                                      getLastAuctionBids(lastBidsJSON.length, lastBidsJSON.isEmpty ? 0 : lastBidsJSON[0]['id']);
                                    });
                                  }
                                }
                                //In last 30 seconds recheck time, Becuse if any one send new bid in this time 90 seconds add to _end_date
                                if (value.inSeconds < 30 && value.inSeconds > 0) {
                                  if (value.inSeconds % 5 == 0) {
                                    // Every 10 seconds
                                    setState(() {
                                      getRemainingTime(productDetailJSON[0]['id']);
                                    });
                                  }
                                }
                              },
                              onDone: () {
                                JJ.jjToast("وتمام. زمان مزایده پایان یافت، برنده یک پیامک برای واریز وجه دریافت خواهد کرد");
                              },
                              decoration: const BoxDecoration(color: Colors.black54),
                              textDirection: TextDirection.rtl,
                              duration: dur,
                            ),
                          if (dur.isNegative)
                            Container(
                                color: Colors.black87,
                                height: 35,
                                width: 200,
                                child: Image.asset(
                                  "images/logo.png",
                                )),
                        ],
                      ),
                    ),
                    const Expanded(flex: 1, child: Text(""))
                  ],
                ),
                // 5th Section product info=========================================================
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 5,
                        child: Row(
                          children: [
                            Text(
                              widget.productJSON['product_name'],
                              style: const TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold),
                            ),
                          ],
                        )),
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            const Text("کد: "),
                            Text(
                              productDetailJSON.isEmpty ? '' : productDetailJSON[0]['id'].toString(),
                              style: const TextStyle(color: Colors.grey, fontSize: 13),
                            ),
                          ],
                        ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Column(
                  children: [
                    const Align(
                      alignment: Alignment.topRight,
                      child: Text("ویژگی های کالا", textAlign: TextAlign.right),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Column(
                      children: [
                        for (int i = 0; i < propAndValueList.length; i++)
                          Row(
                            children: [
                              Expanded(
                                  flex: 2,
                                  child: Text(
                                    propAndValueList.keys.toList()[i],
                                    //The first attr is property name
                                    style: const TextStyle(color: Colors.grey, fontSize: 12),
                                  )),
                              Expanded(
                                  flex: 4,
                                  child: Container(
                                    padding: const EdgeInsets.all(6),
                                    margin: const EdgeInsets.all(4),
                                    decoration: BoxDecoration(color: Colors.black12, borderRadius: BorderRadius.circular(8)),
                                    child: Text(propAndValueList.values.toList()[i]), //The second attr is value
                                  )),
                              Expanded(
                                  flex: 3,
                                  child: i == propAndValueList.length - 1
                                      ? InkWell(
                                          onTap: () {
                                            Navigator.push(context, PageTransition(type: PageTransitionType.leftToRight, child: ProductAbuseReport(productId: productDetailJSON[0]['id'], productTitle: productDetailJSON[0]['product_name'])));
                                          },
                                          child: const Row(
                                            children: [
                                              SizedBox(width: 15),
                                              Icon(
                                                Icons.not_interested_rounded,
                                                color: Colors.red,
                                                size: 20,
                                              ),
                                              Text(
                                                "گزارش تخلف",
                                                style: TextStyle(color: Colors.grey, fontSize: 12, fontWeight: FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        )
                                      : const Text('')),
                            ],
                          ),
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                // 5th Section =========================================================
                Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AuctionBids(
                                        productJSON: widget.productJSON,
                                      )));
                        },
                        child: Text('پیشنهادها: ${widget.productJSON['count_bid']}', style: const TextStyle(color: Colors.lightBlueAccent, fontSize: 12)),
                      ),
                    ),
                    Expanded(
                      child: Text(productDetailJSON.isEmpty ? '' : 'دفعات مشاهده:  ${productDetailJSON[0]['product_visit']}', style: const TextStyle(color: Colors.grey, fontSize: 12)),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  padding: const EdgeInsets.all(15),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Column(
                    children: [
                      const Align(
                        alignment: Alignment.topRight,
                        child: Text(
                          "توضیحات فروشنده:",
                          style: TextStyle(fontSize: 12, color: Colors.grey),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      ExpandableText(productDetailJSON.isEmpty ? '' : productDetailJSON[0]['products_content']),
                    ],
                  ),
                ),
                // 6th Section Price and Buy =========================================================
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Expanded(flex: 3, child: Text("قیمت پایه", style: TextStyle(fontSize: 13))),
                    Expanded(
                        flex: 3,
                        child: Text(
                          '${widget.productJSON['product_auctionInitPrice'].toString().seRagham()}',
                          style: const TextStyle(fontSize: 14),
                          textDirection: TextDirection.ltr,
                        )),
                    const Expanded(flex: 1, child: Text(' تومان ')),
                    const Expanded(flex: 2, child: Text("", style: TextStyle(fontSize: 11, color: Colors.red))),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Expanded(flex: 3, child: Text("آخرین پیشنهاد", style: TextStyle(fontSize: 14))),
                    Expanded(
                        flex: 3,
                        child: Text(lastBidsJSON.isEmpty ? '${widget.productJSON['auctionBid_price'].toString().seRagham()}' : '${lastBidsJSON[0]['auctionBid_price'].toString().seRagham()}',
                            textDirection: TextDirection.ltr, style: TextStyle(fontSize: 14, color: (lastBidsJSON.isNotEmpty && lastBidsJSON[0]['auctionBid_userId'] == JJ.user_id ? Colors.green : Colors.black)))),
                    const Expanded(flex: 1, child: Text(' تومان ')),
                    if (lastBidsJSON.isNotEmpty) // This list fill after some seconds
                      Expanded(flex: 2, child: (lastBidsJSON[0]['auctionBid_userId'] == JJ.user_id) ? const Text('(دم شما گرم)', style: TextStyle(fontSize: 14, color: Colors.green)) : const Text('(دیگران)', style: TextStyle(fontSize: 14, color: Colors.redAccent)))
                  ],
                ),
                const SizedBox(
                  height: 25,
                ),
                Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        /*Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const [
                            Expanded(flex: 4, child: Text("")),
                            Expanded(
                                flex: 5,
                                child: Center(
                                    child: Text(
                                  "مبلغ تضمین مشارکت در مزایده",
                                  style: TextStyle(
                                    color: Colors.red,
                                    fontSize: 11,
                                  ),
                                ))),
                            Expanded(
                              flex: 1,
                              child: Text(""),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 4,
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: int.parse(JJ.userBalance) <
                                        int.parse(JJ
                                            .publicConfigs['config_auctionBid']
                                            .toString())
                                    ? Image.asset(
                                        "images/chargingValet.png",
                                      )
                                    : Text("کیف پول شما=${JJ.userBalance}"),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: Container(
                                alignment: Alignment.center,
                                width: 170,
                                padding: const EdgeInsets.all(5),
                                height: 35,
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  border: Border.all(
                                    color: Colors.red,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(6),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      JJ.publicConfigs['config_auctionBid']
                                          .toString()
                                          .seRagham(),
                                      style: TextStyle(
                                        color: Colors.red,
                                      ),
                                    ),
                                    Text(
                                      ' تومان ',
                                      style: TextStyle(
                                        color: Colors.red,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: InkWell(
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      useSafeArea: true,
                                      builder: (_) => const jjAlertDialog(
                                            type: '',
                                            richText: Text(
                                                ' این مبلغ در صورتیکه معامله با موفقیت انجام شود به حساب شما برگردانده میشود.'
                                                'اگر بعد از ثبت قیمت در پرداخت وجه نهایی مزایده بیش از حد مجاز تعلل کنید یا در دعاوی مربوط به این مزایده مقصر باشید این مبلغ به نفع فروشنده '
                                                'از حساب شما کسر میشود'),
                                            title: 'شارژ کیف پول:',
                                          ));
                                },
                                child: CircleAvatar(
                                  radius: 10,
                                  backgroundColor: Colors.lightBlueAccent,
                                  child: Icon(
                                    Icons.question_mark_rounded,
                                    color: Colors.white,
                                    size: 13,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),*/
                        Row(
                          children: [
                            Expanded(
                                flex: 4,
                                child: Text(
                                  "ثبت پیشنهاد شما"
                                  "${lastBidsJSON.isNotEmpty && lastBidsJSON[0]['auctionBid_userId'] == JJ.user_id ? "\n(روی پیشنهاد خود شما)" : ""}",
                                  style: TextStyle(fontSize: 13, color: lastBidsJSON.isNotEmpty && lastBidsJSON[0]['auctionBid_userId'] == JJ.user_id ? Colors.deepOrange : Colors.black),
                                )),
                            Expanded(
                                flex: 5,
                                child: SizedBox(
                                  height: 35,
                                  child: TextFormField(
                                    controller: nextBidController,
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    textDirection: TextDirection.ltr,
                                    decoration: const InputDecoration(
                                      suffixText: " تومان ",
                                      suffixStyle: TextStyle(fontSize: 12),
                                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                      border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                      contentPadding: EdgeInsets.only(bottom: 20),
                                      errorStyle: TextStyle(
                                        height: 0,
                                      ),
                                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                                    ),
                                    onChanged: (String value) {
                                      // set 1000 separator on add value
                                      if (value.replaceAll('\D', '').isNotEmpty) {
                                        nextBidController.text = '${value.replaceAll(RegExp('\\D'), '').seRagham()}';
                                        nextBidController.selection = TextSelection.collapsed(offset: nextBidController.text.length);
                                      } else {
                                        nextBidController.text = '';
                                        TextSelection.collapsed(offset: nextBidController.text.length);
                                      }
                                    },
                                    validator: (value) {
                                      errorsForForm = '';
                                      var valTemp = value ?? '0';
                                      valTemp = valTemp.replaceAll(RegExp('\\D'), '');
                                      if (valTemp.isEmpty) {
                                        errorsForForm += 'پیشنهاد خود را ثبت کنید' + '\n';
                                        return '';
                                      }
                                      debugPrint('valTemp>>>$valTemp');
                                      debugPrint('nextBid>>>${nextBid}');
                                      if (int.parse(valTemp) < nextBid) {
                                        errorsForForm += 'پیشنهاد شما از${nextBid.toString().seRagham()}'
                                            ' نمیتواند کمتر باشد'
                                            '\n';
                                        setState(() {
                                          nextBidController.text = '${nextBid.toString().seRagham()}';
                                        });
                                        return '';
                                      }
                                      _price = valTemp;
                                    },
                                  ),
                                )),
                            const Expanded(flex: 1, child: Text('')),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Expanded(
                                flex: 4,
                                child: Text(
                                  "افزایش خودکار پیشنهاد تا",
                                  style: TextStyle(fontSize: 13),
                                )),
                            Expanded(
                                flex: 5,
                                child: SizedBox(
                                  height: 35,
                                  child: TextFormField(
                                    controller: autoIncreasePiceController,
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    textDirection: TextDirection.ltr,
                                    decoration: const InputDecoration(
                                      suffixText: " تومان ",
                                      suffixStyle: TextStyle(fontSize: 12),
                                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                      border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe8ac4f))),
                                      contentPadding: EdgeInsets.only(bottom: 20),
                                      errorStyle: TextStyle(
                                        height: 0,
                                      ),
                                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                                    ),
                                    onChanged: (String value) {
                                      // set 1000 separator on add value
                                      if (value.replaceAll('\D', '').isNotEmpty) {
                                        autoIncreasePiceController.text = '${value.replaceAll(RegExp('\\D'), '').seRagham()}';
                                        autoIncreasePiceController.selection = TextSelection.collapsed(offset: autoIncreasePiceController.text.length);
                                      } else {
                                        autoIncreasePiceController.text = '';
                                        TextSelection.collapsed(offset: autoIncreasePiceController.text.length);
                                      }
                                    },
                                    validator: (value) {
                                      var valTemp = value ?? '0';
                                      valTemp = valTemp.replaceAll(RegExp('\\D'), '');
                                      _autoIncreasePrice = valTemp;
                                    },
                                  ),
                                )),
                            Expanded(
                              flex: 1,
                              child: InkWell(
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      useSafeArea: true,
                                      builder: (_) => const jjAlertDialog(
                                            type: '',
                                            richText: Text('شما در صورت تمایل وبا فعال کردن این گزینه به وتمام اجازه میدهید تا سقف مبلغ موردنظر خود که در کادر مربوطه وارد میکنید بصورت اتوماتیک با حداقل افزایش قیمت نسبت به آخرین مبلغ پیشنهادی رقبا اقدام کند .'),
                                            title: 'افزایش خودکار پیشنهاد:',
                                          ));
                                },
                                child: const CircleAvatar(
                                  radius: 10,
                                  backgroundColor: Colors.lightBlueAccent,
                                  child: Icon(
                                    Icons.question_mark_rounded,
                                    color: Colors.white,
                                    size: 13,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        if (productDetailJSON.isNotEmpty)
                          Row(
                            children: [
                              Expanded(
                                flex: 8,
                                child:
                                    //If reserve price is more than last bid price
                                    (int.parse(productDetailJSON[0]['product_auctionMinPrice']) > int.parse('${widget.productJSON['auctionBid_price']}'.isEmpty ? '0' : '${widget.productJSON['auctionBid_price']}'))
                                        ? const Text(
                                            "کالا به قیمت رزرو نرسیده، فروشنده ملزم به ارسال کالا نیست",
                                            style: TextStyle(
                                              color: Colors.red,
                                              fontSize: 11,
                                            ),
                                          )
                                        : const Text(
                                            "کالا به قیمت فروش قطعی رسیده و فروشنده ملزم است کالا را ارسال کند",
                                            style: TextStyle(
                                              color: Colors.green,
                                              fontSize: 11,
                                            ),
                                          ),
                              ),
                              Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        useSafeArea: true,
                                        builder: (_) => const jjAlertDialog(
                                              type: '',
                                              richText: Text('فروشنده در زمان ارائه جنس  می تواند قیمت خرید خود را با عنوان حداقل قیمت فروش قطعی در بخش تنظیمات  وارد کند که اگر قیمت های پیشنهادی مزایده  به این قیمت نرسید برای جلوگیری از ضرر و زیان میتواند ملزم به فروش نباشد .'),
                                              title: 'قیمت فروش قطعی چیست:',
                                            ));
                                  },
                                  child: const CircleAvatar(
                                    radius: 10,
                                    backgroundColor: Colors.lightBlueAccent,
                                    child: Icon(
                                      Icons.question_mark_rounded,
                                      color: Colors.white,
                                      size: 13,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        const SizedBox(
                          height: 20,
                        ),
                        Center(
                          child: InkWell(
                            onTap: validateAndSave,
                            borderRadius: const BorderRadius.all(Radius.circular(5)),
                            highlightColor: Colors.green,
                            child: Container(
                                margin: const EdgeInsets.all(4.0),
                                child: Image.asset(
                                  "images/auctionSetPrice.png",
                                  scale: 1.2,
                                )),
                          ),
                        ),
                        Text(
                          errorsForForm,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 12,
                            height: 2,
                            color: Colors.red,
                          ),
                        ),
                      ],
                    )),
                const SizedBox(
                  height: 20,
                ),
                if (productDetailJSON.isNotEmpty) // Waiting for loading details
                  if ('${productDetailJSON[0]['product_auctionImmediateSalePrice']}' != '0') // If there was immediate price
                    if ((lastBidsJSON.isEmpty && productDetailJSON.isNotEmpty) || // if product is loaded and there is not any bid
                        lastBidsJSON.isNotEmpty && productDetailJSON.isNotEmpty && int.parse(lastBidsJSON[0]['auctionBid_price']) < int.parse(productDetailJSON[0]['product_auctionImmediateSalePrice'])) // If last bid is less than immediate Sale
                      Container(
                        alignment: Alignment.center,
                        height: 30,
                        width: 280,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.black,
                        ),
                        child: InkWell(
                          onTap: () {
                            if (productDetailJSON[0]['product_creator'].toString() == JJ.user_id) {
                              JJ.jjToast("شما نمی توانید کالای خود را خریداری کنید");
                              return;
                            }
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Payment(
                                          productDetailJSON: productDetailJSON[0],
                                          productJSON: widget.productJSON,
                                        )));
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                "images/saleStep2.png",
                                fit: BoxFit.fitHeight,
                              ),
                              const Text(
                                '    خرید فوری   ',
                                style: TextStyle(color: Colors.white, fontSize: 12),
                              ),
                              Text(
                                productDetailJSON[0]['product_auctionImmediateSalePrice'].toString().seRagham(),
                                textDirection: TextDirection.ltr,
                                textAlign: TextAlign.right,
                                style: const TextStyle(color: Colors.white),
                              ),
                              const Text(
                                '  تومان  ',
                                style: TextStyle(color: Colors.white, fontSize: 12),
                              )
                            ],
                          ),
                        ),
                      ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: InkWell(
                    onTap: () {
                      showDialog(
                          context: context,
                          useSafeArea: true,
                          builder: (_) => AlertDialog(
                                insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                contentPadding: const EdgeInsets.all(0.0),
                                content: RegisteredSeller(sellerId: productDetailJSON[0]['product_creator'].toString()),
                              ));
                    },
                    child: Row(
                      children: [
                        Image.asset(
                          'images/info.png',
                          scale: 1.3,
                        ),
                        const Text(" مشخصات فروشنده ",
                            style: TextStyle(
                              color: Colors.lightBlueAccent,
                            )),
                      ],
                    ),
                  ),
                ),
                // 7th Section Other in this category products=========================================================
                const SizedBox(
                  height: 10,
                ),
                const Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "کالاهای مشابه",
                            style: TextStyle(),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "مشاهده ی همه",
                            style: TextStyle(
                              color: Colors.lightBlueAccent,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                for (int i = 0; i < relatedProductsJSON.length; i++)
                  Row(children: [
                    Expanded(
                        child: AspectRatio(
                      aspectRatio: 1,
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.leftToRight,
                                      child: Auction(
                                        productJSON: relatedProductsJSON[i - 1],
                                      )));
                            },
                            child: Image.network('${JJ.server}upload/${relatedProductsJSON[i]!['product_pic1']}')),
                      ),
                    )),
                    Expanded(
                        child: AspectRatio(
                      aspectRatio: 1,
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: (++i < relatedProductsJSON.length)
                            ? InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          child: Auction(
                                            productJSON: relatedProductsJSON[i],
                                          )));
                                },
                                child: Image.network('${JJ.server}upload/${relatedProductsJSON[i]!['product_pic1']}'))
                            : const Text(""),
                      ),
                    )),
                  ]),
              ],
            ),
          ),
        ),
        //Footer================================================
        bottomNavigationBar: const NavBarBottom());
  }
}
