import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'registerVIP.dart';
import 'tools/jjTools.dart';
import 'widgets/navBarBottom.dart';
import 'widgets/navBarTop.dart';

import 'widgets/AuctionRow.dart';
import 'widgets/Header.dart';
import 'widgets/auctionCategory.dart';
import 'widgets/productRow.dart';

class AuctionsOfOneCategory extends StatefulWidget {
  final int categoryId ;
  const AuctionsOfOneCategory({Key? key, required this.categoryId}) : super(key: key);

  @override
  State<AuctionsOfOneCategory> createState() => _AuctionsOfOneCategoryState();
}

class _AuctionsOfOneCategoryState extends State<AuctionsOfOneCategory> {
  late List<dynamic> items = [];

  @override
  void initState() {
    super.initState();
    getMoreItem(0);
  }
  getMoreItem(int lastIndex) async {
    debugPrint('AuctionsOfOneCategory.getMoreItem()');
    String productCreatorCondition = '&product_categoryId=${widget.categoryId}';
    String result = await JJ().jjAjax(
        'do=Product.getTopAuctions&product_categoryId=${widget.categoryId}&lastIndex=$lastIndex&product_isAuction=1');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('محصول مورد نظر شما بارگذاری نشده است');
      return;
    }
    items.addAll(temp);//Add new items to available items
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.black,
            centerTitle: true,
            title: const Header()
        ),
      // 1st Section =========================================================
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              const NavBarTop(),
              if(JJ.user_grade =="VIP")// If user is not VIP show button to become vip for auctions
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: InkWell(
                    onTap:() {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const RegisterVIP()));
                    },
                    child: Image.asset(
                      "images/AuctionHeader.png",
                    ),
                  ),
                ),
              // 3th Section =========================================================
              const SizedBox(
                height: 10,
              ),
              AuctionCategory(categoryId: widget.categoryId),
              //3nd section ====================================================
              //  AuctionRow(productJSON: Null,
              // ),
              //########################################
              for (int i = 0; i < items.length; i++)
                AuctionRow(productJSON: (items[i])),
              //########################################
              const SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  getMoreItem(items.length);
                },
                child: const Center(
                  child: Icon(
                    Icons.add_circle_outlined,
                    color: Colors.black12,
                    size: 55,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      //Footer================================================
        bottomNavigationBar: const NavBarBottom()
    );
  }
}
