import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/navBarBottom.dart';
import 'package:vaTamam/mrAntik/widgets/navBarTop.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'auction.dart';
import 'tools/jjTools.dart';
import 'widgets/AuctionRow.dart';
import 'widgets/Header.dart';

class AuctionsOfOneGallery extends StatefulWidget {
  final Map<String, dynamic> sellerAndLastAuctionJSON;

  const AuctionsOfOneGallery({Key? key, required this.sellerAndLastAuctionJSON}) : super(key: key);

  @override
  State<AuctionsOfOneGallery> createState() => _AuctionsOfOneGalleryState();
}

class _AuctionsOfOneGalleryState extends State<AuctionsOfOneGallery> {
  late List<dynamic> items = [];

  @override
  void initState() {
    super.initState();
    getMoreItem(0);
  }

  bool isFollowedByThisUser = true;
  Future<void> toggleFollow(String id) async {
    isFollowedByThisUser = isFollowedByThisUser ? false : true; //FOR toggle btn
    debugPrint('isFollowedByThisUser:::$isFollowedByThisUser');
    String params = 'do=Access_User.toglleFollow&id=$id';
    String result = await JJ().jjAjax(params);
    Map<String, dynamic> json = jsonDecode(result);
    SharedPreferences? localStorage;
    localStorage = await SharedPreferences.getInstance();
    if (json['status'] == "0") {
      JJ.jjToast(json['comment']);
    } else {
      // JJ.jjToast(json['comment']);
      setState(() {});
    }
  }

  getMoreItem(int lastIndex) async {
    debugPrint('AuctionsOfOneGallery.getMoreItem()');
    String productCreatorCondition = '&product_creator=${widget.sellerAndLastAuctionJSON['id']}';
    String result = await JJ().jjAjax('do=Product.getTopAuctions&product_categoryId=&lastIndex=$lastIndex$productCreatorCondition');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('محصول مورد نظر شما بارگذاری نشده است');
      return;
    }
    items.addAll(temp); //Add new items to available items
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const NavBarTop(),
                // 2nd Section about this gallery=========================================================
                const SizedBox(
                  height: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(
                    '${JJ.server}upload/${widget.sellerAndLastAuctionJSON['user_pic1']}',
                    fit: BoxFit.fitWidth,
                  ),
                ),
                Stack(children: [
                  Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 7, bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      shape: BoxShape.rectangle,
                      border: Border.all(
                        color: Colors.black12,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () {
                            toggleFollow(widget.sellerAndLastAuctionJSON['product_creator']);
                          },
                          child: Image.asset(
                            isFollowedByThisUser
                                ? // if this row is followed by user then che color of follow btn
                                ('images/unFollowEmpty.png')
                                : ('images/follow${'${widget.sellerAndLastAuctionJSON['user_badge']}'.isEmpty ? 'Empty' : '${widget.sellerAndLastAuctionJSON['user_badge']}'}.png'),
                            fit: BoxFit.fitHeight,
                            scale: 1.3,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          child: Text(
                            'رتبه اعتباری' '${widget.sellerAndLastAuctionJSON['user_credit']}',
                            style: const TextStyle(
                              fontSize: 12,
                              color: Colors.black45,
                              height: 2,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 7,
                    top: 7,
                    child: Image.asset(
                      'images/badge${'${widget.sellerAndLastAuctionJSON['user_badge']}'.isEmpty ? 'Empty' : '${widget.sellerAndLastAuctionJSON['user_badge']}'}.png',
                      scale: 1.6,
                    ),
                  )
                ]),
                //3nd section ====================================================
                for (int i = 0; i < items.length; i++)
                  AuctionRow(
                    productJSON: items[i],
                  )
                //If there was only one auction, skip from auctions list and show the onliest auction
                ,
                //===============================================================
                const SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () {
                    getMoreItem(items.length);
                  },
                  child: const Center(
                    child: Icon(
                      Icons.add_circle_outlined,
                      color: Colors.black12,
                      size: 55,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        //Footer================================================
        bottomNavigationBar: const NavBarBottom());
  }
}
