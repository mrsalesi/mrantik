import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'tools/jjTools.dart';

class SellerPhoneNubmer extends StatefulWidget {
  final String sellerId;
  const SellerPhoneNubmer({Key? key, required this.sellerId}) : super(key: key);

  @override
  State<SellerPhoneNubmer> createState() => _SellerPhoneNubmerState();

}



class _SellerPhoneNubmerState extends State<SellerPhoneNubmer> {
  bool _hasCallSupport = false;

  String user_mobile ='000';


  @override
  void initState() {
    super.initState();
    getUserPhoneNumber(widget.sellerId);
    canLaunchUrl(Uri(scheme: 'tel', path: user_mobile)).then((bool result) {
      setState(() {
        _hasCallSupport = result;
      });
    });
  }
  getUserPhoneNumber(String id) async {
    debugPrint('getUserPhoneNumber() ...');
    String result = await JJ().jjAjax('do=Access_User.getUserPhoneNumber&id=$id');
    Map<String,dynamic> json = jsonDecode(result);
    if (json['status'] == "0") {
      String comment = json['comment'];
      if (context.mounted) Navigator.of(context).pop();
      JJ.jjToast(json['comment']);
    }else{
      user_mobile=json['user_mobile'];
    }
    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    String _selectedValue;;
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child:  SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            color: const Color(0xfef7edff),
            borderRadius: BorderRadius.circular(8.0),
            border: Border.all(color: Colors.white54),
          ),
          child: Column(
            children: [
              Row(
                children: const [
                  Icon(
                    Icons.call,
                    color: Colors.black87,
                  ),
                  Text(" شماره تماس فروشنده ",
                      style: TextStyle(
                        color: Colors.black87,
                      )),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () async {
                  Uri launchUri = Uri(
                    scheme: 'tel',
                    path: user_mobile,
                  );
                  await launchUrl(launchUri);
              },
                  child: Container(
                  height: 30,
                  margin:
                  const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(
                  "images/textfieldBackGroundGold.png",
                  ),
                  ),
                  ),
                  child: Row(
                  children: [
                   const Expanded(
                  flex: 2,
                  child: Text(
                  textAlign: TextAlign.center,
                              "تماس تلفنی : ",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: Text(
                            textAlign: TextAlign.center,
                              user_mobile,
                            style: const TextStyle(
                                color: Colors.black,
                                fontSize: 14
                            ),
                          ),
                          ),
                        ],
                      ),
                    ),
              ),
              InkWell(
                onTap: () async {
                  Uri launchUri = Uri(
                    scheme: 'sms',
                    path: user_mobile,
                  );
                  await launchUrl(launchUri);
                },
                child: Container(
                  height: 30,
                  margin:
                  const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        "images/textfieldBackGroundGold.png",
                      ),
                    ),
                  ),
                  child: Row(
                    children: [
                      const Expanded(
                        flex: 2,
                        child: Text(
                          textAlign: TextAlign.center,
                          "پیامک:",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          textAlign: TextAlign.center,
                          user_mobile,
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 20,),
            ],
          ),
        ),
      )
    );
  }
}

