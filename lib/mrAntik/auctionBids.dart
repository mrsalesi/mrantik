import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/FunkyNotification.dart';
import 'package:vaTamam/mrAntik/widgets/Header.dart';
import 'package:vaTamam/mrAntik/widgets/carouseSliderWithIndicator.dart';
import 'package:vaTamam/mrAntik/widgets/navBarBottom.dart';
import 'package:vaTamam/mrAntik/widgets/navBarTop.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:share_plus/share_plus.dart';
import 'package:slide_countdown/slide_countdown.dart';

import 'registeredSeller.dart';
import 'tools/jjTools.dart';
import 'tools/mrAnticCalculator.dart';
import 'widgets/allertDialog.dart';

class AuctionBids extends StatefulWidget {
  final Map<String, dynamic> productJSON; //basic data of product
  const AuctionBids({Key? key, required this.productJSON}) : super(key: key);

  @override
  State<AuctionBids> createState() => _AuctionBidsState();
}

class _AuctionBidsState extends State<AuctionBids> {
  List<dynamic> lastBidsJSON = [];
  late List<dynamic> items = [];


  @override
  initState() {
    super.initState();
    debugPrint('AuctionBids.initState(); JJ.user_grade=${JJ.user_grade}');
    getLastAuctionBids(0, '0');
  }

  ///Get last 10 AuctionBids To calculate how is winner
  getLastAuctionBids(int lastIndex, String lastBidId) async {
    debugPrint('ProductAuctionBid.getLastAuctionBids()... get last Bids');
    String result = await JJ().jjAjax('do=ProductAuctionBid.getLastAuctionBids'
        '&auctionBid_productId=${widget.productJSON['id']}'
        '&lastIndex=$lastIndex'
        '&lastBidId=$lastBidId');
    List<dynamic> temp = jsonDecode(result);
    debugPrint('----------------->>>>>>'+items.toString());
    if (temp.isEmpty) {
      return;
    }
    items.insertAll(0, temp); //Add new items to available items
    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.black,
            centerTitle: true,
            title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                // 3th Section =========================================================

                for (int i = 0; i < items.length; i++)
                  Container(
                    padding: EdgeInsets.all(4),
                    margin: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Color(0xffeaeaea),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child:Row(
                      children: [
                        Expanded(
                          flex: 1,
                            child: Text('${items[i]['id']} ' // Date of bid
                              ,style: TextStyle(fontSize: 11),
                                )
                        ),Expanded(
                          flex: 3,
                            child: Text('${items[i]['auctionBid_date'].toString().toPersianDate()} ' // Date of bid
                                '\n'
                                '${items[i]['auctionBid_date'].toString().substring(10,19)}'
                            ,style: TextStyle(fontSize: 12),)// Time of bid
                        ),
                        Expanded(
                          flex: 4,
                            child: Text('${items[i]['auctionBid_price'].toString().seRagham()} تومان '
                            ,style: TextStyle(color: i==0?Colors.green:Colors.redAccent ),
                            )
                        ),Expanded(
                          flex: 4,
                            child: Text('${items[i]['auctionBid_username'].toString().substring(0, min(10,items[i]['auctionBid_username'].toString().length))}...'
                            ,textAlign: TextAlign.center,
                              textDirection: TextDirection.ltr
                                ,style: TextStyle(fontSize: 12,color: i==0?Colors.green :Colors.redAccent),
                            )
                        ),
                      ],
                    ) ,
                  ),


                const SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () {
                    getLastAuctionBids(items.length,lastBidsJSON.isEmpty ? '0' : lastBidsJSON[0]['id']);
                  },
                  child: const Center(
                    child: Icon(
                      Icons.add_circle_outlined,
                      color: Colors.black12,
                      size: 55,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
    );
  }
}
