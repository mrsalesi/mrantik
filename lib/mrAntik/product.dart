import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vaTamam/mrAntik/widgets/FunkyNotification.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:page_transition/page_transition.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'productAbuseReport.dart';
import 'register.dart';
import 'editProduct.dart';
import 'widgets/Header.dart';
import 'widgets/allertDialog.dart';
import 'widgets/carouseSliderWithIndicator.dart';
import 'widgets/navBarBottom.dart';
import 'widgets/navBarTop.dart';
import 'products.dart';
import 'registeredSeller.dart';
import 'sellerPhoneNubmer.dart';
import 'payment.dart';
import 'tools/jjTools.dart';

class Product extends StatefulWidget {
  final Map<String, dynamic> productJSON; //basic data of product
  const Product({Key? key, required this.productJSON}) : super(key: key);

  @override
  State<Product> createState() => _ProductState();
}

class _ProductState extends State<Product> {
  late List<dynamic> productDetailJSON = [];
  late List<dynamic> relatedProductsJSON = [];
  late List<String> imagesList = [];
  late Map<String, String> propAndValueList = {};
  bool isLiked = false;

  @override
  initState() {
    super.initState();
    getProduct(widget.productJSON!['id']);
    checkProductIsLiked();
  }

  ///  To check if product is liked before or not
  checkProductIsLiked() {
    String? likedProducts = JJ.localStorage!.getString('likedProductsList');
    debugPrint("likedProducts:$likedProducts");
    if (likedProducts!.contains(',${widget.productJSON['id']},')) {
      isLiked = true;
    }
  }

  abuseReposr() async {}

  getProduct(String id) async {
    debugPrint('getProduct() ...');
    String result = await JJ().jjAjax('do=Product.getProduct&id=$id');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isEmpty) {
      JJ.jjToast('این محصول در حال حاضر وجود ندارد');
    } else {
      productDetailJSON = temp;
      debugPrint("\n%%%%%" + productDetailJSON[0]['products_content']);
      for (int i = 1; i <= 9; i++) {
        if (productDetailJSON[0]['product_pic$i'].toString().isNotEmpty) {
          imagesList.add(productDetailJSON[0]['product_pic$i']);
        }
      }
      for (int i = 0; i < 20; i++) {
        // Creation a list of filled prop and values
        if (productDetailJSON[0]['product_val$i'].toString().isNotEmpty && productDetailJSON[0]['product_prop$i'].toString().isNotEmpty) {
          propAndValueList[productDetailJSON[0]['product_prop$i']] = productDetailJSON[0]['product_val$i'];
        }
      }
      debugPrint(')))))))))))))))))))>>>$propAndValueList');
      setState(() {});
      getRelatedProducts(widget.productJSON!['id']); //Get related product
    }
  }

  getRelatedProducts(String id) async {
    debugPrint('getProduct() ...');
    String result = await JJ().jjAjax('do=Product.getRelatedProducts&id=$id');
    List<dynamic> temp = jsonDecode(result);
    if (temp.isNotEmpty) {
      relatedProductsJSON = temp;
      debugPrint("%%%%%" + relatedProductsJSON[0]['product_pic1']);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(automaticallyImplyLeading: false, backgroundColor: Colors.black, centerTitle: true, title: const Header()),
        // 1st Section =========================================================
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const NavBarTop(),
                // 2nd Section Categories of Auction=========================================================
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(
                        "وتمام",
                        style: TextStyle(color: Colors.black38),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Colors.black,
                        size: 20.0,
                      ),
                    ],
                  ),
                ),
                // 3th Section =========================================================
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Column(
                          children: [
                            if (widget.productJSON['product_creator'] == JJ.user_id)
                              InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.leftToRight,
                                            child: EditProductByUser(
                                              productDetailJSON: productDetailJSON,
                                            )));
                                  },
                                  child: const Icon(
                                    Icons.edit,
                                    color: Color(0xffe8ac4f),
                                  ))
                            else
                              const SizedBox(
                                height: 30,
                              ),
                            const SizedBox(
                              height: 150,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: InkWell(
                                onTap: () {
                                  JJ.jjShare('${productDetailJSON[0]['product_name']} https://vatama.com/Product/${productDetailJSON[0]['id']}');
                                },
                                child: const Icon(
                                  Icons.share_outlined,
                                  color: Colors.black,
                                  size: 30.0,
                                ),
                              ),
                            ),
                          ],
                        )),
                    Expanded(
                      flex: 4,
                      child: imagesList.isEmpty //while the product detail is caching from server
                          ? CarouselSliderWithIndicator(
                              //load carouselSlider with first image from productJson
                              imagesList: ['${widget.productJSON['product_pic1']}'])

                          : CarouselSliderWithIndicator(
                              imagesList: imagesList,
                            ),
                    ),
                    Expanded(
                        flex: 1,
                        child: Column(
                          children: [
                            if (widget.productJSON['product_creator'] == JJ.user_id)
                                InkWell(
                                  onTap: (){
                                    launchUrl(Uri.parse("${JJ.server.replaceFirst("app.", "")}nardeban.jsp?id=${widget.productJSON['id']}"),mode: LaunchMode.externalApplication);
                                  },
                                  // child: const Icon(Icons.move_up, color: Color(0xffe8ac4f), size: 30.0,),
                                  child: Image.asset("images/moveUp.png",scale: 1.9,),
                                )
                            else
                              const SizedBox(
                                height: 30,
                              ),
                            const SizedBox(
                              height: 150,
                            ),
                            InkWell(
                              onTap: () {
                                isLiked = isLiked == true ? false : true; //toggle isLiked flag
                                // Add this product id to one string list in local storage
                                if (isLiked) {
                                  if (JJ.localStorage!.getString('likedProductsList') == null) {
                                    // for first like
                                    JJ.localStorage!.setString('likedProductsList', ',${widget.productJSON['id']},');
                                  } else {
                                    String? likedProducts = JJ.localStorage!.getString('likedProductsList');
                                    likedProducts = '${likedProducts!},${widget.productJSON['id']},';
                                    JJ.localStorage!.setString('likedProductsList', likedProducts);
                                  }
                                } else {
                                  // remove this id from likedProductsList
                                  if (JJ.localStorage!.getString('likedProductsList') != null) {
                                    // if not null, this may not be accord
                                    String? likedProducts = JJ.localStorage!.getString('likedProductsList');
                                    likedProducts = likedProducts!.replaceAll(',${widget.productJSON['id']},', '');
                                    JJ.localStorage!.setString('likedProductsList', likedProducts);
                                  }
                                }
                                setState(() {});
                              },
                              child: isLiked
                                  ? const Icon(
                                      Icons.favorite_outlined,
                                      color: Colors.redAccent,
                                      size: 30.0,
                                    )
                                  : const Icon(
                                      Icons.favorite_outline_outlined,
                                      color: Colors.black,
                                      size: 30.0,
                                    ),
                            ),
                          ],
                        )),
                  ],
                ),
                // 4th Section product info=========================================================
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 5,
                        child: Row(
                          children: [
                            Text(
                              widget.productJSON['product_name'],
                              style: const TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold),
                            ),
                          ],
                        )),
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            const Text("کد: "),
                            Text(
                              productDetailJSON.isEmpty ? '' : productDetailJSON[0]['id'].toString(),
                              style: const TextStyle(color: Colors.grey, fontSize: 13),
                            ),
                          ],
                        ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                //ویژگی های کالا  = Product Features
                Column(
                  children: [
                    Row(
                      children: [
                        const Align(
                          alignment: Alignment.topRight,
                          child: Text("ویژگی های کالا", textAlign: TextAlign.right),
                        ),
                        if (widget.productJSON['product_creator'] == JJ.user_id)
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        child: EditProductByUser(
                                          productDetailJSON: productDetailJSON,
                                        )));
                              },
                              child: const Row(
                                children: [
                                  Icon(
                                    Icons.edit,
                                    color: Color(0xffe8ac4f),
                                  ),
                                  Text(' (ویرایش)', style: TextStyle(color: Color(0xffe8ac4f))),
                                ],
                              ))
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Column(
                      children: [
                        Row(
                          children: [
                            const Expanded(
                                flex: 2,
                                child: Text(
                                  "ابعاد",
                                  //The first attr is property name
                                  style: TextStyle(color: Colors.grey, fontSize: 12),
                                )),
                            Expanded(
                                flex: 4,
                                child: Container(
                                  padding: const EdgeInsets.all(6),
                                  margin: const EdgeInsets.all(4),
                                  decoration: BoxDecoration(color: Colors.black12, borderRadius: BorderRadius.circular(8)),
                                  child: Text(productDetailJSON.isEmpty ? "" : productDetailJSON[0]['product_dimension']),
                                )),
                            const Expanded(flex: 3, child: Text('')),
                          ],
                        ),
                        for (int i = 0; i < propAndValueList.length; i++)
                          Row(
                            children: [
                              Expanded(
                                  flex: 2,
                                  child: Text(
                                    propAndValueList.keys.toList()[i] ?? "",
                                    //The first attr is property name
                                    style: const TextStyle(color: Colors.grey, fontSize: 12),
                                  )),
                              Expanded(
                                  flex: 4,
                                  child: Container(
                                    padding: const EdgeInsets.all(6),
                                    margin: const EdgeInsets.all(4),
                                    decoration: BoxDecoration(color: Colors.black12, borderRadius: BorderRadius.circular(8)),
                                    child: Text(propAndValueList.values.toList()[i] ?? ""), //The second attr is value
                                  )),
                              Expanded(
                                  flex: 3,
                                  child: i == propAndValueList.length - 1
                                      ? InkWell(
                                          onTap: () {
                                            Navigator.push(context, PageTransition(type: PageTransitionType.leftToRight, child: ProductAbuseReport(productId: productDetailJSON[0]['id'], productTitle: productDetailJSON[0]['product_name'])));
                                          },
                                          child: const Row(
                                            children: [
                                              SizedBox(width: 15),
                                              Icon(
                                                Icons.not_interested_rounded,
                                                color: Colors.red,
                                                size: 20,
                                              ),
                                              Text(
                                                "گزارش تخلف",
                                                style: TextStyle(color: Colors.grey, fontSize: 12, fontWeight: FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        )
                                      : const Text('')),
                            ],
                          ),
                      ],
                    )
                  ],
                ),

                // 5th Section =========================================================
                Container(
                  padding: const EdgeInsets.all(15),
                  margin: const EdgeInsets.only(top: 5),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Column(
                    children: [
                      const Align(
                        alignment: Alignment.topRight,
                        child: Text(
                          "توضیحات فروشنده:",
                          style: TextStyle(fontSize: 12, color: Colors.grey),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      ExpandableText(productDetailJSON.isEmpty ? '' : productDetailJSON[0]['products_content']),
                    ],
                  ),
                ),
                // 6th Section Price and Buy =========================================================
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Expanded(
                      child: Text(
                        'قیمت کالا',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, height: 2),
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(widget.productJSON['product_price1'].toString() == "0" ? 'توافقی' : widget.productJSON['product_price1'].toString().seRagham() ,textDirection: TextDirection.ltr,),
                            if (widget.productJSON['product_price1'].toString() != "0")
                              const Text(
                                ' تومان ',
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                    const Expanded(
                      child: Text(''),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1, //if price is 0 don't show buy btn
                      child: (widget.productJSON['product_price1'].toString() != "0")
                          ? Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Align(
                                      alignment: Alignment.centerLeft,
                                      child: InkWell(
                                        onTap: () {
                                          showDialog(
                                              context: context,
                                              useSafeArea: true,
                                              builder: (_) => const jjAlertDialog(
                                                    type: '',
                                                    richText: Text(''
                                                        'با فعال سازی این گزینه شما میتوانید جهت اطمینان از تضمین معامله  با نظارت وتمام اقدام به خرید کنید بصورتی که وجه شما پس از واریزی به امانت نزد ما نگه داری میشود و پس از تحویل کالا و تایید شما از صحت و سلامت جنس نسبت به آزاد سازی وجه به فروشنده اقدام میشود، در غیر اینصورت تمامی مبلغ واریزی به حساب شما عودت داده میشود.'),
                                                    title: 'خرید با نظارت مسترانتیک:',
                                                  ));
                                        },
                                        child: const CircleAvatar(
                                          radius: 10,
                                          backgroundColor: Colors.lightBlueAccent,
                                          child: Icon(
                                            Icons.question_mark_rounded,
                                            color: Colors.white,
                                            size: 15,
                                          ),
                                        ),
                                      )),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  InkWell(
                                      onTap: () {
                                        if (JJ.user_token.isEmpty) {
                                          // if user is not login
                                          showDialog(
                                              context: context,
                                              useSafeArea: true,
                                              builder: (_) => AlertDialog(
                                                    insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                                    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                                    contentPadding: const EdgeInsets.all(0.0),
                                                    content: Register(),
                                                  ));
                                        } else {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => Payment(
                                                        productDetailJSON: productDetailJSON[0],
                                                        productJSON: widget.productJSON,
                                                      )));
                                        }
                                      },
                                      child: Image.asset("images/buy.png")),
                                ],
                              ),
                            )
                          : const Text(''),
                    ),
                    Expanded(
                      flex: 1,
                      child: InkWell(
                        onTap: () {
                          if (JJ.user_token.isEmpty) {
                            // if user is not login, show login/register Form
                            showDialog(
                                context: context,
                                useSafeArea: true,
                                builder: (_) => AlertDialog(
                                      insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                      contentPadding: const EdgeInsets.all(0.0),
                                      content: Register(),
                                    ));
                          } else {
                            showDialog(
                                context: context,
                                useSafeArea: true,
                                builder: (_) => AlertDialog(
                                      insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                      contentPadding: const EdgeInsets.all(0.0),
                                      content: SellerPhoneNubmer(sellerId: productDetailJSON[0]['product_creator'].toString()),
                                    ));
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: InkWell(
                                    onTap: () {
                                      showDialog(
                                          context: context,
                                          useSafeArea: true,
                                          builder: (_) => const jjAlertDialog(
                                                type: '',
                                                richText: Text('وتمام به شما این امکان را'
                                                    ' میدهد که کاملا رایگان و بصورت مستقیم نسبت به خرید و فروش با کاربران اقدام کنید،'
                                                    ' اما مستر انتیک در این مورد هیچ گونه عواقبی را تقبل نمیکند پس بنا به هشدار پلیس محترم اولا'
                                                    ' از صحت و سقم جنس اطمینان حاصل کنین سپس به واریزی به صاحب کالا اقدام فرمایید.'),
                                                title: 'خرید مستقیم از فروشنده:',
                                              ));
                                    },
                                    child: const CircleAvatar(
                                      radius: 10,
                                      backgroundColor: Colors.lightBlueAccent,
                                      child: Icon(
                                        Icons.question_mark_rounded,
                                        color: Colors.white,
                                        size: 15,
                                      ),
                                    ),
                                  )),
                              const SizedBox(
                                height: 5,
                              ),
                              Image.asset("images/buyDirect.png"),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: InkWell(
                    onTap: () {
                      showDialog(
                          context: context,
                          useSafeArea: true,
                          builder: (_) => AlertDialog(
                                insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                                shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                contentPadding: const EdgeInsets.all(0.0),
                                content: RegisteredSeller(sellerId: productDetailJSON[0]['product_creator'].toString()),
                              ));
                    },
                    child: Row(
                      children: [
                        Image.asset(
                          'images/info.png',
                          scale: 1.3,
                        ),
                        const Text(" مشخصات فروشنده ",
                            style: TextStyle(
                              color: Colors.lightBlueAccent,
                            )),
                      ],
                    ),
                  ),
                ),
                // 7th Section Other in this category products=========================================================
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    const Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "کالاهای مشابه",
                            style: TextStyle(),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              PageTransition(
                                  type: PageTransitionType.leftToRight,
                                  child: Products(
                                    categoryId: productDetailJSON[0]['product_categoryId'],
                                    categoryTitle: "محصولات هم گروه با ${productDetailJSON[0]['product_name']}",
                                  )));
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(20.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "مشاهده ی همه",
                              style: TextStyle(
                                color: Colors.lightBlueAccent,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                for (int i = 0; i < relatedProductsJSON.length; i++)
                  Row(children: [
                    Expanded(
                        child: AspectRatio(
                      aspectRatio: 1,
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.leftToRight,
                                      child: Product(
                                        productJSON: relatedProductsJSON[i-1],
                                      )));
                            },
                            child: Image.network('${JJ.server}upload/${relatedProductsJSON[i]!['product_pic1']}')),
                      ),
                    )),
                    Expanded(
                        child: AspectRatio(
                      aspectRatio: 1,
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: (++i < relatedProductsJSON.length)
                            ? InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          child: Product(
                                            productJSON: relatedProductsJSON[i],
                                          )));
                                },
                                child: Image.network('${JJ.server}upload/${relatedProductsJSON[i]!['product_pic1']}'))
                            : Text(""),
                      ),
                    )),
                  ]),
              ],
            ),
          ),
        ),
        //Footer================================================
        bottomNavigationBar: const NavBarBottom());
  }
}
